/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src';
import {name as appName} from './app.json';

import * as Sentry from '@sentry/react-native';

if (__DEV__) {
  import('./src/config/ReactotronConfig').then(() =>
    console.log('Reactotron Configured')
  );
}

Sentry.init({
  dsn: 'https://8017f4020b6c41428b5fb7102ebc04ba@o1135359.ingest.sentry.io/6184205',
});

const MyApp = Sentry.wrap(App);

AppRegistry.registerComponent(appName, () => MyApp);
