import styled from 'styled-components/native';

import {Image} from 'react-native';

export const Logo = styled(Image).attrs({
  source: require('./assets/log-one.png'),
  resizeMode: 'contain',
})`
  width: 100px;
  height: 100px;
  align-self: center;
`;
