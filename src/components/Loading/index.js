import React from 'react';
import {ActivityIndicator} from 'react-native';

import {colors} from '../../global';
import {useAuth} from '../../hooks/Auth';

import {Container, Text} from './styles';

export default function Loading() {
  const {language} = useAuth();
  return (
    <Container>
      <ActivityIndicator size="large" color={colors.color_input_text} />
      <Text>{language ? 'Loading' : 'Carregando'} ...</Text>
    </Container>
  );
}
