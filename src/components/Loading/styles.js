import styled from 'styled-components/native';

import {colors, fonts} from '../../global';

export const Container = styled.SafeAreaView`
  flex: 1;
  background: ${colors.background_main};
  justify-content: center;
  align-items: center;
`;

export const Text = styled.Text`
  font-size: ${fonts.bigger};
  color: ${colors.color_input_text};
  font-family: ${fonts.fontFamilyRegular};
`;
