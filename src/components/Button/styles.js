import styled, {css} from 'styled-components/native';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';

import {colors, fonts} from '../../global';

export const Container = styled.TouchableOpacity`
  width: ${({ smallerButton }) => smallerButton ? '47%' : '100%'};
  height: 48px;
  background-color: ${colors.orange};
  border-radius: 24px;
  margin-top: 8px;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  position: relative;
  ${props =>
    !props.active &&
    css`
      background-color: #333333;
    `}
`;

export const ButtonText = styled.Text`
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.regular};
  color: ${colors.color_text};
`;

export const Icon = styled(SimpleLineIcon)`
  position: absolute;
  top: 30%;
  left: 16px;
`;
