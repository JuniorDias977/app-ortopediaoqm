import styled, {css} from 'styled-components/native';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import TextInputMask from 'react-native-text-input-mask';

import {colors, fonts} from '../../global';

export const Container = styled.View`
  width: 100%;
  align-items: center;
`;

export const Content = styled.View`
  width: 100%;
  flex-direction: row;
  padding: 0 16px 0 0;
  align-items: flex-end;
  padding-bottom: 5px;
  margin-bottom: 12px;
  min-height: 55px;

  border-bottom-width: 1px;
  border-bottom-color: ${({borderBottom}) =>
    borderBottom ? borderBottom : colors.color_text};
  ${props =>
    props.password &&
    css`
      padding-right: 5px;
    `};
  ${props =>
    props.isFocused &&
    css`
      border-bottom-color: ${colors.orange};
    `};
  ${props =>
    props.error &&
    css`
      border-bottom-color: ${colors.red};
    `};
`;

export const TextInput = styled.TextInput`
  flex: 1;
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.regular};
  color: ${colors.color_input_text};
  padding: 0;
  ${props =>
    props.textAlign &&
    css`
      text-align: center;
    `};
`;

export const TextInputM = styled(TextInputMask)`
  flex: 1;
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.regular};
  color: ${colors.color_input_text};
  ${props =>
    props.textAlign &&
    css`
      text-align: center;
    `};
`;

export const Icon = styled(SimpleLineIcon)`
  margin-right: 0;
  padding-bottom: 5px;
`;

export const ErrorText = styled.Text`
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.small};
  color: ${colors.error};
`;
