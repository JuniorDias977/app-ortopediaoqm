import React, {useState, useEffect, useCallback} from 'react';
import {TouchableOpacity} from 'react-native';

import {colors} from '../../global';
import {
  Container,
  TextInput,
  Content,
  Icon,
  ErrorText,
  TextInputM,
} from './styles';

const Input = ({
  icon,
  password = false,
  error,
  mask = null,
  type = null,
  isValid = true,
  borderBottom = null,
  textAlign = null,
  ...rest
}) => {
  const [passwordView, setPasswordView] = useState(false);
  const [isFocused, setIsFocused] = useState(false);

  const handleInputFocus = useCallback(() => {
    setIsFocused(true);
  }, []);

  const handleInputBlur = useCallback(() => {
    setIsFocused(false);
  }, []);

  useEffect(() => {
    setPasswordView(password);
  }, [password]);

  return (
    <Container password={password} isFocused={isFocused} error={error}>
      <Content borderBottom={borderBottom} isFocused={isFocused}>
        {!mask ? (
          <TextInput
            {...rest}
            isFocused={isFocused}
            textAlign={textAlign}
            secureTextEntry={passwordView}
            onFocus={handleInputFocus}
            onBlur={handleInputBlur}
            placeholderTextColor="rgba(241, 241, 241, 0.4)"
          />
        ) : (
          <TextInputM
            textAlign={textAlign}
            {...rest}
            onFocus={handleInputFocus}
            onBlur={handleInputBlur}
            mask={mask}
            placeholderTextColor={colors.placeholder_text}
          />
        )}

        {isValid && type && type === 'email' ? (
          <Icon name="check" size={20} color="green" />
        ) : null}

        {password && (
          <TouchableOpacity onPress={() => setPasswordView(!passwordView)}>
            <Icon
              passwordView={passwordView}
              name="eye"
              size={20}
              color={passwordView ? colors.placeholder_text : colors.orange}
            />
          </TouchableOpacity>
        )}
      </Content>
    </Container>
  );
};

export default Input;
