import styled, {css} from 'styled-components/native';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import FeatherIcon from 'react-native-vector-icons/Feather';
import TextInputMask from 'react-native-text-input-mask';

import {colors, fonts} from '../../global';

export const Container = styled.View`
  width: ${({isFocused}) => (isFocused ? 88 : 100)}%;
  align-items: center;
  background-color: ${({isFocused}) => (isFocused ? '#333333' : 'transparent')};
  height: 54px;
  border-radius: 12px;
  margin: 0;
  padding: 0;
  margin-left: ${({isFocused}) => (isFocused ? 12 : 0)}px;
  align-items: center;
  z-index: 9;
`;

export const Content = styled.View`
  width: 100%;
  flex-direction: row;
  padding: 0 16px 0 0;
  align-items: flex-end;
  padding-bottom: 5px;
  margin-bottom: 12px;
  height: 54px;
  align-items: center;
  justify-content: space-between;
  z-index: 9;
`;

export const TextInput = styled.TextInput`
  flex: 1;
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.regular};
  color: ${colors.color_input_text};
  padding-bottom: 0;
  padding-left: 8px;
  padding-top: 10px;
  z-index: 8;
  ${props =>
    props.textAlign &&
    css`
      text-align: center;
    `};
`;

export const TextInputM = styled(TextInputMask)`
  flex: 1;
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.regular};
  color: ${colors.color_input_text};
  ${props =>
    props.textAlign &&
    css`
      text-align: center;
    `};
`;

export const Icon = styled(SimpleLineIcon)`
  margin-right: 0;
`;

export const IconF = styled(FeatherIcon)`
  padding-top: 5px;
  margin-right: 0;
`;

export const ErrorText = styled.Text`
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.small};
  color: ${colors.error};
`;
