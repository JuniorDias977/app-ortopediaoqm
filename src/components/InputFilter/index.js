import React, {useState, useEffect, useCallback, useRef} from 'react';
import {TouchableOpacity} from 'react-native';

import {colors} from '../../global';
import {Container, TextInput, Content, Icon, IconF} from './styles';

const Input = ({
  icon,
  error,
  mask = null,
  type = null,
  isValid = true,
  borderBottom = null,
  textAlign = null,
  setText,
  setIsFocusedData,
  handleInputBlurData = false,
  setHasFocusedData,
  ...rest
}) => {
  const [isFocused, setIsFocused] = useState(false);
  const refInput = useRef();

  const handleInputFocus = useCallback(() => {
    setIsFocused(true);
    setHasFocusedData(true);
  }, [setHasFocusedData]);

  const handleInputBlur = useCallback(() => {
    // setIsFocused(false);
    // setHasFocusedData(false);
  }, []);

  useEffect(() => {
    if (handleInputBlurData) {
      setIsFocused(false);
      setHasFocusedData(false);
      handleInputBlur();
      setIsFocusedData(false);
      refInput.current.blur();
    }
  }, [
    handleInputBlurData,
    handleInputBlur,
    setHasFocusedData,
    setIsFocusedData,
  ]);

  return (
    <Container isFocused={isFocused} error={error}>
      <Content borderBottom={borderBottom} isFocused={isFocused}>
        {!mask ? (
          <TextInput
            {...rest}
            ref={refInput}
            isFocused={isFocused}
            textAlign={textAlign}
            onFocus={handleInputFocus}
            onBlur={handleInputBlur}
            placeholderTextColor="rgba(241, 241, 241, 0.4)"
          />
        ) : null}

        {icon && !isFocused ? (
          <Icon name={icon} size={20} color={colors.white} />
        ) : null}

        {isFocused ? (
          <TouchableOpacity
            style={{zIndex: 9999999}}
            onPress={() => {
              setText('');
            }}>
            <IconF name="x" size={24} color={colors.white} />
          </TouchableOpacity>
        ) : null}
      </Content>
    </Container>
  );
};

export default Input;
