import styled from 'styled-components';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import {Picker} from '@react-native-community/picker';

import {colors, fonts} from '../../global';

export const HeaderName = styled.Text`
  font-size: ${fonts.biggerTwo};
  color: ${colors.background_main};
  font-family: ${fonts.fontFamilyBold};
  text-align: center;
  width: 100%;
`;

export const Container = styled.ScrollView`
  background: ${colors.background_main};
  padding: 12px;
  border-radius: 15px;
  max-height: 250px;
`;

export const MenuButton = styled.TouchableOpacity`
  height: 40px;
  width: 40px;
  align-items: center;
  justify-content: center;
  align-self: flex-end;
`;

export const Icon = styled(SimpleLineIcon)``;

export const PickerContainer = styled.View`
  width: 100%;
  padding: 5px 0 0 0;
  margin: 0;
  margin-bottom: 12px;
  flex-direction: row;
  align-items: flex-start;
  position: relative;
  min-height: 20%;
`;

export const PickerStyled = styled(Picker)`
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.regular};
  padding: 0;
  color: ${({color}) => (color ? color : colors.white)};
  margin: 0;
  width: 102%;
  text-align: left;
  top: 11%;
  left: -2%;
  position: absolute;
  ::placeholder {
    width: 100%;
    text-align: left;
    padding: 0;
    margin: 0;
    color: ${({color}) => (color ? color : colors.white)};
  }
`;

export const TextSelect = styled.Text`
  font-size: ${fonts.regular};
  color: rgba(241, 241, 241, 0.4);
  font-family: ${fonts.fontFamilyRegular};
  position: absolute;
  top: 40%;
`;
