import React, {useCallback} from 'react';
import Modal from 'react-native-modal';

import {colors} from '../../global';

import {
  Container,
  Icon,
  MenuButton,
  PickerContainer,
  PickerStyled,
  TextSelect,
} from './styles';
const ModalLoad = ({
  isModalVisible,
  setModalVisible,
  setSelected,
  selected,
}) => {
  const handleClose = useCallback(() => {
    setModalVisible(false);
  }, [setModalVisible]);

  return (
    <Modal isVisible={isModalVisible}>
      <Container contentContainerStyle={{flexGrow: 1, paddingBottom: 40}}>
        <MenuButton onPress={handleClose}>
          <Icon name="close" size={25} color={colors.white} />
        </MenuButton>
        <PickerContainer>
          {!selected ? <TextSelect>Ano da sua residência</TextSelect> : null}
          <PickerStyled
            selectedValue={selected}
            mode="dropdown"
            onValueChange={(itemValue, itemIndex) => setSelected(itemValue)}>
            <PickerStyled.Item
              color="#717171"
              label="Ano da sua residência"
              value=""
            />
            <PickerStyled.Item label="Sou R1" value="R1" />
            <PickerStyled.Item label="Sou R2" value="R2" />
            <PickerStyled.Item label="Sou R3" value="R3" />
          </PickerStyled>
          {/* <IconStyled
                      name="arrow-down"
                      size={20}
                      color={colors.placeholder_text}
                    /> */}
        </PickerContainer>
      </Container>
    </Modal>
  );
};

export default ModalLoad;
