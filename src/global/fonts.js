export default {
  small: '13px',
  regular: '16px',
  bigger: '24px',
  biggerTwo: '28px',
  extraBigger: '32px',
  extraBiggerTwo: '44px',
  fontFamilyRegular: 'Poppins-Regular',
  fontFamilyBold: 'Poppins-Medium',
  fontFamilyStrongBold: 'Poppins-SemiBold',
  fontFamilyLight: 'Poppins-Light',
};
