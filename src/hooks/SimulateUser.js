import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from 'react';
import firestore from '@react-native-firebase/firestore';

import {useAuth} from './Auth';

const SimulateUserContext = createContext({});

export function useSimulateUser() {
  const context = useContext(SimulateUserContext);

  if (!context) {
    throw new Error(
      'useSimulateUser must be used whitin an SimulateUserProvider',
    );
  }

  return context;
}

export const SimulateUserProvider = ({children}) => {
  const [loading, setLoading] = useState(true);
  const [simulatesUser, setSimulateUser] = useState([]);
  const {user} = useAuth();

  const getSimulateUser = useCallback(
    search => {
      setLoading(true);
      try {
        if (search) {
          firestore()
            .collection('simulates_user')
            .where('name', '==', search)
            .onSnapshot(querySnapshot => {
              let categoriesArr = [];
              querySnapshot.forEach(documentSnapshot => {
                categoriesArr.push({
                  ...documentSnapshot.data(),
                  key: documentSnapshot.id,
                });
              });
              setSimulateUser(categoriesArr);
              setLoading(false);
            });
        } else {
          firestore()
            .collection('simulates_user')
            .where('user', '==', user.key)
            .onSnapshot(querySnapshot => {
              let categoriesArr = [];
              querySnapshot.forEach(documentSnapshot => {
                categoriesArr.push({
                  ...documentSnapshot.data(),
                  key: documentSnapshot.id,
                });
              });
              setSimulateUser(categoriesArr);
              setLoading(false);
            });
        }

        setLoading(false);
      } catch (err) {
        setLoading(false);
      }
    },
    [user],
  );

  useEffect(() => {
    getSimulateUser();
  }, [getSimulateUser]);

  return (
    <SimulateUserContext.Provider
      value={{
        simulatesUser,
        loadingSimulateUser: loading,
        getSimulateUser,
      }}>
      {children}
    </SimulateUserContext.Provider>
  );
};
