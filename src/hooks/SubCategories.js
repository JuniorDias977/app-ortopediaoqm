import React, {createContext, useState, useContext, useEffect} from 'react';
import firestore from '@react-native-firebase/firestore';

const SubCategoriesContext = createContext({});

export function useSubCategories() {
  const context = useContext(SubCategoriesContext);

  if (!context) {
    throw new Error(
      'useSubCategories must be used whitin an SubCategoriesProvider',
    );
  }

  return context;
}

export const SubCategoriesProvider = ({children}) => {
  const [loading, setLoading] = useState(true);
  const [subCategories, setSubCategories] = useState([]);

  const getSubCategories = search => {
    setLoading(true);
    try {
      if (search) {
        firestore()
          .collection('sub_categories')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let categoriesArr = [];
            if (querySnapshot) {
              querySnapshot.forEach(documentSnapshot => {
                categoriesArr.push({
                  ...documentSnapshot.data(),
                  key: documentSnapshot.id,
                });
              });
            }
            setSubCategories(categoriesArr);
            setLoading(false);
          });
      } else {
        firestore()
          .collection('sub_categories')
          .onSnapshot(querySnapshot => {
            let categoriesArr = [];
            querySnapshot.forEach(documentSnapshot => {
              categoriesArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setSubCategories(categoriesArr);
            setLoading(false);
          });
      }
      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  };

  useEffect(() => {
    getSubCategories();
  }, []);

  return (
    <SubCategoriesContext.Provider
      value={{
        subCategories:
          subCategories && subCategories.length ? subCategories : [],
        loadingSubCategories: loading,
        getSubCategories,
      }}>
      {children}
    </SubCategoriesContext.Provider>
  );
};
