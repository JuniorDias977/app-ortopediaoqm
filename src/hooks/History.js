import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from 'react';
import firestore from '@react-native-firebase/firestore';

import {useAuth} from './Auth';

const HistoryUserContext = createContext({});

export function useHistoryUser() {
  const context = useContext(HistoryUserContext);

  if (!context) {
    throw new Error(
      'useHistoryUser must be used whitin an HistoryUserProvider',
    );
  }

  return context;
}

export const HistoryUserProvider = ({children}) => {
  const [loading, setLoading] = useState(true);
  const [historyUser, setHistoryUser] = useState([]);
  const {user} = useAuth();

  const getHistoryUser = useCallback(() => {
    setLoading(true);
    try {
      firestore()
        .collection('history')
        .where('user', '==', user.key)
        .onSnapshot(querySnapshot => {
          let categoriesArr = [];
          querySnapshot.forEach(documentSnapshot => {
            categoriesArr.push({
              ...documentSnapshot.data(),
              key: documentSnapshot.id,
            });
          });
          setHistoryUser(categoriesArr);
          setLoading(false);
        });

      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  }, [user]);

  useEffect(() => {
    getHistoryUser();
  }, [getHistoryUser]);

  return (
    <HistoryUserContext.Provider
      value={{
        historyUser: historyUser && historyUser.length ? historyUser : [],
        loadingHistory: loading,
        getHistoryUser,
      }}>
      {children}
    </HistoryUserContext.Provider>
  );
};
