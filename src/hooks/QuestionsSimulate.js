import React, {createContext, useState, useContext, useEffect} from 'react';
import firestore from '@react-native-firebase/firestore';

const QuestionsSimulateContext = createContext({});

export function useQuestionsSimulate() {
  const context = useContext(QuestionsSimulateContext);

  if (!context) {
    throw new Error(
      'useQuestionsSimulate must be used whitin an QuestionsSimulateProvider',
    );
  }

  return context;
}

export const QuestionsSimulateProvider = ({children}) => {
  const [loading, setLoading] = useState(true);
  const [questionsSimulate, setQuestionsSimulate] = useState([]);

  const getQuestionsSimulate = search => {
    setLoading(true);
    try {
      if (search) {
        firestore()
          .collection('questions_simulate')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let questionsSimulateArr = [];
            querySnapshot.forEach(documentSnapshot => {
              questionsSimulateArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setQuestionsSimulate(questionsSimulateArr);
            setLoading(false);
          });
      } else {
        firestore()
          .collection('questions_simulate')
          .onSnapshot(querySnapshot => {
            let questionsSimulateArr = [];
            querySnapshot.forEach(documentSnapshot => {
              questionsSimulateArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setQuestionsSimulate(questionsSimulateArr);
            setLoading(false);
          });
      }
      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  };

  useEffect(() => {
    getQuestionsSimulate();
  }, []);

  return (
    <QuestionsSimulateContext.Provider
      value={{
        questionsSimulate:
          questionsSimulate && questionsSimulate.length
            ? questionsSimulate
            : [],
        loadingQuestionsSimulate: loading,
        getQuestionsSimulate,
      }}>
      {children}
    </QuestionsSimulateContext.Provider>
  );
};
