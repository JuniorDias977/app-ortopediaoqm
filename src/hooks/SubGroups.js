import React, {createContext, useState, useContext, useEffect} from 'react';
import firestore from '@react-native-firebase/firestore';

const SubGroupsContext = createContext({});

export function useSubGroups() {
  const context = useContext(SubGroupsContext);

  if (!context) {
    throw new Error('useSubGroups must be used whitin an SubGroupsProvider');
  }

  return context;
}

export const SubGroupsProvider = ({children}) => {
  const [loading, setLoading] = useState(true);
  const [subGroups, setSubGroups] = useState([]);

  const getSubGroups = search => {
    setLoading(true);
    try {
      if (search) {
        firestore()
          .collection('sub_groups')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let subGroupsArr = [];
            if (querySnapshot) {
              querySnapshot.forEach(documentSnapshot => {
                subGroupsArr.push({
                  ...documentSnapshot.data(),
                  key: documentSnapshot.id,
                });
              });
            }
            setSubGroups(subGroupsArr);
            setLoading(false);
          });
      } else {
        firestore()
          .collection('sub_groups')
          .onSnapshot(querySnapshot => {
            let subGroupsArr = [];
            querySnapshot.forEach(documentSnapshot => {
              subGroupsArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setSubGroups(subGroupsArr);
            setLoading(false);
          });
      }
      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  };

  useEffect(() => {
    getSubGroups();
  }, []);

  return (
    <SubGroupsContext.Provider
      value={{
        subGroups: subGroups && subGroups.length ? subGroups : [],
        loadingsubGroups: loading,
        getSubGroups,
      }}>
      {children}
    </SubGroupsContext.Provider>
  );
};
