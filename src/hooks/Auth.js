import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from 'react';
import {Alert, Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {GoogleSignin} from '@react-native-google-signin/google-signin';

const AuthContext = createContext({});

export function useAuth() {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth must be used whitin an AuthProvider');
  }

  return context;
}

export const AuthProvider = ({children}) => {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    GoogleSignin.configure({
      webClientId: '',
    });
  }, []);

  useEffect(() => {
    async function loadStoragedData() {
      const [user] = await AsyncStorage.multiGet(['@appOrtopedia:user']);

      if (user[1]) {
        setData({user: JSON.parse(user[1])});
      }

      setLoading(false);
    }
    loadStoragedData();
  }, []);

  const signOut = useCallback(async () => {
    await AsyncStorage.multiRemove(['@appOrtopedia:user']);
    setData({});
  }, []);

  const signIn = useCallback(async ({email, password}) => {
    setLoading(true);
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(async function (user) {
        firestore()
          .collection('users')
          // Filter results
          .where('email', '==', user.user.email)
          .get()
          .then(async querySnapshot => {
            setLoading(true);
            let usersArr = [];
            querySnapshot.forEach(documentSnapshot => {
              usersArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            if (usersArr && usersArr.length) {
              if (usersArr[0].active === true) {
                await AsyncStorage.multiSet([
                  ['@appOrtopedia:user', JSON.stringify(usersArr[0])],
                ]);
                setLoading(false);

                firestore().collection('users').doc(usersArr[0].key).update({
                  key: usersArr[0].key,
                });
                setData({user: usersArr[0]});
              } else {
                setLoading(false);
                Alert.alert(
                  'Erro!',
                  'Usuário ainda não ativo. Aguarde contato através de seu Email.',
                );
              }
            }
          });
      })
      .catch(() => {
        setLoading(false);
        Alert.alert('Erro!', 'Email ou senha inválidos. Tente novamente.');
      });
  }, []);

  const singnInFacebook = useCallback(async facebookCredential => {
    setLoading(true);
    auth()
      .signInWithCredential(facebookCredential)
      .then(async function (user) {
        firestore()
          .collection('users')
          .where('email', '==', user.user.email)
          .onSnapshot(async querySnapshot => {
            let questionsArr = [];
            querySnapshot.forEach(documentSnapshot => {
              questionsArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            if (questionsArr && questionsArr.length) {
              await AsyncStorage.multiSet([
                ['@appOrtopedia:user', JSON.stringify(questionsArr[0])],
              ]);
              firestore().collection('users').doc(questionsArr[0].key).update({
                key: questionsArr[0].key,
              });

              setData({user: questionsArr[0]});
            } else {
              Alert.alert(
                'Erro!',
                'Email ou senha inválidos. Tente novamente.',
              );
            }
            setLoading(false);
          });
      })
      .catch(() => {
        Alert.alert('Erro!', 'Email ou senha inválidos. Tente novamente.');
        setLoading(false);
      });
  }, []);

  const singnInGoogle = useCallback(async () => {
    setLoading(true);
    GoogleSignin.configure({
      webClientId:
        Platform.OS === 'android'
          ? '655428816414-vqhfmpo9n3lmvgokeh3k6kq920q3vhgb.apps.googleusercontent.com'
          : '657839954573-2b0hmuddqe1dardh0ppidrm257ve10m3.apps.googleusercontent.com',
    });
    const {idToken} = await GoogleSignin.signIn();
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
    auth()
      .signInWithCredential(googleCredential)
      .then(async function (user) {
        setLoading(true);
        firestore()
          .collection('users')
          .where('email', '==', user.user.email)
          .onSnapshot(async querySnapshot => {
            let questionsArr = [];
            querySnapshot.forEach(documentSnapshot => {
              questionsArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            if (questionsArr && questionsArr.length) {
              await AsyncStorage.multiSet([
                [
                  '@appOrtopedia:user',
                  JSON.stringify({
                    avatar: user.user.photoURL,
                    ...questionsArr[0],
                  }),
                ],
              ]);
              firestore().collection('users').doc(questionsArr[0].key).update({
                avatar: user.user.photoURL,
                key: questionsArr[0].key,
              });
              setData({user: {avatar: user.user.photoURL, ...questionsArr[0]}});
              setLoading(false);
            } else {
              setLoading(false);
              Alert.alert(
                'Erro!',
                'Email ou senha inválidos. Tente novamente.',
              );
            }
            setLoading(false);
          });
      })
      .catch(() => {
        setLoading(false);
        Alert.alert('Erro!', 'Email ou senha inválidos. Tente novamente.');
      });
  }, []);

  const updateUser = useCallback(async user => {
    firestore()
      .collection('users')
      .where('email', '==', user.email)
      .onSnapshot(async querySnapshot => {
        let questionsArr = [];
        querySnapshot.forEach(documentSnapshot => {
          questionsArr.push({
            ...documentSnapshot.data(),
            key: documentSnapshot.id,
          });
        });
        if (questionsArr && questionsArr.length) {
          await AsyncStorage.multiSet([
            [
              '@appOrtopedia:user',
              JSON.stringify({
                ...questionsArr[0],
              }),
            ],
          ]);

          setData({user: {...questionsArr[0]}});
        }
        setLoading(false);
      });
  }, []);

  return (
    <AuthContext.Provider
      value={{
        user: data.user,
        signIn,
        signOut,
        loading,
        singnInFacebook,
        singnInGoogle,
        updateUser,
      }}>
      {children}
    </AuthContext.Provider>
  );
};
