import React, {createContext, useState, useContext, useEffect} from 'react';
import PropTypes from 'prop-types';
import firestore from '@react-native-firebase/firestore';

const QuestionsContext = createContext({});

export function useQuestions() {
  const context = useContext(QuestionsContext);

  if (!context) {
    throw new Error('useQuestions must be used whitin an QuestionsProvider');
  }

  return context;
}

export const QuestionsProvider = ({children}) => {
  const [loading, setLoading] = useState(true);
  const [questions, setQuestions] = useState([]);

  const getQuestions = search => {
    setLoading(true);
    try {
      if (search) {
        firestore()
          .collection('questions')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let QuestionsArr = [];
            if (querySnapshot) {
              querySnapshot.forEach(documentSnapshot => {
                QuestionsArr.push({
                  ...documentSnapshot.data(),
                  key: documentSnapshot.id,
                });
              });
            }
            setQuestions(QuestionsArr);
            setLoading(false);
          });
      } else {
        firestore()
          .collection('questions')
          .onSnapshot(querySnapshot => {
            let questionsArr = [];
            querySnapshot.forEach(documentSnapshot => {
              questionsArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setQuestions(questionsArr);
            setLoading(false);
          });
      }
      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  };

  useEffect(() => {
    getQuestions();
  }, []);

  return (
    <QuestionsContext.Provider
      value={{
        questions: questions && questions.length ? questions : [],
        loadingQuestions: loading,
        getQuestions,
      }}>
      {children}
    </QuestionsContext.Provider>
  );
};

QuestionsProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
