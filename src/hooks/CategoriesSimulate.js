import React, {createContext, useState, useContext, useEffect} from 'react';
import firestore from '@react-native-firebase/firestore';
const CategoriesSimulateContext = createContext({});

export function useCategoriesSimulate() {
  const context = useContext(CategoriesSimulateContext);

  if (!context) {
    throw new Error(
      'useCategoriesSimulate must be used whitin an CategoriesSimulateProvider',
    );
  }

  return context;
}

export const CategoriesSimulateProvider = ({children}) => {
  const [loading, setLoading] = useState(true);
  const [categoriesSimulate, setCategoriesSimulate] = useState([]);

  const getCategoriesSimulate = search => {
    setLoading(true);
    try {
      if (search) {
        firestore()
          .collection('categories_simulate')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let categoriesArr = [];
            querySnapshot.forEach(documentSnapshot => {
              categoriesArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setCategoriesSimulate(categoriesArr);
            setLoading(false);
          });
      } else {
        firestore()
          .collection('categories_simulate')
          .onSnapshot(querySnapshot => {
            let QuestionsArr = [];
            querySnapshot.forEach(documentSnapshot => {
              QuestionsArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setCategoriesSimulate(QuestionsArr);
            setLoading(false);
          });
      }
      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  };

  useEffect(() => {
    getCategoriesSimulate();
  }, []);

  return (
    <CategoriesSimulateContext.Provider
      value={{
        categoriesSimulate:
          categoriesSimulate && categoriesSimulate.length
            ? categoriesSimulate
            : [],
        loadingCategories: loading,
        getCategoriesSimulate,
      }}>
      {children}
    </CategoriesSimulateContext.Provider>
  );
};
