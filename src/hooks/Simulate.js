import React, {createContext, useState, useContext, useEffect} from 'react';
import firestore from '@react-native-firebase/firestore';

const SimulateContext = createContext({});

export function useSimulate() {
  const context = useContext(SimulateContext);

  if (!context) {
    throw new Error('useSimulate must be used whitin an SimulateProvider');
  }

  return context;
}

export const SimulateProvider = ({children}) => {
  const [loading, setLoading] = useState(true);
  const [simulates, setSimulate] = useState([]);

  const getSimulate = search => {
    setLoading(true);
    try {
      if (search) {
        firestore()
          .collection('simulates')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let categoriesArr = [];
            querySnapshot.forEach(documentSnapshot => {
              categoriesArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setSimulate(categoriesArr);
            setLoading(false);
          });
      } else {
        firestore()
          .collection('simulates')
          .onSnapshot(querySnapshot => {
            let categoriesArr = [];
            querySnapshot.forEach(documentSnapshot => {
              const doc = documentSnapshot.data();
              if (doc.active === true || doc.active === 'true') {
                categoriesArr.push({
                  ...documentSnapshot.data(),
                  key: documentSnapshot.id,
                });
              }
            });
            setSimulate(categoriesArr);
            setLoading(false);
          });
      }
      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  };

  useEffect(() => {
    getSimulate();
  }, []);

  return (
    <SimulateContext.Provider
      value={{
        simulates: simulates && simulates.length ? simulates : [],
        loadingSimulate: loading,
        getSimulate,
      }}>
      {children}
    </SimulateContext.Provider>
  );
};
