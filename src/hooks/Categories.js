import React, {createContext, useState, useContext, useEffect} from 'react';
import firestore from '@react-native-firebase/firestore';

const CategoriesContext = createContext({});

export function useCategories() {
  const context = useContext(CategoriesContext);

  if (!context) {
    throw new Error('useCategories must be used whitin an CategoriesProvider');
  }

  return context;
}

export const CategoriesProvider = ({children}) => {
  const [loading, setLoading] = useState(true);
  const [categories, setCategories] = useState([]);

  const getCategories = search => {
    setLoading(true);
    try {
      if (search) {
        firestore()
          .collection('categories')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let categoriesArr = [];
            if (querySnapshot) {
              querySnapshot.forEach(documentSnapshot => {
                categoriesArr.push({
                  ...documentSnapshot.data(),
                  key: documentSnapshot.id,
                });
              });
            }
            setCategories(categoriesArr);
            setLoading(false);
          });
      } else {
        firestore()
          .collection('categories')
          .onSnapshot(querySnapshot => {
            let categoriesArr = [];
            querySnapshot.forEach(documentSnapshot => {
              categoriesArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setCategories(categoriesArr);
            setLoading(false);
          });
      }
      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  };

  useEffect(() => {
    getCategories();
  }, []);

  return (
    <CategoriesContext.Provider
      value={{
        categories: categories && categories.length ? categories : [],
        loadingCategories: loading,
        getCategories,
      }}>
      {children}
    </CategoriesContext.Provider>
  );
};
