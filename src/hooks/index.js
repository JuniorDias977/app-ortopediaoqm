import React from 'react';

import {AuthProvider} from './Auth';
import {CategoriesProvider} from './Categories';
import {SubCategoriesProvider} from './SubCategories';
import {SubGroupsProvider} from './SubGroups';
import {QuestionsProvider} from './Questions';
import {CategoriesSimulateProvider} from './CategoriesSimulate';
import {SimulateProvider} from './Simulate';
import {QuestionsSimulateProvider} from './QuestionsSimulate';
import {SimulateUserProvider} from './SimulateUser';
import {SimulateMainProvider} from './SimulateMain';
import {HistoryUserProvider} from './History';

const AppProvider = ({children}) => {
  return (
    <AuthProvider>
      <SimulateMainProvider>
        <CategoriesSimulateProvider>
          <QuestionsSimulateProvider>
            <SimulateProvider>
              <CategoriesProvider>
                <SubCategoriesProvider>
                  <SubGroupsProvider>
                    <QuestionsProvider>
                      <SimulateUserProvider>
                        <HistoryUserProvider>{children}</HistoryUserProvider>
                      </SimulateUserProvider>
                    </QuestionsProvider>
                  </SubGroupsProvider>
                </SubCategoriesProvider>
              </CategoriesProvider>
            </SimulateProvider>
          </QuestionsSimulateProvider>
        </CategoriesSimulateProvider>
      </SimulateMainProvider>
    </AuthProvider>
  );
};

export default AppProvider;
