import React, {
  createContext,
  useState,
  useContext,
  useEffect,
  useCallback,
} from 'react';
import firestore from '@react-native-firebase/firestore';

import {useAuth} from './Auth';

const SimulateMainContext = createContext({});

export function useSimulateMain() {
  const context = useContext(SimulateMainContext);

  if (!context) {
    throw new Error(
      'useSimulateMain must be used whitin an SimulateMainProvider',
    );
  }

  return context;
}

export const SimulateMainProvider = ({children}) => {
  const [loading, setLoading] = useState(true);
  const [simulateMain, setSimulateMain] = useState([]);
  const {user} = useAuth();

  const getSimulateMain = useCallback(
    search => {
      setLoading(true);
      try {
        if (search) {
          firestore()
            .collection('simulate_main_user_questions')
            .where('name', '==', search)
            .onSnapshot(querySnapshot => {
              let simulateMainArr = [];
              querySnapshot.forEach(documentSnapshot => {
                simulateMainArr.push({
                  ...documentSnapshot.data(),
                  key: documentSnapshot.id,
                });
              });
              setSimulateMain(simulateMainArr);
              setLoading(false);
            });
        } else {
          firestore()
            .collection('simulate_main_user_questions')
            .where('user', '==', user.key)
            .onSnapshot(querySnapshot => {
              let simulateMainArr = [];
              querySnapshot.forEach(documentSnapshot => {
                simulateMainArr.push({
                  ...documentSnapshot.data(),
                  key: documentSnapshot.id,
                });
              });
              setSimulateMain(simulateMainArr);
              setLoading(false);
            });
        }
        setLoading(false);
      } catch (err) {
        setLoading(false);
      }
    },
    [user],
  );

  useEffect(() => {
    getSimulateMain();
  }, [getSimulateMain]);

  return (
    <SimulateMainContext.Provider
      value={{
        simulateMain: simulateMain && simulateMain.length ? simulateMain : [],
        loadingSimulateMain: loading,
        getSimulateMain,
      }}>
      {children}
    </SimulateMainContext.Provider>
  );
};
