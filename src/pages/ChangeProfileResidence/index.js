import React, {useCallback, useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {Alert, KeyboardAvoidingView, Platform, ScrollView} from 'react-native';
import IconF from 'react-native-vector-icons/Feather';
import firestore from '@react-native-firebase/firestore';
import {Checkbox} from 'react-native-paper';

import Loading from '../../components/Loading';
import {useAuth} from '../../hooks/Auth';
import {colors} from '../../global';

import {
  Container,
  Content,
  Row,
  ButtonBack,
  HeaderTitle,
  RowTwo,
} from './styles';

const SignIn = () => {
  const {user, updateUser} = useAuth();
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
  const [checkedROne, setCheckedROne] = useState(false);
  const [checkedRTwo, setCheckedRTwo] = useState(false);
  const [checkedRThree, setCheckedRThree] = useState(false);

  useEffect(() => {
    if (user && user.residence) {
      if (user.residence === 'R1') {
        setCheckedROne(true);
      } else if (user.residence === 'R2') {
        setCheckedRTwo(true);
      } else {
        setCheckedRThree(true);
      }
    }
  }, [user]);

  const handleCheckedResidence = useCallback(() => {
    if (checkedROne) {
      return 'R1';
    }
    if (checkedRTwo) {
      return 'R2';
    }
    return 'R3';
  }, [checkedROne, checkedRTwo]);

  const handleEdit = useCallback(async () => {
    try {
      if (!checkedROne && !checkedRTwo && !checkedRThree) {
        Alert.alert('ERRO', 'Selecione seu nível de residência');
        return;
      }
      setLoading(true);
      firestore().collection('users').doc(user.key).update({
        residence: handleCheckedResidence(),
      });
      updateUser(user);
      navigation.goBack();
    } catch (error) {
      setLoading(false);
    }
  }, [
    user,
    updateUser,
    navigation,
    handleCheckedResidence,
    checkedROne,
    checkedRTwo,
    checkedRThree,
  ]);

  return loading ? (
    <Loading />
  ) : (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <ScrollView
          style={{flex: 1}}
          contentContainerStyle={{flexGrow: 1}}
          showsVerticalScrollIndicator={false}>
          <Content>
            <Row>
              <ButtonBack onPress={() => navigation.goBack()}>
                <IconF name="x" color={colors.white_correct} size={30} />
              </ButtonBack>
              <HeaderTitle>Alterar Residência</HeaderTitle>
              <ButtonBack
                onPress={() => handleEdit()}
                style={{marginLeft: 'auto'}}>
                <IconF name="check" color={colors.orange} size={30} />
              </ButtonBack>
            </Row>
            <RowTwo>
              <HeaderTitle>R1</HeaderTitle>
              <Checkbox
                color={colors.orange}
                uncheckedColor={colors.white}
                status={checkedROne ? 'checked' : 'unchecked'}
                onPress={() => {
                  setCheckedROne(!checkedROne);
                  setCheckedRTwo(false);
                  setCheckedRThree(false);
                }}
              />
            </RowTwo>
            <RowTwo>
              <HeaderTitle>R2</HeaderTitle>
              <Checkbox
                color={colors.orange}
                uncheckedColor={colors.white}
                status={checkedRTwo ? 'checked' : 'unchecked'}
                onPress={() => {
                  setCheckedRTwo(!checkedRTwo);
                  setCheckedROne(false);
                  setCheckedRThree(false);
                }}
              />
            </RowTwo>
            <RowTwo>
              <HeaderTitle>R3</HeaderTitle>
              <Checkbox
                color={colors.orange}
                uncheckedColor={colors.white}
                status={checkedRThree ? 'checked' : 'unchecked'}
                onPress={() => {
                  setCheckedRThree(!checkedRThree);
                  setCheckedROne(false);
                  setCheckedRTwo(false);
                }}
              />
            </RowTwo>
          </Content>
        </ScrollView>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default SignIn;
