/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {KeyboardAvoidingView, Platform, ScrollView, View} from 'react-native';

import Button from '../../components/Button';

import {
  Container,
  Content,
  TitlePage,
  HeaderTitle,
  TextInfo,
  IconStyled,
  // TextButtonSignIn,
  // ButtonSignIn,
} from './styles';
import {colors} from '../../global';

const SignIn = () => {
  const navigation = useNavigation();

  return (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <ScrollView
          style={{flex: 1}}
          contentContainerStyle={{flexGrow: 1}}
          showsVerticalScrollIndicator={false}>
          <Content>
            <HeaderTitle>Cadastro Realizado</HeaderTitle>
            <View
              style={{
                width: 98,
                height: 98,
                backgroundColor: 'rgba(255, 153, 0, 0.2)',
                borderRadius: 49,
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 32,
              }}>
              <IconStyled name="check" size={65} color={colors.orange} />
            </View>
            <TextInfo>
              Parabéns Seu cadastro foi realizado com sucesso.{' '}
              {Platform.OS === 'ios'
                ? null
                : 'Algumas funcionalidades são exclusivas de nossos alunos, se quiser ter acesso a todas elas, basta acessar alguma página restrita.'}
            </TextInfo>
            <Button
              onPress={() => navigation.navigate('SignIn')}
              style={{marginTop: 'auto'}}>
              Começar a estudar
            </Button>
          </Content>
        </ScrollView>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default SignIn;
