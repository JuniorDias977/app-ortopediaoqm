import React, {useCallback, useState, useEffect} from 'react';
import firestore from '@react-native-firebase/firestore';
import {Dimensions} from 'react-native';
// import crashlytics from '@react-native-firebase/crashlytics';

import {useAuth} from '../../../../hooks/Auth';
import {colors} from '../../../../global';
const {width} = Dimensions.get('window');

import {
  Container,
  Title,
  Icon,
  TouchableOpacity,
  RowItem,
  TextAnser,
  TitleCategory,
} from './styles';
import {ScrollView} from 'react-native';
import {useQuestions} from '../../../../hooks/Questions';
import {ActivityIndicator} from 'react-native-paper';

const Item = ({
  item,
  index,
  onButtonPress,
  lastIndex,
  selected,
  setCorrect,
  setErrored,
  setLoading,
}) => {
  const {user} = useAuth();
  const [splited, setSplited] = useState(true);
  const {questions} = useQuestions();
  const [questionData, setQuestionData] = useState();
  const [loadingLocale, setLoadingLocale] = useState(true);

  useEffect(() => {
    try {
      setLoadingLocale(true);
      if (item && item.key) {
        setQuestionData(
          questions && questions.length
            ? questions.find(qt => qt.key === item.key)
            : null,
        );
      }
      setLoadingLocale(false);
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, [item, questions]);

  useEffect(() => {
    try {
      firestore()
        .collection('users')
        .where('key', '==', user.key)
        .onSnapshot(querySnapshot => {
          let usersArr = [];
          querySnapshot.forEach(documentSnapshot => {
            usersArr.push({
              ...documentSnapshot.data(),
              key: documentSnapshot.id,
            });
          });
        });
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleNext = useCallback(
    async val => {
      try {
        setLoading(true);
        if (index !== lastIndex) {
          onButtonPress(index + 1, val, false);
          setCorrect(false);
          setErrored(false);
          setSplited(true);
        } else {
          onButtonPress(index, val, true);
        }
      } catch (error) {
        console.log(error);
        // crashlytics().recordError(error);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [index, onButtonPress, lastIndex, setCorrect, setErrored],
  );

  const handleText = useCallback((val, splt) => {
    if (val.length > 200 && splt) {
      return `${val.slice(0, 200)}...`;
    }
    if (val.length > 200 && !splt) {
      return val;
    }

    return val;
  }, []);

  return loadingLocale && !questionData ? (
    <Container>
      <ActivityIndicator size="large" color={colors.white} />
    </Container>
  ) : (
    <Container>
      <ScrollView
        style={{flex: 1}}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          flexGrow: 1,
          width: width - 48,
        }}>
        <>
          <TitleCategory>
            {questionData && questionData.category
              ? questionData.category.label
              : 'Sem categoria'}
            {questionData && questionData.sub_category
              ? `/${questionData.sub_category.label}`
              : ''}
          </TitleCategory>

          <Title>
            {handleText(
              questionData && questionData.description
                ? questionData.description
                : '',
              splited,
            )}
          </Title>

          {questionData &&
          questionData.description &&
          questionData.description.length > 200 ? (
            <TouchableOpacity onPress={() => setSplited(!splited)}>
              <Icon
                name={!splited ? 'arrow-up' : 'arrow-down'}
                size={22}
                color={colors.white}
              />
            </TouchableOpacity>
          ) : null}
        </>
        <RowItem
          selected={selected === 'a'}
          onPress={() => {
            handleNext('a');
            if (questionData && questionData.is_correct_a) {
              setCorrect(true);
              setErrored(false);
            } else {
              setErrored(true);
              setCorrect(false);
            }
            // setClicked('a');
            // setHidden(true);
            // setDisabled(true);
          }}>
          <TextAnser selected={selected === 'a'}>
            {questionData && questionData.alternative_a
              ? questionData.alternative_a
              : ''}
          </TextAnser>
        </RowItem>
        <RowItem
          selected={selected === 'b'}
          onPress={() => {
            handleNext('b');
            if (questionData && questionData.is_correct_b) {
              setCorrect(true);
              setErrored(false);
            } else {
              setErrored(true);
              setCorrect(false);
            }
            // setClicked('b');
            // setHidden(true);
            // setDisabled(true);
          }}>
          <TextAnser selected={selected === 'b'}>
            {questionData && questionData.alternative_b
              ? questionData.alternative_b
              : ''}
          </TextAnser>
        </RowItem>
        <RowItem
          selected={selected === 'c'}
          onPress={() => {
            handleNext('c');
            if (questionData && questionData.is_correct_c) {
              setCorrect(true);
              setErrored(false);
            } else {
              setErrored(true);
              setCorrect(false);
            }
            // setClicked('c');
            // setHidden(true);
            // setDisabled(true);
          }}>
          <TextAnser selected={selected === 'c'}>
            {questionData && questionData.alternative_c
              ? questionData.alternative_c
              : ''}
          </TextAnser>
        </RowItem>
        <RowItem
          selected={selected === 'd'}
          onPress={() => {
            handleNext('d');
            if (questionData && questionData.is_correct_d) {
              setCorrect(true);
              setErrored(false);
            } else {
              setErrored(true);
              setCorrect(false);
            }
            // setClicked('d');
            // setHidden(true);
            // setDisabled(true);
          }}>
          <TextAnser selected={selected === 'd'}>
            {questionData && questionData.alternative_d
              ? questionData.alternative_d
              : ''}
          </TextAnser>
        </RowItem>
      </ScrollView>
    </Container>
  );
};

export default Item;
