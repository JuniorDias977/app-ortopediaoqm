import React, {useEffect, useState} from 'react';
import {KeyboardAvoidingView, Platform} from 'react-native';

import Loading from '../../components/Loading';
import {useCategories} from '../../hooks/Categories';

import {
  Container,
  Content,
  Row,
  HeaderTitle,
  FlatListStyled,
  HeaderLabel,
} from './styles';
import Item from './Components/Item';

const SignIn = () => {
  const {categories, loadingCategories} = useCategories();
  const [categoriesData, setCategoriesData] = useState([]);

  useEffect(() => {
    setCategoriesData(categories.filter(cat => cat.active));
  }, [categories]);

  return loadingCategories ? (
    <Loading />
  ) : (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <Content>
          <Row>
            <HeaderTitle>Qual tema você deseja estudar hoje?</HeaderTitle>
          </Row>
          {/* <HeaderLabel>
            Questões deste aplicativo foram retiradas da Sociedade Brasileira de
            Ortopedia e Traumatologia, utilizando as referências oficiais da
            SBOT.
          </HeaderLabel> */}
          <FlatListStyled
            showsVerticalScrollIndicator={false}
            data={categoriesData && categoriesData.length ? categoriesData : []}
            renderItem={({item}) => <Item item={item} />}
            keyExtractor={(item, index) => index.toString()}
          />
        </Content>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default SignIn;
