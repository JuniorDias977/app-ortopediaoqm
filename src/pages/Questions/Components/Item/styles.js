import styled from 'styled-components';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';

import {colors, fonts} from '../../../../global';

export const TouchableOpacity = styled.View`
  position: absolute;
  right: 18px;
  top: 61%;
`;

export const Icon = styled(SimpleLineIcon)``;

export const Container = styled.TouchableOpacity`
  border-bottom-width: 1px;
  border-bottom-color: #333333;
  margin-top: 12px;
  align-items: center;
  justify-content: flex-start;
  padding: 10px 0 20px;
`;

export const Title = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyBold};
  text-align: left;
  margin-top: 8%;
  width: 70%;
  margin-left: 10%;
`;

export const Logo = styled.Image.attrs({
  source: require('../../../../assets/log-one.png'),
  resizeMode: 'contain',
})`
  position: absolute;
  width: 56px;
  height: 56px;
  top: 24px;
  left: 0;
`;

export const LogoIcon = styled.Image.attrs({
  resizeMode: 'contain',
})`
  position: absolute;
  width: 56px;
  height: 56px;
  top: 24px;
  left: 0;
`;
