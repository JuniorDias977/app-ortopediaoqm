import React, {useEffect, useState, useCallback} from 'react';
import {KeyboardAvoidingView, Platform, ScrollView, View} from 'react-native';
import {ProgressBar} from 'react-native-paper';
import {useFocusEffect, useNavigation} from '@react-navigation/native';
// import crashlytics from '@react-native-firebase/crashlytics';

import Loading from '../../components/Loading';
import {useCategoriesSimulate} from '../../hooks/CategoriesSimulate';
import {useSimulate} from '../../hooks/Simulate';
import {useSimulateUser} from '../../hooks/SimulateUser';
import {useQuestionsSimulate} from '../../hooks/QuestionsSimulate';
import {useSimulateMain} from '../../hooks/SimulateMain';
import {useCategories} from '../../hooks/Categories';
import {useQuestions} from '../../hooks/Questions';

import {
  Container,
  Content,
  ContainerHeader,
  Logo,
  HeaderName,
  Spotlight,
  IconStyled,
  TextTitle,
  TextAmount,
  TextDuration,
  Row,
  LogoTwo,
  SimulateTitle,
  CardSimulate,
  TextTitleTwo,
} from './styles';
import {colors} from '../../global';
import reactotron from 'reactotron-react-native';

const SignIn = () => {
  const navigation = useNavigation();

  const {categories, loadingCategories} = useCategories();
  const {questions, loadingQuestions} = useQuestions();

  const {simulateMain, loadingSimulateMain, getSimulateMain} =
    useSimulateMain();

  const {categoriesSimulate, loadingCategoriesSimulate} =
    useCategoriesSimulate();

  const {simulatesUser, loadingSimulateUser} = useSimulateUser();
  const {simulates, loadingSimulate} = useSimulate();
  const {questionsSimulate, loadingQuestionsSimulate} = useQuestionsSimulate();

  const [categoriesSimulateData, setCategoriesSimulateData] = useState([]);
  const [simulateData, setSimulateData] = useState([]);
  const [simulateUserData, setSimulateUserData] = useState([]);
  const [questionsSimulateData, setQuestionsSimulateData] = useState([]);

  const [loading, setLoading] = useState(true);

  function getRandom(arr, n) {
    try {
      if (n > 0 && arr.length > 0) {
        var result = new Array(n),
          len = arr.length,
          taken = new Array(len);
        if (n > len) {
          throw new RangeError('getRandom: more elements taken than available');
        }
        while (n--) {
          var x = Math.floor(Math.random() * len);
          result[n] = arr[x in taken ? taken[x] : x];
          taken[x] = --len in taken ? taken[len] : len;
        }
        return result;
      }
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }

  useEffect(() => {
    try {
      if (!loadingQuestions && !loadingCategories && questions && categories) {
        let val = 100 / categories.length;
        let arr = [];
        if (
          questions.filter(qt => qt.active && !qt.category).length &&
          questions.filter(qt => qt.active && !qt.category).length > val
        ) {
          arr = getRandom(
            questions.filter(qt => qt.active && !qt.category),
            parseInt(val, 10),
          );
        } else {
          arr = [...questions.filter(qt => qt.active && !qt.category)];
        }
        categories.forEach(cat => {
          let catArr = [];
          catArr = questions.filter(
            qt => qt.active && qt.category.value === cat.key,
          );
          if (catArr.length && catArr.length > val) {
            catArr = [...getRandom(catArr, parseInt(val, 10))];
          }
          arr = [...arr, ...catArr];
        });
        if (arr.length === 100) {
          setQuestionsSimulateData(arr);
        } else if (arr.length < 100 && questions.length >= 100) {
          const newVal = 100 - arr.length;
          let newQuestions = [];
          questions.forEach(qt => {
            if (!arr.find(ar => ar.key === qt.key)) {
              newQuestions.push(qt);
            }
          });
          arr = [...arr, ...getRandom(newQuestions, newVal)];

          reactotron.log(arr, 'QUESTION SIMULATE');
          setQuestionsSimulateData(arr);
        } else {
          setQuestionsSimulateData(arr);
        }
      }
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, [questions, categories, loadingCategories, loadingQuestions]);

  useEffect(() => {
    try {
      if (simulatesUser && simulatesUser.length) {
        let arrUserSimulates = [];
        simulatesUser.map(sim => {
          simulateData.map(sm => {
            if (sim.simulate === sm.key && sim.status === 'in_progress') {
              arrUserSimulates.push({
                ...sm,
                questions_solved: sim.questions_solved,
              });
            }
          });
          if (
            sim.simulate === 'simulate_main' &&
            sim.status === 'in_progress'
          ) {
            const simFinddd = simulateMain.find(sm => sm.active);
            const questionsD = simFinddd ? simFinddd.questions : [];
            const obj = {
              ...sim,
              name: 'Simulado geral',
              questions_solved: sim.questions_solved,
              duration: '03:30',
              questions: questionsD,
            };
            if (!arrUserSimulates.find(ar => ar.name === obj.name)) {
              arrUserSimulates.push(obj);
            }
          }
        });

        reactotron.log(arrUserSimulates, 'USER SIMULATES');
        setSimulateUserData(arrUserSimulates);
      }
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, [simulatesUser, simulateData, simulateMain]);

  useEffect(() => {
    try {
      setCategoriesSimulateData(categoriesSimulate.filter(cat => cat.active));
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, [categoriesSimulate]);

  useEffect(() => {
    try {
      setSimulateData(simulates.filter(sim => sim.active));
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, [simulates]);

  useFocusEffect(
    useCallback(() => {
      try {
        setLoading(true);
        getSimulateMain();
        setLoading(false);
      } catch (error) {
        console.log(error);
        // crashlytics().recordError(error);
      }
    }, [getSimulateMain]),
  );

  const handleSimulateUser = useCallback(() => {
    try {
      return simulateUserData.map(simulate =>
        simulate.name === 'Simulado geral' ? (
          <CardSimulate
            onPress={() =>
              navigation.navigate('InitSimulateMain', {
                item: {
                  key: 'simulate_main',
                  questionsSimulateData,
                  questions: simulate.questions.length,
                  duration: '03:30',
                },
              })
            }>
            <TextTitleTwo>{simulate.name}</TextTitleTwo>
            <ProgressBar
              progress={
                simulate.questions_solved
                  ? simulate.questions_solved / simulate.questions.length < 100
                    ? simulate.questions_solved / simulate.questions.length
                    : 0
                  : 0
              }
              color={colors.orange}
              style={{
                backgroundColor: 'rgba(241, 241, 241, 0.4)',
                minWidth: 128,
              }}
            />
            <TextAmount style={{width: '100%'}}>
              {simulate.questions.length === 1
                ? `${simulate.questions.length} questão`
                : `${simulate.questions.length} questões`}
            </TextAmount>
            <Row>
              <IconStyled
                name="clock"
                size={20}
                color="rgba(241, 241, 241, 0.4);"
              />
              <TextDuration>{simulate.duration}</TextDuration>
            </Row>
          </CardSimulate>
        ) : (
          <CardSimulate
            onPress={() =>
              navigation.navigate('InitSimulate', {
                item: {
                  ...simulate,
                  questions: questionsSimulate.filter(
                    qt => qt.simulate.value === simulate.key,
                  ).length,
                },
              })
            }>
            <TextTitleTwo>{simulate.name}</TextTitleTwo>
            <ProgressBar
              progress={
                simulate.questions_solved
                  ? simulate.questions_solved /
                      questionsSimulate.filter(
                        qt => qt.simulate.value === simulate.key,
                      ).length <
                    100
                    ? simulate.questions_solved /
                      questionsSimulate.filter(
                        qt => qt.simulate.value === simulate.key,
                      ).length
                    : 0
                  : 0
              }
              color={colors.orange}
              style={{
                backgroundColor: 'rgba(241, 241, 241, 0.4)',
                minWidth: 128,
              }}
            />
            <TextAmount style={{width: '100%'}}>
              {questionsSimulate.filter(
                qt => qt.simulate.value === simulate.key,
              ).length === 1
                ? `${
                    questionsSimulate.filter(
                      qt => qt.simulate.value === simulate.key,
                    ).length
                  } questão`
                : `${
                    questionsSimulate.filter(
                      qt => qt.simulate.value === simulate.key,
                    ).length
                  } questões`}
            </TextAmount>
            <Row>
              <IconStyled
                name="clock"
                size={20}
                color="rgba(241, 241, 241, 0.4);"
              />
              <TextDuration>{simulate.duration}</TextDuration>
            </Row>
          </CardSimulate>
        ),
      );
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, [simulateUserData, navigation, questionsSimulate, questionsSimulateData]);

  const handleSimulate = useCallback(
    cat => {
      try {
        return simulateData.map(simulate =>
          simulate.category.value === cat.key ? (
            <CardSimulate
              onPress={() =>
                navigation.navigate('InitSimulate', {
                  item: {
                    ...simulate,
                    questions: questionsSimulate.filter(
                      qt => qt.simulate.value === simulate.key,
                    ).length,
                  },
                })
              }>
              <TextTitleTwo>{simulate.name}</TextTitleTwo>
              <ProgressBar
                progress={
                  simulateUserData.find(sm => sm.key === simulate.key)
                    ? simulateUserData.find(sm => sm.key === simulate.key)
                        .questions_solved /
                        questionsSimulate.filter(
                          qt => qt.simulate.value === simulate.key,
                        ).length <
                      100
                      ? simulateUserData.find(sm => sm.key === simulate.key)
                          .questions_solved /
                        questionsSimulate.filter(
                          qt => qt.simulate.value === simulate.key,
                        ).length
                      : 0
                    : 0
                }
                color={colors.orange}
                style={{
                  backgroundColor: 'rgba(241, 241, 241, 0.4)',
                  minWidth: 128,
                }}
              />
              <TextAmount style={{width: '100%'}}>
                {questionsSimulate.filter(
                  qt => qt.simulate.value === simulate.key,
                ).length === 1
                  ? `${
                      questionsSimulate.filter(
                        qt => qt.simulate.value === simulate.key,
                      ).length
                    } questão`
                  : `${
                      questionsSimulate.filter(
                        qt => qt.simulate.value === simulate.key,
                      ).length
                    } questões`}
              </TextAmount>
              <Row>
                <IconStyled
                  name="clock"
                  size={20}
                  color="rgba(241, 241, 241, 0.4);"
                />
                <TextDuration>{simulate.duration}</TextDuration>
              </Row>
            </CardSimulate>
          ) : null,
        );
      } catch (error) {
        console.log(error);
        // crashlytics().recordError(error);
      }
    },
    [simulateData, navigation, questionsSimulate, simulateUserData],
  );

  return loadingCategoriesSimulate ||
    loadingSimulateMain ||
    loadingSimulateUser ||
    loadingSimulate ||
    loadingQuestionsSimulate ||
    loadingCategories ||
    loading ||
    loadingQuestions ? (
    <Loading />
  ) : (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <ContainerHeader>
        <Logo />
        <HeaderName>Simulados</HeaderName>
      </ContainerHeader>
      <Container>
        <ScrollView
          contentContainerStyle={{flexGrow: 1}}
          style={{width: '100%'}}>
          <Content>
            <Spotlight
              onPress={() =>
                questionsSimulateData && questionsSimulateData.length
                  ? navigation.navigate('InitSimulateMain', {
                      item: {
                        key: 'simulate_main',
                        questionsSimulateData,
                        questions: questionsSimulateData.length,
                        duration: '03:30',
                      },
                    })
                  : null
              }>
              <LogoTwo />
              <TextTitle>Simulado especial da Ortopedia OQM</TextTitle>
              <TextAmount>
                {questions.length < 100 ? questions.length : '100'} questões
              </TextAmount>
              <Row>
                <IconStyled
                  name="clock"
                  size={20}
                  color="rgba(241, 241, 241, 0.4);"
                />
                <TextDuration>03:30</TextDuration>
              </Row>
            </Spotlight>
          </Content>
          <View style={{paddingBottom: 80}}>
            {simulateUserData && simulateUserData.length ? (
              <>
                <SimulateTitle style={{paddingLeft: 24}}>
                  Simulados em andamento
                </SimulateTitle>
                <Row style={{paddingLeft: 24, width: '100%'}}>
                  <ScrollView
                    horizontal
                    contentContainerStyle={{flexGrow: 1}}
                    showsHorizontalScrollIndicator={false}
                    style={{width: '100%'}}>
                    {handleSimulateUser()}
                  </ScrollView>
                </Row>
              </>
            ) : null}
            {categoriesSimulateData && categoriesSimulateData.length
              ? categoriesSimulateData.map(cat => (
                  <>
                    <SimulateTitle style={{paddingLeft: 24}}>
                      {cat.name}
                    </SimulateTitle>
                    <Row
                      style={{
                        paddingLeft: 24,
                        width: '100%',
                        flex: 1,
                      }}>
                      <ScrollView
                        horizontal
                        contentContainerStyle={{flexGrow: 1}}
                        showsHorizontalScrollIndicator={false}
                        style={{width: '100%'}}>
                        {handleSimulate(cat)}
                      </ScrollView>
                    </Row>
                  </>
                ))
              : null}
          </View>
        </ScrollView>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default SignIn;
