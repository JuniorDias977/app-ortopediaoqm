import React, {useCallback, useState} from 'react';
import {Formik} from 'formik';
import {useNavigation} from '@react-navigation/native';
import {Alert, KeyboardAvoidingView, Platform, ScrollView} from 'react-native';
import IconF from 'react-native-vector-icons/Feather';
import auth from '@react-native-firebase/auth';

import Loading from '../../components/Loading';
import {useAuth} from '../../hooks/Auth';
import Input from '../../components/Input';
import {colors} from '../../global';

import {
  Container,
  Content,
  Row,
  ButtonBack,
  HeaderTitle,
  HeaderLabel,
} from './styles';

const SignIn = () => {
  const {language, user} = useAuth();
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
  const [oldPassword, setOldPassword] = useState(false);
  const [password, setPassword] = useState(false);
  const [confirmpassword, setConfirmPassword] = useState(false);

  const handleEdit = useCallback(async () => {
    try {
      if (!oldPassword) {
        Alert.alert('ERRO', 'Insira sua senha atual');
        return;
      }
      if (!password) {
        Alert.alert('ERRO', 'Insira sua senha');
        return;
      }
      if (!confirmpassword) {
        Alert.alert('ERRO', 'Confirme sua senha');
        return;
      }
      if (password !== confirmpassword) {
        Alert.alert('ERRO', 'As senhas não são iguais');
        return;
      }
      setLoading(true);
      const emailCred = auth.EmailAuthProvider.credential(
        user.email,
        oldPassword,
      );
      auth()
        .currentUser.reauthenticateWithCredential(emailCred)
        .then(() => {
          // User successfully reauthenticated.
          auth().currentUser.updatePassword(password);
        })
        .catch(() => {
          Alert.alert('ERRO', 'Houve um erro inesperado.');
        });

      navigation.goBack();
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  }, [password, user, navigation, confirmpassword, oldPassword]);

  return loading ? (
    <Loading />
  ) : (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <ScrollView
          style={{flex: 1}}
          contentContainerStyle={{flexGrow: 1}}
          showsVerticalScrollIndicator={false}>
          <Content>
            <Row>
              <ButtonBack onPress={() => navigation.goBack()}>
                <IconF name="x" color={colors.white_correct} size={30} />
              </ButtonBack>
              <HeaderTitle>Alterar senha</HeaderTitle>
              <ButtonBack
                onPress={() => handleEdit()}
                style={{marginLeft: 'auto'}}>
                <IconF name="check" color={colors.orange} size={30} />
              </ButtonBack>
            </Row>
            <Formik initialValues={{name: user.name}} onSubmit={handleEdit}>
              {({handleBlur, values, errors, setFieldValue}) => (
                <>
                  <HeaderLabel>Senha atual</HeaderLabel>
                  <Input
                    name="oldPass"
                    placeholder={
                      language ? 'Type your e-mail' : 'Digite sua senha atual'
                    }
                    autoCorrect={false}
                    password
                    autoCapitalize="none"
                    onChangeText={e => {
                      setFieldValue('oldPass', e);
                      setOldPassword(e);
                    }}
                    onBlur={handleBlur('oldPass')}
                    value={values.oldPass}
                    error={errors.oldPass}
                  />
                  <HeaderLabel style={{marginTop: 24}}>Nova senha</HeaderLabel>
                  <Input
                    name="password"
                    placeholder={
                      language ? 'Type your e-mail' : 'Digite sua nova senha'
                    }
                    autoCorrect={false}
                    password
                    autoCapitalize="none"
                    onChangeText={e => {
                      setFieldValue('password', e);
                      setPassword(e);
                    }}
                    onBlur={handleBlur('password')}
                    value={values.password}
                    error={errors.password}
                  />
                  <HeaderLabel style={{marginTop: 24}}>
                    Repita a nova senha
                  </HeaderLabel>
                  <Input
                    name="confirmPassword"
                    password
                    placeholder={
                      language ? 'Type your e-mail' : 'Repita sua nova senha'
                    }
                    autoCorrect={false}
                    autoCapitalize="none"
                    onChangeText={e => {
                      setFieldValue('confirmPassword', e);
                      setConfirmPassword(e);
                    }}
                    onBlur={handleBlur('confirmPassword')}
                    value={values.confirmPassword}
                    error={errors.confirmPassword}
                  />
                </>
              )}
            </Formik>
          </Content>
        </ScrollView>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default SignIn;
