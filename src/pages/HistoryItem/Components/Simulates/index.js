import React, {useEffect, useState} from 'react';

import {Container, Content, TextError, TextCorrect, Row, Title} from './styles';
import {useHistoryUser} from '../../../../hooks/History';
import Loading from '../../../../components/Loading';

const History = ({item}) => {
  const [loading, setLoading] = useState(false);
  const {historyUser, loadingHistory} = useHistoryUser();
  const [questionsData, setQuestionsData] = useState([]);
  const idSim = item[0].simulateUserId;

  function groupBy(objectArray, property) {
    return objectArray.reduce(function (acc, obj) {
      var key = obj[property];
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(obj);
      return acc;
    }, {});
  }

  useEffect(() => {
    setLoading(true);
    if (historyUser && !loadingHistory) {
      const questions = historyUser.filter(itm => {
        if (itm.simulate) {
          return itm.simulate.key === idSim;
        }
      });

      if (questions) {
        let valueData = [];
        questions.forEach((itm, index) => {
          if (itm && itm.question && itm.question.category) {
            const obj = {
              category: {
                value: itm.question.category.value,
                label: itm.question.category.label,
              },
              correct: itm.correct ? 1 : 0,
              error: itm.errored ? 1 : 0,
            };
            // valueData = oldValue;
            valueData.push(obj);
          } else {
            const obj = {
              category: {
                value: 'others-category',
                label: 'Variados',
              },
              correct: itm.correct ? 1 : 0,
              error: itm.errored ? 1 : 0,
            };
            valueData.push(obj);
          }
        });
        let newArr = [];
        valueData.forEach(itm => {
          console.log(itm);
          const equals = newArr.find(
            i => i.category.label === itm.category.label,
          );
          if (equals) {
            const newObj = {
              category: {
                value: itm.category.value,
                label: itm.category.label,
              },
              correct: equals.correct + itm.correct,

              error: equals.error + itm.error,
            };
            newArr = newArr.filter(
              it => it.category.label !== itm.category.label,
            );
            newArr.push(newObj);
          } else {
            const newObj = {
              category: {
                value: itm.category.value,
                label: itm.category.label,
              },
              correct: itm.correct ? 1 : 0,
              error: itm.error ? 1 : 0,
            };
            newArr.push(newObj);
          }
        });
        setQuestionsData(newArr);
      }
      setLoading(false);
    }
  }, [historyUser, loadingHistory, item, idSim]);

  return loading || loadingHistory ? (
    <Loading />
  ) : (
    <Container>
      <Content>
        {questionsData && questionsData.length
          ? questionsData.map(itm => (
              <Row>
                <Title>{itm.category.label}</Title>
                <TextCorrect>{itm.correct}</TextCorrect>
                <TextError>{itm.error}</TextError>
              </Row>
            ))
          : null}
      </Content>
    </Container>
  );
};

export default History;
