import styled from 'styled-components/native';

import {colors, fonts} from '../../../../global';

export const Container = styled.View`
  flex: 1;
  background-color: ${colors.background_main};
  position: relative;
  padding: 24px 0 0;
  z-index: 999;
`;

export const Content = styled.ScrollView`
  flex: 1;
  padding: 0 24px;
`;

export const Title = styled.Text`
  font-size: ${fonts.regular};
  color: rgba(241, 241, 241, 0.6);
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  flex-basis: 70%;
`;

export const TextCorrect = styled.Text`
  font-size: 18px;
  color: #a9c21b;
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  flex-basis: 15%;
`;

export const TextError = styled.Text`
  font-size: 18px;
  color: #ff5630;
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  flex-basis: 15%;
`;

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
