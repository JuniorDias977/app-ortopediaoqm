import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';

import {
  Container,
  Content,
  QuestionItem,
  Row,
  QuestionItemText,
} from './styles';

import {useHistoryUser} from '../../../../hooks/History';
import Loading from '../../../../components/Loading';

const History = ({item}) => {
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
  const {historyUser, loadingHistory} = useHistoryUser();
  const [questionsData, setQuestionsData] = useState([]);
  const idSim = item[0].simulateUserId;

  useEffect(() => {
    setLoading(true);
    if (historyUser && !loadingHistory) {
      const questions = historyUser.filter((itm, index) => {
        if (itm.simulate) {
          return itm.simulate.key === idSim;
        }
      });

      if (questions) {
        setQuestionsData(questions);
      }
      setLoading(false);
    }
  }, [historyUser, loadingHistory, item, idSim]);

  return loading || loadingHistory ? (
    <Loading />
  ) : (
    <Container>
      <Content>
        <Row>
          {questionsData && questionsData.length
            ? questionsData.map((itm, index) => (
                <QuestionItem
                  isCorrect={itm.correct}
                  onPress={() =>
                    navigation.navigate('HistorySimulateItem', {
                      question: {...item[index], questionId: index + 1},
                    })
                  }>
                  <QuestionItemText>{index + 1}</QuestionItemText>
                </QuestionItem>
              ))
            : null}
        </Row>
      </Content>
    </Container>
  );
};

export default History;
