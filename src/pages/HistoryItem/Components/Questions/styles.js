import styled from 'styled-components/native';

import {colors, fonts} from '../../../../global';

export const Container = styled.View`
  flex: 1;
  background-color: ${colors.background_main};
  position: relative;
  padding: 24px 0 0;
  z-index: 999;
`;

export const Content = styled.ScrollView`
  flex: 1;
`;

export const Row = styled.View`
  flex-wrap: wrap;
  justify-content: flex-start;
  width: 97%;
  align-items: center;
  flex-direction: row;
  margin: 0 24px;
`;

export const QuestionItem = styled.TouchableOpacity`
  margin-right: 13px;
  margin-bottom: 13px;
  background-color: ${({isCorrect}) => (isCorrect ? '#A9C21B' : '#FF5630')};
  width: 32px;
  height: 32px;
  border-radius: 16px;
  align-items: center;
  flex-direction: row;
  justify-content: center;
`;

export const QuestionItemText = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.background_main};
  font-family: ${fonts.fontFamilyRegular};
`;
