import React, {useEffect, useState} from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {format, parseISO} from 'date-fns';

import {
  Container,
  Content,
  ContainerHeader,
  IconStyled,
  HeaderName,
  ContentHeader,
  Row,
  Column,
  LabelDate,
  TitleLabel,
  Label,
  TitleLabelTwo,
} from './styles';

import {useSimulate} from '../../hooks/Simulate';

import Questions from './Components/Questions';
import Simulates from './Components/Simulates';
import {colors} from '../../global';
import {APPROVAL_RANGE} from '../History/Components/Simulates/Components/Categories';
import {LabelAproveReprove} from '../History/Components/Simulates/Components/Categories/styles';

const History = ({route}) => {
  const navigation = useNavigation();
  const {simulates} = useSimulate();
  const Tab = createMaterialTopTabNavigator();
  const item = route.params;
  const [time, setTime] = useState('00h00m');
  const [simulate, setSimulate] = useState();
  const [correct, setCorrect] = useState(0);
  const [errored, setErrored] = useState(0);

  const averageScore = ((100 * correct) / (correct + errored)).toFixed(0);

  const id = item[0].simulateId || '';
  const itmTime = item[0].time || '';

  function pad(num) {
    return ('0' + num).slice(-2);
  }

  function hhmmss(secs) {
    var minutes = Math.floor(secs / 60);
    secs = secs % 60;
    var hours = Math.floor(minutes / 60);
    minutes = minutes % 60;
    return `${pad(hours)}h${pad(minutes)}m`;
    // return pad(hours)+":"+pad(minutes)+":"+pad(secs); for old browsers
  }

  useEffect(() => {
    if (simulates) {
      setSimulate(simulates.find(sim => sim.key === id));
    }
  }, [simulates, item, id]);

  useEffect(() => {
    if (simulate) {
      const durationTime = simulate.duration.split(':');
      setTime(
        hhmmss(durationTime[0] * 60 * 60 + durationTime[1] * 60 - itmTime),
      );
    } else {
      setTime('03h00');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [simulate, item]);

  useEffect(() => {
    if (item && item.length) {
      let cr = 0;
      let er = 0;
      item.forEach(itm => {
        if (itm.correct) {
          cr += 1;
        } else {
          er += 1;
        }
      });
      setCorrect(cr);
      setErrored(er);
    }
  }, [item]);

  return (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <ContainerHeader>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <IconStyled name="arrow-left" size={25} color={colors.white} />
        </TouchableOpacity>
        <HeaderName>
          {simulate ? simulate.name : 'Simulado especial'}
        </HeaderName>
      </ContainerHeader>
      <ContentHeader>
        <Row>
          <Column basis={70}>
            <Row>
              <Column basis={40}>
                <LabelDate>
                  {item[item.length - 1].date &&
                  !item[item.length - 1].date.nanoseconds
                    ? format(parseISO(item[item.length - 1].date), 'dd/MM/yyyy')
                    : null}
                </LabelDate>
                <LabelDate>{''}</LabelDate>
                <TitleLabel>{correct + errored}</TitleLabel>
                <Label>{correct + errored > 1 ? 'Questões' : 'Questão'}</Label>
              </Column>
              <Column basis={20}>
                <LabelDate>{''}</LabelDate>
                <LabelDate>{''}</LabelDate>
                <TitleLabelTwo color="#A9C21B">
                  {((100 * correct) / (correct + errored)).toFixed(0)}%
                </TitleLabelTwo>
                <Label>
                  {correct} {correct > 1 ? 'Acertos' : 'Acerto'}
                </Label>
              </Column>
              <Column basis={20}>
                <LabelDate>{''}</LabelDate>
                <LabelDate>{''}</LabelDate>
                <TitleLabelTwo color="#FF5630">
                  {((100 * errored) / (correct + errored)).toFixed(0)}%
                </TitleLabelTwo>
                <Label>
                  {errored} {errored > 1 ? 'Erros' : 'Erro'}
                </Label>
              </Column>
            </Row>
          </Column>

          <Column basis={30} style={{paddingRight: 12}}>
            <Row>
              <Column basis={94}>
                <LabelDate>{time}</LabelDate>
                <LabelDate>{''}</LabelDate>
                <LabelAproveReprove
                  averageScore={averageScore}
                  style={{marginBottom: 12}}>
                  {averageScore <= APPROVAL_RANGE ? 'Aprovado' : 'Reprovado'}
                </LabelAproveReprove>
              </Column>
            </Row>
          </Column>
        </Row>
      </ContentHeader>
      <Container>
        <ScrollView
          contentContainerStyle={{flexGrow: 1}}
          style={{width: '100%'}}>
          <Content>
            <Tab.Navigator
              tabBarOptions={{
                activeTintColor: colors.orange,
                inactiveTintColor: colors.color_botton_inactive,
                activeBorderColor: colors.orange,
                style: {
                  backgroundColor: colors.background_main,
                  height: 60,
                  elevation: 0, // for Android
                  borderBottomColor: colors.color_botton_inactive,
                  shadowOffset: {
                    width: 0,
                  },
                  paddingBottom: 8,
                  marginRight: 100,
                },
                indicatorStyle: {
                  backgroundColor: colors.orange,
                },
              }}>
              <Tab.Screen
                name="Respostas"
                component={() => <Questions item={item} />}
              />
              <Tab.Screen
                name="Por assunto"
                component={() => <Simulates item={item} />}
              />
            </Tab.Navigator>
          </Content>
        </ScrollView>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default History;
