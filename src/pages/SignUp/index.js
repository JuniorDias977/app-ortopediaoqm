import React, {useCallback, useState} from 'react';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {useNavigation} from '@react-navigation/native';
import {sendGridEmail} from 'react-native-sendgrid';
import {
  Alert,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';
import IconF from 'react-native-vector-icons/Feather';
import {Checkbox} from 'react-native-paper';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {format} from 'date-fns';

import Loading from '../../components/Loading';
import Input from '../../components/Input';
import Button from '../../components/Button';
import {colors} from '../../global';
import Modal from '../../components/Modal';

import {
  Container,
  Content,
  ForgotPasswordText,
  ButtonSelect,
  Row,
  RowTwo,
  ButtonBack,
  TitlePage,
  HeaderTitle,
  TextTerms,
  PickerContainer,
  TextSelect,
  PickerStyled,
  TextButtonSignIn,
  ButtonSignIn,
  LogoG,
  TextSelectNew,
} from './styles';

const schema = Yup.object().shape({
  name: Yup.string().required('Nome obrigatório'),
  email: Yup.string().required('Email obrigatório'),
  password: Yup.string().required('Senha obrigatória'),
});

const SENDGRIDAPIKEY =
  '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';
const FROMEMAIL = 'contato@ortopediaoqm.com.br';
const SUBJECT = 'Conta cadastrada [Ortopedia OQM]';

const MESSAGE = `<p>Olá,</p><p>Sua conta foi registrada no aplicativo de estudos da Ortopedia OQM.</p>${
  Platform.OS === 'ios'
    ? null
    : '<p>Ficamos muito felizes de você fazer parte do nosso grupo de estudos, mas algumas funcionalidades são exclusivas de nossos alunos. Caso queira fazer parte desse grupo e ter acesso a todas as funcionalidades, basta acessar alguma página restrita e você terá todas as informações.</p><p>Bons estudos!</p><p>Ortopedia OQM</p>'
}`;

const SignIn = () => {
  const navigation = useNavigation();
  const [showAlert, setShowAlert] = useState(false);
  const [loading, setLoading] = useState(false);
  const [checked, setChecked] = useState(false);
  const [email, setEmail] = useState();
  const [name, setName] = useState();
  const [password, setPassword] = useState();
  const [selected, setSelected] = useState('');
  const [modalMediaVisible, setModalMediaVisible] = useState(false);

  const handleSignUp = useCallback(
    async data => {
      try {
        if (!selected) {
          Alert.alert('ERRO', 'Selecione o ano de sua residência');
          return;
        }
        setLoading(true);
        auth()
          .createUserWithEmailAndPassword(data.email.trim(), data.password)
          .then(function () {
            setLoading(true);
            firestore()
              .collection('users')
              .add({
                name: data.name,
                email: data.email.trim(),
                is_admin: false,
                avatar: '',
                residence: selected,
                is_studenty: checked,
                date: format(new Date(), 'yyyy-MM-dd')
                  .concat(' ')
                  .concat(format(new Date(), 'HH:mm:ss')),
                active: checked ? false : true,
              })
              .then(async () => {
                setLoading(false);
                if (!checked) {
                  const sendRequest = sendGridEmail(
                    SENDGRIDAPIKEY,
                    data.email.trim(),
                    FROMEMAIL,
                    SUBJECT,
                    MESSAGE,
                    'text/html',
                  );
                  sendRequest
                    .then(() => {
                      navigation.navigate('SuccessUserConfirm');
                    })
                    .catch(error => {
                      console.log(error);
                    });
                } else {
                  navigation.navigate('SuccessUser');
                }
              })
              .catch(function () {
                setLoading(false);
              });
          })
          .catch(function (error) {
            if (error.code === 'auth/email-already-in-use') {
              Alert.alert('Email já cadastrado');
              setLoading(false);
            }
            if (error.code === 'auth/invalid-email') {
              Alert.alert('Email invalido!');
              setLoading(false);
            }
            return;
          });
      } catch (error) {
        setLoading(false);
      }
    },
    [selected, checked, navigation],
  );

  return loading ? (
    <Loading />
  ) : (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <ScrollView
          style={{flex: 1}}
          contentContainerStyle={{flexGrow: 1}}
          showsVerticalScrollIndicator={false}>
          <Content>
            <Row>
              <ButtonBack onPress={() => navigation.goBack()}>
                <IconF
                  name="arrow-left"
                  color={colors.white_correct}
                  size={30}
                />
              </ButtonBack>
              <HeaderTitle>Cadastro</HeaderTitle>
            </Row>
            <TitlePage>Criar conta</TitlePage>
            <Formik
              initialValues={{email: '', password: ''}}
              validationSchema={schema}
              onSubmit={handleSignUp}>
              {({handleBlur, handleSubmit, values, errors, setFieldValue}) => (
                <>
                  <Input
                    name="name"
                    icon="user"
                    placeholder={'Nome'}
                    autoCorrect={false}
                    autoCapitalize="none"
                    onChangeText={e => {
                      setFieldValue('name', e);
                      setName(e);
                    }}
                    onBlur={handleBlur('name')}
                    value={name || values.name}
                    error={errors.name}
                  />
                  <Input
                    name="email"
                    icon="user"
                    placeholder={'E-mail'}
                    autoCorrect={false}
                    autoCapitalize="none"
                    onChangeText={e => {
                      setFieldValue('email', e);
                      setEmail(e);
                    }}
                    onBlur={handleBlur('email')}
                    value={email || values.email}
                    error={errors.email}
                  />
                  <Input
                    name="password"
                    icon="lock"
                    placeholder={'Senha'}
                    password
                    onChangeText={e => {
                      setFieldValue('password', e);
                      setPassword(e);
                    }}
                    onBlur={handleBlur('password')}
                    value={values.password}
                    returnKeyType="send"
                    onSubmitEditing={handleSubmit}
                    error={errors.password}
                  />
                  {Platform.OS === 'android' ? (
                    <PickerContainer>
                      {!selected ? (
                        <TextSelect>Ano da sua residência</TextSelect>
                      ) : null}
                      <PickerStyled
                        selectedValue={selected}
                        mode="dropdown"
                        onValueChange={(itemValue, itemIndex) =>
                          setSelected(itemValue)
                        }>
                        <PickerStyled.Item
                          color="#717171"
                          label="Ano da sua residência"
                          value=""
                        />
                        <PickerStyled.Item label="Sou R1" value="R1" />
                        <PickerStyled.Item label="Sou R2" value="R2" />
                        <PickerStyled.Item label="Sou R3" value="R3" />
                      </PickerStyled>
                      {/* <IconStyled
                      name="arrow-down"
                      size={20}
                      color={colors.placeholder_text}
                    /> */}
                    </PickerContainer>
                  ) : (
                    <ButtonSelect
                      onPress={() => setModalMediaVisible(!modalMediaVisible)}>
                      {!selected ? (
                        <TextSelectNew>Ano da sua residência</TextSelectNew>
                      ) : (
                        <TextSelectNew selected={!!selected}>
                          {selected}
                        </TextSelectNew>
                      )}
                    </ButtonSelect>
                  )}
                  <Row>
                    <Checkbox
                      color={colors.orange}
                      uncheckedColor={colors.white}
                      status={checked ? 'checked' : 'unchecked'}
                      onPress={() => {
                        setChecked(!checked);
                      }}
                    />
                    <ForgotPasswordText style={{color: '#BEBFC2'}}>
                      Sou aluno OQM
                    </ForgotPasswordText>
                  </Row>
                  <Button
                    active={!!name && !!password && !!email && !!selected}
                    onPress={handleSubmit}
                    style={{marginTop: 'auto'}}>
                    Cadastrar
                  </Button>
                </>
              )}
            </Formik>
            <Modal
              isModalVisible={modalMediaVisible}
              setModalVisible={setModalMediaVisible}
              selected={selected}
              setSelected={setSelected}
            />
            <AwesomeAlert
              show={showAlert}
              showProgress={false}
              title={'Cadastro concluído'}
              message={'Você já pode efetuar login na nossa plataforma'}
              closeOnTouchOutside={false}
              closeOnHardwareBackPress={false}
              showCancelButton={false}
              showConfirmButton={true}
              confirmText={'Fazer login'}
              confirmButtonColor={colors.orange}
              onConfirmPressed={() => {
                setShowAlert(false);
                navigation.navigate('SignIn');
              }}
            />
            {/* <ButtonSignIn
              background="#385996"
              onPress={() => {}}
              style={{marginTop: 12}}>
              <LogoF />
              <TextButtonSignIn color="#f1f1f1">
                PREENCHER COM O FACEBOOK
              </TextButtonSignIn>
            </ButtonSignIn>
            <ButtonSignIn background="#f1f1f1" onPress={() => {}}>
              <LogoG />
              <TextButtonSignIn color="grey">
                PREENCHER COM O GOOLE
              </TextButtonSignIn>
            </ButtonSignIn>
            <ButtonSignIn background="#385996" onPress={getDataFromFacebook}>
              <Icon
                name="social-facebook"
                color={colors.white_correct}
                size={25}
                style={{marginRight: 8}}
              />
              <TextButtonSignIn color="#fff">
                Preencher com o facebook
              </TextButtonSignIn>
            </ButtonSignIn>
            <ButtonSignIn background="#fff" onPress={getDataFromGoogle}>
              <Icon
                name="social-google"
                color={colors.osloGrey}
                size={25}
                style={{marginRight: 8}}
              />
              <TextButtonSignIn color="grey">
                Preencher com o Google
              </TextButtonSignIn>
            </ButtonSignIn>
            <TextTerms>Ao entrar declaro que li e aceito os</TextTerms>
            <RowTwo>
              <TouchableOpacity onPress={() => navigation.navigate('Terms')}>
                <ForgotPasswordText>Termos & Condições</ForgotPasswordText>
              </TouchableOpacity>
              <TextTerms> e a</TextTerms>
            </RowTwo>
            <TouchableOpacity onPress={() => navigation.navigate('Policy')}>
              <ForgotPasswordText>Política de privacidade</ForgotPasswordText>
            </TouchableOpacity> */}
          </Content>
        </ScrollView>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default SignIn;
