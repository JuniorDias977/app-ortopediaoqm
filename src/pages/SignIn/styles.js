import styled from 'styled-components/native';
import {Image} from 'react-native';

import {colors, fonts} from '../../global';

export const Container = styled.View`
  flex: 1;
  background-color: ${colors.background_main};
  position: relative;
  padding: 24px 24px 0;
`;

export const Content = styled.View`
  flex: 1;
`;

export const TextError = styled.Text`
  font-size: ${fonts.small};
  color: ${colors.error};
  font-family: ${fonts.fontFamilyRegular};
`;

export const LogoF = styled(Image).attrs({
  source: require('../../assets/face.png'),
  resizeMode: 'contain',
})`
  height: 22px;
  width: 22px;
  margin-right: 8px;
`;

export const LogoG = styled(Image).attrs({
  source: require('../../assets/google.png'),
  resizeMode: 'contain',
})`
  height: 22px;
  width: 22px;
  margin-right: 8px;
`;

export const ForgotPassword = styled.TouchableOpacity`
  margin-top: 12px;
`;

export const ForgotPasswordText = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white_correct};
  font-family: ${fonts.fontFamilyRegular};
  text-decoration: underline;
  text-align: center;
`;

export const CreateAccount = styled.TouchableOpacity`
  margin-top: 50px;
  margin-bottom: 50px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const CreateAccountText = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.grey};
  font-family: ${fonts.fontFamilyRegular};
  margin-left: 16px;
`;

export const RowTwo = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ButtonBack = styled.TouchableOpacity``;

export const HeaderTitle = styled.Text`
  font-size: ${fonts.bigger};
  color: ${colors.white_correct};
  font-family: ${fonts.fontFamilyRegular};
  margin-top: 5px;
  margin-left: 16px;
`;

export const TitlePage = styled.Text`
  font-size: ${fonts.bigger};
  color: rgba(241, 241, 241, 0.4);
  font-family: ${fonts.fontFamilyRegular};
  margin: 36px 0 12px 0;
`;

export const TextTerms = styled.Text`
  font-size: ${fonts.regular};
  color: rgba(241, 241, 241, 0.4);
  font-family: ${fonts.fontFamilyRegular};
  text-align: center;
  margin-left: 5px;
`;
export const ButtonSignIn = styled.TouchableOpacity`
  margin-top: 12px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: 12px;
  border-radius: 24px;
  background-color: ${({background}) =>
    background ? background : colors.orange};
  width: 100%;
`;

export const TextButtonSignIn = styled.Text`
  font-size: ${fonts.regular};
  margin-top: 5px;
  color: ${({color}) => (color ? color : colors.color_text)};
  font-family: ${fonts.fontFamilyRegular};
`;
