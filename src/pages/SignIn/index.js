import React, {useCallback, useState} from 'react';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {useNavigation} from '@react-navigation/native';
import {
  Alert,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';
import IconF from 'react-native-vector-icons/Feather';
// import {LoginManager, AccessToken} from 'react-native-fbsdk-next';
import auth from '@react-native-firebase/auth';

import Loading from '../../components/Loading';
import {useAuth} from '../../hooks/Auth';
import Input from '../../components/Input';
import Button from '../../components/Button';
import {colors} from '../../global';

import {
  Container,
  Content,
  ForgotPassword,
  ForgotPasswordText,
  Row,
  RowTwo,
  ButtonBack,
  TitlePage,
  HeaderTitle,
  TextTerms,
  TextButtonSignIn,
  ButtonSignIn,
  LogoF,
  LogoG,
} from './styles';

const schema = Yup.object().shape({
  email: Yup.string().required('email obrigatório'),
  password: Yup.string().required('Senha obrigatória'),
});

const SignIn = () => {
  const {signIn, language, loading} = useAuth();
  const navigation = useNavigation();
  const [showAlert, setShowAlert] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loadingData, setLoadingData] = useState('');

  const handleSignIn = async data => {
    try {
      setLoadingData(true);
      await signIn({
        email: data.email,
        password: data.password,
      });
      setLoadingData(false);
    } catch (error) {
      setShowAlert(true);
      setLoadingData(false);
    }
  };

  return loading || loadingData ? (
    <Loading />
  ) : (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{flex: 1}}
          contentContainerStyle={{flexGrow: 1}}>
          <Content>
            <Row>
              <ButtonBack onPress={() => navigation.goBack()}>
                <IconF
                  name="arrow-left"
                  color={colors.white_correct}
                  size={30}
                />
              </ButtonBack>
              <HeaderTitle>Entrar</HeaderTitle>
            </Row>
            <TitlePage>Entre com sua conta</TitlePage>
            <Formik
              initialValues={{email: '', password: ''}}
              validationSchema={schema}
              onSubmit={handleSignIn}>
              {({
                handleChange,
                handleBlur,
                handleSubmit,
                values,
                errors,
                setFieldValue,
              }) => (
                <>
                  <Input
                    name="email"
                    icon="user"
                    placeholder={'E-mail'}
                    autoCorrect={false}
                    autoCapitalize="none"
                    onChangeText={e => {
                      setFieldValue('email', e);
                      setEmail(e);
                    }}
                    onBlur={handleBlur('email')}
                    value={values.email}
                    error={errors.email}
                  />
                  <Input
                    name="password"
                    icon="lock"
                    placeholder={'Senha'}
                    password
                    onChangeText={e => {
                      setFieldValue('password', e);
                      setPassword(e);
                    }}
                    onBlur={handleBlur('password')}
                    value={values.password}
                    returnKeyType="send"
                    onSubmitEditing={handleSubmit}
                    error={errors.password}
                  />
                  <Button
                    onPress={handleSubmit}
                    active={!!password && !!email}
                    style={{marginTop: 24}}>
                    Entrar
                  </Button>
                </>
              )}
            </Formik>

            <AwesomeAlert
              show={showAlert}
              showProgress={false}
              title={
                language
                  ? 'Incorrect login / password'
                  : 'Login/Senha incorretos'
              }
              message={
                language
                  ? 'Check your credentials and try again'
                  : 'Cheque suas credênciais e tente novamente'
              }
              closeOnTouchOutside={false}
              closeOnHardwareBackPress={false}
              showCancelButton={false}
              showConfirmButton={true}
              confirmText={language ? 'Try again' : 'Tentar novamente'}
              confirmButtonColor={colors.primary}
              onConfirmPressed={() => {
                setShowAlert(false);
              }}
            />
            <ForgotPassword onPress={() => navigation.navigate('Recover')}>
              <ForgotPasswordText>ESQUECEU A SENHA?</ForgotPasswordText>
            </ForgotPassword>
          </Content>
        </ScrollView>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default SignIn;
