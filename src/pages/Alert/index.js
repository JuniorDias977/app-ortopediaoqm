import React, {useState, useCallback} from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  View,
  ScrollView,
  Linking,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import IconF from 'react-native-vector-icons/Feather';

import Loading from '../../components/Loading';
import Button from '../../components/Button';

import {
  Container,
  Content,
  AlertText,
  ButtonBack,
  Text,
  ViewAlert,
  TextWhiteBigger,
  RowAlert,
  TextRowAlert,
  TextRowAlertSmall,
  TextRowAlertSmallStrong,
  Row,
  LogoAlert,
  TextWhite,
  LogBg,
} from './styles';

import {colors} from '../../global';

const Category = () => {
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);

  const handleStudentOQM = useCallback(() => {
    Linking.openURL('https://www.facebook.com/Ortopedia-OQM-101506351362304/');
  }, []);

  return loading ? (
    <Loading />
  ) : (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <LogBg />
        <ButtonBack onPress={() => navigation.navigate('SIMULADOS')}>
          <IconF name="arrow-left" color={colors.white_correct} size={30} />
        </ButtonBack>
        {Platform.OS === 'android' ? (
          <>
            <Text>Desculpe, você não pode acessar essa área. </Text>
            <TextWhiteBigger>
              Função restrita aos nossos alunos!{' '}
            </TextWhiteBigger>
            <Content>
              <ScrollView showsVerticalScrollIndicator={false}>
                <View>
                  <View
                    style={{
                      paddingTop: 24,
                      paddingBottom: 12,
                    }}>
                    <ViewAlert>
                      <RowAlert>
                        <TextRowAlert>Economize 50%</TextRowAlert>
                        <TextRowAlertSmall>Expira em:</TextRowAlertSmall>
                        <TextRowAlertSmallStrong>
                          23h45m34s
                        </TextRowAlertSmallStrong>
                      </RowAlert>
                      <AlertText>
                        Venha ser um aluno OQM e tenha todo o acompanhamento
                        para otimizar seus estudos.
                      </AlertText>
                    </ViewAlert>
                    <TextWhite style={{marginTop: 12}}>
                      Quer ser aluno OQM e ter resultado positivo nas suas
                      provas?
                    </TextWhite>
                  </View>
                  <Row>
                    <LogoAlert />
                    <TextWhite>Professores renomados</TextWhite>
                  </Row>
                  <Row>
                    <LogoAlert />
                    <TextWhite>Aulas didáticas</TextWhite>
                  </Row>
                  <Row>
                    <LogoAlert />
                    <TextWhite>Explicação didática das questões</TextWhite>
                  </Row>
                  <Row>
                    <LogoAlert />
                    <TextWhite>
                      Acompanhamento do seu desempenho por professores
                      especializados
                    </TextWhite>
                  </Row>
                  <Row>
                    <LogoAlert />
                    <TextWhite>Análise gráfica do seu desempenho</TextWhite>
                  </Row>
                  <Row>
                    <LogoAlert />
                    <TextWhite>Conteúdo exclusivo</TextWhite>
                  </Row>
                  <View
                    style={{
                      paddingTop: 'auto',
                      paddingBottom: 24,
                    }}>
                    <Button onPress={handleStudentOQM}>
                      Quero ser um aluno OQM
                    </Button>
                  </View>
                </View>
              </ScrollView>
            </Content>
          </>
        ) : (
          <Text>Ainda estamos trabalhando para sua melhor experiência. </Text>
        )}
      </Container>
    </KeyboardAvoidingView>
  );
};

export default Category;
