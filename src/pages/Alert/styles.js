import styled from 'styled-components/native';
import {Dimensions} from 'react-native';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';

import {colors, fonts} from '../../global';

const {width} = Dimensions.get('window');

export const Container = styled.View`
  flex: 1;
  background-color: ${colors.background_main};
  position: relative;
`;

export const Content = styled.View`
  flex: 1;
  padding: 12px 24px;
  margin-top: 24px;
`;

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: 12px;
  padding-right: 24px;
`;

export const ButtonBack = styled.TouchableOpacity`
  margin: 24px 0 0 24px;
`;

export const LogoAlert = styled.Image.attrs({
  source: require('../../assets/check.png'),
  resizeMode: 'contain',
})`
  width: 24px;
  height: 24px;
  margin-right: 16px;
`;

export const LogBg = styled.Image.attrs({
  source: require('../../assets/bg-log.png'),
  resizeMode: 'cover',
})`
  width: 100%;
  height: 220px;
  position: absolute;
  left: 0;
  top: 0;
`;

export const LogoIcon = styled.Image.attrs({
  resizeMode: 'cover',
})`
  width: 124px;
  align-self: center;
  height: 124px;
  border-radius: 62px;
  margin-top: 32px;
  border-width: 6px;
  border-color: #333333;
`;

export const Title = styled.Text`
  font-size: ${fonts.bigger};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyBold};
  text-align: left;
  align-self: center;
  margin-top: 16px;
`;

export const TitlePage = styled.Text`
  font-size: ${fonts.bigger};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyBold};
  text-align: left;
  align-self: center;
  margin-top: 24px;
  margin-left: 48px;
`;

export const Text = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.orange};
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  margin-top: 16px;
  padding: 0 24px;
`;

export const TextWhite = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
`;

export const TextWhiteBigger = styled.Text`
  font-size: ${fonts.bigger};
  padding: 0 24px;
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
`;

export const ContentDescription = styled.View`
  flex: 1;
  background-color: #262626;
  width: ${width}px;
  border-top-left-radius: 24px;
  border-top-right-radius: 24px;
  margin-top: 32px;
  padding: 24px;
`;

export const Icon = styled(SimpleLineIcon)``;

export const TouchableOpacity = styled.TouchableOpacity`
  margin-bottom: 24px;
`;
export const ViewAlert = styled.View`
  border-width: 1px;
  border-color: rgba(241, 241, 241, 0.6);
  border-radius: 12px;
`;

export const AlertText = styled.Text`
  font-size: ${fonts.small};
  color: rgba(241, 241, 241, 0.6);
  font-family: ${fonts.fontFamilyLight};
  text-align: center;
  padding: 24px;
  margin: 0 12px;
`;

export const RowAlert = styled.View`
  flex-direction: row;
  background-color: ${colors.orange};
  align-items: center;
  border-top-right-radius: 12px;
  border-top-left-radius: 12px;
  padding: 12px;
`;

export const TextRowAlert = styled.Text`
  font-size: ${fonts.regular};
  color: #262626;
  font-family: ${fonts.fontFamilyStrongBold};
  text-align: center;
`;

export const TextRowAlertSmall = styled.Text`
  font-size: ${fonts.small};
  color: #262626;
  font-family: ${fonts.fontFamilyLight};
  text-align: center;
  margin-left: auto;
`;

export const TextRowAlertSmallStrong = styled.Text`
  font-size: ${fonts.small};
  color: #262626;
  font-family: ${fonts.fontFamilyStrongBold};
  text-align: center;
  margin-left: 8px;
`;
