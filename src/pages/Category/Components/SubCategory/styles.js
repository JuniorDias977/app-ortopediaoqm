import styled, {css} from 'styled-components';

import {colors, fonts} from '../../../../global';

export const Container = styled.TouchableOpacity`
  padding: 8px 10px;
  margin-right: 8px;
  height: 40px;

  ${({checked}) =>
    checked
      ? css`
          background-color: ${colors.white};
          border-radius: 24px;
        `
      : null}
`;

export const Title = styled.Text`
  font-size: ${fonts.regular};
  color: ${({checked}) => (checked ? colors.background_main : colors.white)};
  font-family: ${fonts.fontFamilyBold};
`;
