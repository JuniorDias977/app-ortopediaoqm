import React from 'react';

import {Container, Title} from './styles';

const Item = ({item, setSelectSubCategory, selectSubCategory}) => {
  return (
    <Container
      onPress={() => setSelectSubCategory(item)}
      checked={selectSubCategory && selectSubCategory.key === item.key}>
      <Title checked={selectSubCategory && selectSubCategory.key === item.key}>
        {item.name}
      </Title>
    </Container>
  );
};

export default Item;
