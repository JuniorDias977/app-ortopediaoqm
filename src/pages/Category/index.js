import React, {useState, useEffect, useCallback} from 'react';
import {Alert, KeyboardAvoidingView, Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {useNavigation} from '@react-navigation/native';

import IconF from 'react-native-vector-icons/Feather';
import storage from '@react-native-firebase/storage';
import {Checkbox} from 'react-native-paper';

import Loading from '../../components/Loading';
import InputFilter from '../../components/InputFilter';
import Button from '../../components/Button';

import {useSubCategories} from '../../hooks/SubCategories';
import {useSubGroups} from '../../hooks/SubGroups';

import firestore from '@react-native-firebase/firestore';
import {useAuth} from '../../hooks/Auth';
import reactotron from 'reactotron-react-native';

import Item from './Components/Item';
import SubCategory from './Components/SubCategory';

import {colors} from '../../global';

import {
  Container,
  Content,
  Row,
  HeaderTitle,
  FlatListStyled,
  ButtonBack,
  LogoIcon,
  Logo,
  Title,
  RowTwo,
  FlatListStyledHorizontal,
  RowThree,
  ExportAll,
  ContentRow,
} from './styles';

const Category = props => {
  const navigation = useNavigation();
  const {user} = useAuth();

  const {subCategories} = useSubCategories();
  const {subGroups} = useSubGroups();

  const [loading, setLoading] = useState(false);
  const [category, setCategory] = useState(false);
  const [urlImage, setUrlImage] = useState();

  const [selectSubCategory, setSelectSubCategory] = useState();
  const [subsCategories, setSubsCategories] = useState([]);
  const [subsGroups, setSubsGroups] = useState([]);
  const [selectSubGroup, setSelectSubGroup] = useState([]);

  const [checked, setChecked] = useState(true);
  const [itemData, setItemData] = useState(false);
  const [text, setText] = useState('');

  const [newQuestion, setNewQuestion] = useState([]);

  useEffect(() => {
    if (props && props.route.params) {
      setItemData(true);
    }
  }, [props]);

  useEffect(() => {
    if (
      props &&
      props.route.params &&
      props.route.params.item &&
      props.route.params.item.goback
    ) {
      navigation.goBack();
    }
  }, [props, navigation]);

  useEffect(() => {
    async function getCategory() {
      const categoryStorage = await AsyncStorage.getItem('@categoryOrtopedia:');
      if (categoryStorage) {
        setCategory(JSON.parse(categoryStorage));
      }
    }
    getCategory();
  }, [itemData]);

  useEffect(() => {
    async function getImage() {
      if (category && category.icon) {
        storage()
          .ref(category.icon)
          .getDownloadURL()
          .then(url => {
            setUrlImage(url);
          });
      }
    }
    getImage();
  }, [category]);

  useEffect(() => {
    setLoading(true);
    if (subCategories && subCategories.length) {
      const newSubs =
        subCategories.filter(sub => {
          return sub.category.value === category.key && sub.active;
        }) || [];

      setSubsCategories(newSubs);
      setSelectSubCategory(newSubs[0]);
    }
    setLoading(false);
  }, [category, subCategories]);

  useEffect(() => {
    if (selectSubCategory && subGroups && subGroups.length) {
      const newSubs =
        subGroups.filter(sub => {
          return sub.sub_category.value === selectSubCategory.key && sub.active;
        }) || [];

      setSubsGroups(newSubs);
    } else {
      setSubsGroups([]);
    }
  }, [selectSubCategory, subGroups]);

  useEffect(() => {
    if (selectSubCategory) {
      firestore()
        .collection('question_answered')
        .where('user_id', '==', user.key)
        .onSnapshot(querySnapshot => {
          let filterDocQuestion = [];

          querySnapshot.forEach(documentSnapshot => {
            const {answered_question_user, key} = documentSnapshot.data();

            filterDocQuestion.push({
              answered_question_user,
              key,
            });
          });

          let filterResult = [];

          filterDocQuestion.forEach(arrayQuestion => {
            const key = arrayQuestion.key;

            let answered_question_user =
              arrayQuestion.answered_question_user.filter(itemArray => {
                return (
                  itemArray.category.value === category.key &&
                  itemArray.sub_category.value === selectSubCategory.key
                );
              });

            if (answered_question_user.length > 0) {
              filterResult.push({
                answered_question_user,
                key,
              });
            }
          });

          setNewQuestion(filterResult);
        });
    }
  }, [user.key, category, selectSubCategory]);

  const handleQuestions = useCallback(() => {
    if (checked) {
      navigation.navigate('QuestionResolve', {item: subsGroups});
      return;
    }
    if (!selectSubGroup) {
      Alert.alert('ATENÇÃO', 'Selecione um sub grupo.');
      return;
    }
    navigation.navigate('QuestionResolve', {item: selectSubGroup});
  }, [selectSubGroup, subsGroups, navigation, checked]);

  const handleNewQuestion = useCallback(() => {
    if (checked) {
      navigation.navigate('QuestionResolve', {item: subsGroups, newQuestion});
      return;
    }
    if (!selectSubGroup) {
      Alert.alert('ATENÇÃO', 'Selecione um sub grupo.');
      return;
    }
    navigation.navigate('QuestionResolve', {item: selectSubGroup, newQuestion});
  }, [selectSubGroup, subsGroups, navigation, checked, newQuestion]);

  return loading ? (
    <Loading />
  ) : (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <Content>
          <Row>
            {itemData ? null : (
              <ButtonBack onPress={() => navigation.goBack()}>
                <IconF
                  name="arrow-left"
                  color={colors.white_correct}
                  size={30}
                />
              </ButtonBack>
            )}
            {itemData ? (
              <InputFilter
                value={text}
                placeholder="Selecione ou pesquise por assunto"
                icon="magnifier"
                onChangeText={e => setText(e)}
                setText={setText}
              />
            ) : null}
          </Row>
          {itemData ? null : (
            <>
              <RowTwo>
                {urlImage ? <LogoIcon source={{uri: urlImage}} /> : <Logo />}
                <Title>{category.name}</Title>
              </RowTwo>
              <Row>
                <HeaderTitle>
                  As questões serão filtradas com base nos assuntos que você
                  deixar ativado. Edite as preferências para intensificar seus
                  estudos!
                </HeaderTitle>
              </Row>
            </>
          )}
          {subsCategories && subsCategories.length ? (
            <FlatListStyledHorizontal
              data={
                subsCategories && subsCategories.length ? subsCategories : []
              }
              renderItem={({item}) => (
                <SubCategory
                  key={item.key}
                  item={item}
                  setSelectSubCategory={setSelectSubCategory}
                  selectSubCategory={selectSubCategory}
                />
              )}
              keyExtractor={item => item.id}
              horizontal
              showsVerticalScrollIndicator={false}
            />
          ) : null}
          <RowThree>
            <ExportAll>Todos</ExportAll>
            <Checkbox
              color={colors.orange}
              uncheckedColor={colors.white}
              status={checked ? 'checked' : 'unchecked'}
              onPress={() => {
                setChecked(!checked);
              }}
            />
          </RowThree>

          <FlatListStyled
            itemData={itemData}
            data={subsGroups && subsGroups.length ? subsGroups : []}
            renderItem={({item}) => (
              <Item
                item={item}
                key={item.key}
                setSelectSubGroup={setSelectSubGroup}
                selectSubGroup={selectSubGroup}
                setChecked={setChecked}
                checked={checked}
              />
            )}
            keyExtractor={item => item.id}
            showsVerticalScrollIndicator={false}
          />

          <ContentRow>
            <Button smallerButton onPress={handleNewQuestion}>
              Novas questões
            </Button>
            <Button smallerButton onPress={handleQuestions}>
              Todas as questões
            </Button>
          </ContentRow>
        </Content>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default Category;
