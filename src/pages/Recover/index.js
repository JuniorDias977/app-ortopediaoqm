import React, {useCallback, useState} from 'react';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {useNavigation} from '@react-navigation/native';
import {Alert, KeyboardAvoidingView, Platform, ScrollView} from 'react-native';
import IconF from 'react-native-vector-icons/Feather';
import auth from '@react-native-firebase/auth';

import Loading from '../../components/Loading';
import Input from '../../components/Input';
import Button from '../../components/Button';
import {colors} from '../../global';

import {
  Container,
  Content,
  Row,
  ButtonBack,
  TitlePage,
  HeaderTitle,
  TextTerms,
} from './styles';

const schema = Yup.object().shape({
  email: Yup.string().required('Email obrigatório'),
});

const SignIn = () => {
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
  const [stepOne, setStepOne] = useState(true);
  const [stepTwo, setStepTwo] = useState(false);
  const [email, setEmail] = useState();

  const handleRecover = useCallback(async data => {
    try {
      auth().sendPasswordResetEmail(data.email);
      setStepTwo(true);
      setStepOne(false);
    } catch (error) {
      setLoading(false);
      Alert.alert('ERRO', 'Houve um erro inesperado.');
    }
  }, []);

  function validateEmail(eml) {
    var re = /\S+@\S+\.\S+/;
    return re.test(eml);
  }

  return loading ? (
    <Loading />
  ) : (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <ScrollView
          style={{flex: 1}}
          contentContainerStyle={{flexGrow: 1}}
          showsVerticalScrollIndicator={false}>
          <Content>
            <Row>
              <ButtonBack onPress={() => navigation.goBack()}>
                <IconF
                  name={stepOne ? 'arrow-left' : 'x'}
                  color={colors.white_correct}
                  size={30}
                />
              </ButtonBack>
              <HeaderTitle>Esqueceu sua senha?</HeaderTitle>
            </Row>
            <TitlePage>Redefina sua senha</TitlePage>
            <TextTerms>
              {stepOne
                ? 'Insira seu endereço de e-mail abaixo para receber um link de redefinição de senha.'
                : 'Seu link de redefinição de senha foi enviado!'}
            </TextTerms>
            {stepOne ? (
              <Formik
                initialValues={{email: '', password: ''}}
                validationSchema={schema}
                onSubmit={handleRecover}>
                {({
                  handleBlur,
                  handleSubmit,
                  values,
                  errors,
                  setFieldValue,
                }) => (
                  <>
                    <Input
                      name="email"
                      placeholder={'E-mail'}
                      autoCorrect={false}
                      autoCapitalize="none"
                      isValid={validateEmail(values.email)}
                      type="email"
                      onChangeText={e => {
                        setFieldValue('email', e);
                        setEmail(e);
                      }}
                      onBlur={handleBlur('email')}
                      value={email || values.email}
                      error={errors.email}
                    />

                    <Button
                      active={!!values.email && validateEmail(values.email)}
                      onPress={() => (values.email ? handleSubmit() : null)}
                      style={{marginTop: 32}}>
                      Redefinir
                    </Button>
                  </>
                )}
              </Formik>
            ) : null}
            {stepTwo ? (
              <Button onPress={() => navigation.navigate('SignIn')}>
                Entrar
              </Button>
            ) : null}
          </Content>
        </ScrollView>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default SignIn;
