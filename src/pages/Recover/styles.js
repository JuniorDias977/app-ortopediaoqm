import styled from 'styled-components/native';
import SvgUri from 'react-native-svg-uri';

import {colors, fonts} from '../../global';

export const Container = styled.View`
  flex: 1;
  background-color: ${colors.background_main};
  position: relative;
  padding: 36px;
`;

export const Content = styled.View`
  flex: 1;
`;

export const TextError = styled.Text`
  font-size: ${fonts.small};
  color: ${colors.error};
  font-family: ${fonts.fontFamilyRegular};
`;

export const Logo = styled(SvgUri).attrs({
  source: require('../../assets/log.svg'),
  resizeMode: 'contain',
})`
  height: 200px;
`;

export const ForgotPassword = styled.TouchableOpacity`
  margin-top: 12px;
`;

export const ForgotPasswordText = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white_correct};
  font-family: ${fonts.fontFamilyRegular};
  text-align: center;
  margin-top: 5px;
`;

export const CreateAccount = styled.TouchableOpacity`
  margin-top: 50px;
  margin-bottom: 50px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const CreateAccountText = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.grey};
  font-family: ${fonts.fontFamilyRegular};
  margin-left: 16px;
`;

export const RowTwo = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ButtonBack = styled.TouchableOpacity``;

export const HeaderTitle = styled.Text`
  font-size: ${fonts.bigger};
  color: ${colors.white_correct};
  font-family: ${fonts.fontFamilyRegular};
  margin-top: 5px;
  margin-left: 16px;
`;

export const TitlePage = styled.Text`
  font-size: ${fonts.bigger};
  color: rgba(241, 241, 241, 0.4);
  font-family: ${fonts.fontFamilyRegular};
  margin: 36px 0 28px 5px;
`;

export const TextTerms = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.color_input_text};
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  margin-left: 6px;
  margin-bottom: 28px;
`;
