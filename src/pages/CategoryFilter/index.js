import React, {useState, useEffect, useCallback} from 'react';
import {Alert, KeyboardAvoidingView, Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {useNavigation} from '@react-navigation/native';
import IconF from 'react-native-vector-icons/Feather';
import storage from '@react-native-firebase/storage';
import {Checkbox} from 'react-native-paper';

import Loading from '../../components/Loading';
import InputFilter from '../../components/InputFilter';
import Button from '../../components/Button';
import {useSubCategories} from '../../hooks/SubCategories';
import {useSubGroups} from '../../hooks/SubGroups';

import {
  Container,
  Content,
  Row,
  HeaderTitle,
  FlatListStyled,
  ButtonBack,
  LogoIcon,
  Logo,
  Title,
  RowTwo,
  FlatListStyledHorizontal,
  RowThree,
  ExportAll,
  IconStyled,
  HeaderTitleTwo,
} from './styles';
import Item from './Components/Item';
import SubCategory from './Components/SubCategory';

import {colors} from '../../global';

const Category = props => {
  const navigation = useNavigation();
  const {subCategories} = useSubCategories();
  const {subGroups} = useSubGroups();
  const [loading, setLoading] = useState(false);
  const [category, setCategory] = useState(false);
  const [urlImage, setUrlImage] = useState();
  const [selectSubCategory, setSelectSubCategory] = useState();
  const [subsCategories, setSubsCategories] = useState([]);
  const [subsGroups, setSubsGroups] = useState([]);
  const [checked, setChecked] = useState(true);
  const [selectSubGroup, setSelectSubGroup] = useState([]);
  const [itemData, setItemData] = useState(false);
  const [text, setText] = useState('');
  const [isFocusedData, setIsFocusedData] = useState(false);
  const [hasFocusedData, setHasFocusedData] = useState(false);
  const [subsGroupsFiltered, setSubsGroupsFiltered] = useState([]);
  const [loadingSubs, setLoadingSubs] = useState(false);

  useEffect(() => {
    if (props && props.route.params) {
      setItemData(true);
    }
  }, [props]);

  useEffect(() => {
    async function getCategory() {
      const categoryStorage = await AsyncStorage.getItem('@categoryOrtopedia:');
      if (categoryStorage) {
        setCategory(JSON.parse(categoryStorage));
      }
    }
    getCategory();
  }, [itemData]);

  useEffect(() => {
    async function getImage() {
      if (category && category.icon) {
        storage()
          .ref(category.icon)
          .getDownloadURL()
          .then(url => {
            setUrlImage(url);
          });
      }
    }
    getImage();
  }, [category]);

  useEffect(() => {
    setLoading(true);
    if (subCategories && subCategories.length) {
      const newSubs =
        subCategories.filter(sub => {
          return sub.category.value === category.key && sub.active;
        }) || [];
      setSubsCategories(newSubs);
      setSelectSubCategory(newSubs[0]);
    }
    setLoading(false);
  }, [category, subCategories]);

  useEffect(() => {
    if (selectSubCategory && subGroups && subGroups.length) {
      const newSubs =
        subGroups.filter(sub => {
          return sub.sub_category.value === selectSubCategory.key && sub.active;
        }) || [];
      setSubsGroups(newSubs);
    } else {
      setSubsGroups([]);
    }
  }, [selectSubCategory, subGroups]);

  useEffect(() => {
    if (subsGroups && subsGroups.length) {
      let newSubs = [];
      setLoadingSubs(true);
      setTimeout(() => {
        subsGroups.forEach(element => {
          if (text) {
            if (element.name.toUpperCase().includes(text.toUpperCase())) {
              newSubs.push(element);
            }
          }
        });
        setSubsGroupsFiltered(newSubs);
        setLoadingSubs(false);
      }, 3000);
    }
  }, [subsGroups, text]);

  const handleQuestions = useCallback(() => {
    if (checked) {
      navigation.navigate('QuestionResolve', {item: subsGroups});
      return;
    }
    if (!selectSubGroup) {
      Alert.alert('ATENÇÃO', 'Selecione um sub grupo.');
      return;
    }
    navigation.navigate('QuestionResolve', {item: selectSubGroup});
  }, [selectSubGroup, subsGroups, navigation, checked]);

  return loading ? (
    <Loading />
  ) : (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <Content>
          <Row>
            {text || hasFocusedData ? (
              <ButtonBack
                onPress={() => {
                  setText(null);
                  setIsFocusedData(true);
                  setHasFocusedData(false);
                }}>
                <IconF
                  name="arrow-left"
                  color={colors.white_correct}
                  size={30}
                />
              </ButtonBack>
            ) : null}
            {itemData ? (
              <InputFilter
                value={text}
                placeholder="Selecione ou pesquise por assunto"
                icon="magnifier"
                onChangeText={e => setText(e)}
                setText={setText}
                handleInputBlurData={isFocusedData}
                setIsFocusedData={setIsFocusedData}
                setHasFocusedData={setHasFocusedData}
              />
            ) : null}
          </Row>
          {itemData ? null : (
            <>
              <RowTwo>
                {urlImage ? <LogoIcon source={{uri: urlImage}} /> : <Logo />}
                <Title>{category.name}</Title>
              </RowTwo>
              <Row>
                <HeaderTitle>
                  As questões serão filtradas com base nos assuntos que você
                  deixar ativado. Edite as preferências para intensificar seus
                  estudos!
                </HeaderTitle>
              </Row>
            </>
          )}
          {subsCategories &&
          subsCategories.length &&
          (!text || !hasFocusedData) ? (
            <FlatListStyledHorizontal
              data={
                subsCategories && subsCategories.length ? subsCategories : []
              }
              renderItem={({item}) => (
                <SubCategory
                  key={item.key}
                  item={item}
                  setSelectSubCategory={setSelectSubCategory}
                  selectSubCategory={selectSubCategory}
                />
              )}
              keyExtractor={item => item.id}
              horizontal
              showsVerticalScrollIndicator={false}
            />
          ) : null}
          {!text || !hasFocusedData ? (
            <>
              <RowThree>
                <ExportAll>Todos</ExportAll>
                <Checkbox
                  color={colors.orange}
                  uncheckedColor={colors.white}
                  status={checked ? 'checked' : 'unchecked'}
                  onPress={() => {
                    setChecked(!checked);
                  }}
                />
              </RowThree>
              <FlatListStyled
                itemData={itemData}
                data={subsGroups && subsGroups.length ? subsGroups : []}
                renderItem={({item}) => (
                  <Item
                    item={item}
                    key={item.key}
                    setSelectSubGroup={setSelectSubGroup}
                    selectSubGroup={selectSubGroup}
                    setChecked={setChecked}
                    checked={checked}
                  />
                )}
                keyExtractor={item => item.id}
                showsVerticalScrollIndicator={false}
              />
            </>
          ) : null}
          {loadingSubs && text ? (
            <Row>
              <IconStyled
                name="refresh"
                size={24}
                color="rgba(241, 241, 241, 0.4)"
              />
              <HeaderTitleTwo>Procurando por "{text}”...</HeaderTitleTwo>
            </Row>
          ) : null}
          {subsGroupsFiltered && subsGroupsFiltered.length && text ? (
            <FlatListStyled
              itemData={itemData}
              data={
                subsGroupsFiltered && subsGroupsFiltered.length
                  ? subsGroupsFiltered
                  : []
              }
              renderItem={({item}) => (
                <Item
                  item={item}
                  key={item.key}
                  setSelectSubGroup={setSelectSubGroup}
                  selectSubGroup={selectSubGroup}
                  setChecked={setChecked}
                  checked={checked}
                />
              )}
              keyExtractor={item => item.id}
              showsVerticalScrollIndicator={false}
            />
          ) : null}

          <Button onPress={handleQuestions}>Iniciar</Button>
        </Content>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default Category;
