import styled from 'styled-components/native';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {Dimensions} from 'react-native';
import {Image} from 'react-native';

import {colors, fonts} from '../../global';

const {width} = Dimensions.get('window');

export const Logo = styled(Image).attrs({
  source: require('../../assets/log-one.png'),
  resizeMode: 'contain',
})`
  position: absolute;
  width: 40px;
  height: 40px;
  top: 24px;
  left: 20px;
`;

const ImageStyle = styled.Image``;
export const Container = styled.View`
  flex: 1;
  background-color: ${colors.background_main};
  position: relative;
`;

export const Content = styled.View`
  flex: 1;
  width: ${width}px;
`;

export const ContainerHeader = styled.View`
  width: 100%;
  background-color: ${colors.background_main};
  justify-content: center;
  padding: 24px 20px;
  justify-content: space-between;
  flex-direction: row;
  z-index: 3;
  position: relative;
`;

export const ScrollViewStyled = styled.ScrollView`
  flex: 1;
  position: absolute;
  margin: auto;
  height: 100%;
  z-index: 2;
`;

export const Icon = styled(SimpleLineIcon)``;

export const IconFont = styled(FontAwesome)``;

export const HeaderProfile = styled.View`
  width: ${width}px;
  align-items: center;
  justify-content: center;
`;

export const HeaderImage = styled.TouchableOpacity`
  width: 120px;
  height: 120px;
  border-radius: 60px;
  border-width: 1px;
  align-items: center;
  justify-content: center;
  border-color: ${colors.white};
  position: relative;
`;

export const ImageUser = styled(ImageStyle).attrs({})`
  width: 120px;
  height: 120px;
  border-radius: 60px;
`;

export const HeaderName = styled.Text`
  font-size: ${fonts.biggerTwo};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyBold};
  text-align: center;
  width: 100%;
`;

export const ViewImageText = styled.View`
  position: absolute;
  top: 90%;
  justify-content: center;
  align-items: center;
  padding: 0;
  border-width: 3px;
  border-color: #f1f1f1;
  border-radius: 5px;
  margin: 0;
  padding: 0 5px;
  background-color: ${colors.orange};
`;

export const ImageText = styled.Text`
  font-size: ${fonts.small};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyBold};
  text-align: center;
`;

export const MyAccountText = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.orange};
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  width: 100%;
  padding: 0 24px;
  margin-top: 24px;
`;

export const MyAccountTextOut = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.orange};
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  width: 100%;
  padding: 0 24px;
  margin-top: 1px;
`;

export const DetailsItem = styled.TouchableOpacity`
  padding: 12px 24px;
  background-color: ${colors.background_secondary};
  border-top-width: 1px;
  border-top-color: ${colors.color_botton_inactive};
  border-bottom-width: 1px;
  border-bottom-color: ${colors.color_botton_inactive};
`;

export const DetailItemTitle = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.color_input_text};
  font-family: ${fonts.fontFamilyBold};
`;

export const DetailItemText = styled.Text`
  font-size: ${fonts.regular};
  color: ${({editable}) =>
    editable ? colors.color_input_text : colors.color_botton_inactive};
  font-family: ${fonts.fontFamilyBold};
`;

export const DetailItemTextOut = styled.Text`
  font-size: ${fonts.regular};
  font-family: ${fonts.fontFamilyBold};
  color: #ff6b4a;
`;

export const ModalSelection = styled.Modal``;

export const ModalContainer = styled.View`
  flex: 1;
  justify-content: center;
  padding: 30px;
  background-color: rgba(0, 0, 0, 0.5);
`;

export const ModalCard = styled.View`
  background-color: ${colors.background_main};
  padding: 30px;
  border-radius: 12px;
`;

export const ModalTitle = styled.Text`
  font-size: ${fonts.small};
  color: ${colors.white};
  width: 100%;
  text-align: center;
`;

export const ModalButton = styled.TouchableOpacity`
  align-self: flex-end;
  margin-top: 24px;
`;

export const ModalOption = styled.TouchableOpacity`
  margin-top: 24px;
`;

export const ModalButtonText = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  width: 100%;
  text-align: center;
`;
