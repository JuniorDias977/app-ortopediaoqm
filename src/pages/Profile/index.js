import React, {useState, useEffect, useCallback} from 'react';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import storage from '@react-native-firebase/storage';
import firestore from '@react-native-firebase/firestore';
import {Alert, Platform} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Loading from '../../components/Loading';
import {useAuth} from '../../hooks/Auth';

import {colors} from '../../global';
import {
  Container,
  Content,
  ScrollViewStyled,
  ContainerHeader,
  Logo,
  HeaderName,
  ImageText,
  HeaderProfile,
  HeaderImage,
  ImageUser,
  IconFont,
  MyAccountText,
  DetailsItem,
  DetailItemTitle,
  DetailItemText,
  DetailItemTextOut,
  MyAccountTextOut,
  ModalCard,
  ModalTitle,
  ModalButton,
  ModalButtonText,
  ModalOption,
  ModalSelection,
  ModalContainer,
  ViewImageText,
} from './styles';

const Profile = () => {
  const {user, signOut, updateUser} = useAuth();
  const [loading, setLoading] = useState(false);
  const [modalMediaVisible, setModalMediaVisible] = useState(false);
  const [urlData, setUrlData] = useState('');
  const navigation = useNavigation();

  useEffect(() => {
    async function getImage() {
      if (user && user.avatar) {
        storage()
          .ref(user.avatar)
          .getDownloadURL()
          .then(url => {
            setUrlData(url);
          })
          .catch(() => {
            setUrlData(user.avatar);
          });
      }
    }
    getImage();
  }, [user]);

  const handleImage = useCallback(
    async ({response, typeAction}) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        setLoading(true);
        const filename = response.uri.substring(
          response.uri.lastIndexOf('/') + 1,
        );
        try {
          // const uriD = await getPathForFirebaseStorage(response.uri)
          const source = {uri: response.uri};
          const uploadUri =
            Platform.OS === 'ios'
              ? source.uri.replace('file://', '')
              : source.uri;
          setUrlData(uploadUri);
          const task = storage()
            .ref('images/users')
            .child(`${filename}----${user.uid}----`)
            .putFile(uploadUri);
          const resp = await task;
          firestore().collection('users').doc(user.key).update({
            avatar: resp.metadata.fullPath,
          });
          updateUser(user);
          setLoading(false);
        } catch (e) {
          setLoading(false);
          console.error(e);
          Alert.alert('Erro ao enviar a imagem');
          return;
        }
        Alert.alert('Envio concluído!', 'Sua foto foi atualizada');
        setLoading(false);
      }
    },
    [user, updateUser],
  );

  const getImage = useCallback(
    typeAction => {
      const options = {
        title: 'Selecione a imagem da refeição',
        takePhotoButtonTitle: 'Tirar foto',
        chooseFromLibraryButtonTitle: 'Selecionar da galeria',
        cancelButtonTitle: 'Cancelar',
        storageOptions: {privateDirectory: true},
      };

      if (typeAction === 'Tirar foto') {
        try {
          launchCamera(options, response =>
            handleImage({response, typeAction}),
          );
        } catch (err) {}
      } else {
        try {
          launchImageLibrary(options, response =>
            handleImage({response, typeAction}),
          );
        } catch (err) {}
      }
    },
    [handleImage],
  );

  return loading ? (
    <Loading />
  ) : (
    <Container>
      <ScrollViewStyled contentContainerStyle={{flexGrow: 1}}>
        <ContainerHeader>
          <Logo />
          <HeaderName>Meu perfil</HeaderName>
        </ContainerHeader>
        <Content>
          <HeaderProfile>
            <HeaderImage
              onPress={() => setModalMediaVisible(!modalMediaVisible)}>
              {user && user.avatar && user.avatar !== '' && urlData ? (
                <>
                  <ImageUser source={{uri: urlData}} />
                </>
              ) : (
                <IconFont name="camera" size={35} color={colors.white} />
              )}
              {user.is_studenty ? (
                <ViewImageText>
                  <ImageText>Sou aluno</ImageText>
                </ViewImageText>
              ) : null}
            </HeaderImage>
          </HeaderProfile>
          <MyAccountText>Minha conta</MyAccountText>
          <DetailsItem>
            <DetailItemTitle>Meu e-mail</DetailItemTitle>
            <DetailItemText editable={false}>{user.email}</DetailItemText>
          </DetailsItem>
          <DetailsItem onPress={() => navigation.navigate('ChangeProfile')}>
            <DetailItemTitle>Meu nome</DetailItemTitle>
            <DetailItemText editable={true}>
              {user.name || user.displayName}
            </DetailItemText>
          </DetailsItem>
          <DetailsItem
            onPress={() => navigation.navigate('ChangeProfilePhone')}>
            <DetailItemTitle>Telefone</DetailItemTitle>
            <DetailItemText editable={true}>{user.phone}</DetailItemText>
          </DetailsItem>
          <DetailsItem
            onPress={() => navigation.navigate('ChangeProfileResidence')}>
            <DetailItemTitle>Redidência</DetailItemTitle>
            <DetailItemText editable={true}>
              {user.residence ? `Sou ${user.residence}` : ''}
            </DetailItemText>
          </DetailsItem>
          <MyAccountText>Minha assinatura</MyAccountText>
          <DetailsItem>
            <DetailItemText editable={false}>
              {user.is_admin
                ? 'Sou administrador'
                : `${
                    user && user.level === 'studenty'
                      ? 'Sou aluno'
                      : 'Não sou aluno'
                  }`}
            </DetailItemText>
          </DetailsItem>
          <MyAccountText>Segurança</MyAccountText>
          <DetailsItem onPress={() => navigation.navigate('ChangeProfilePass')}>
            <DetailItemText editable={true}>Alterar senha</DetailItemText>
          </DetailsItem>
          <MyAccountTextOut> </MyAccountTextOut>
          <DetailsItem onPress={signOut}>
            <DetailItemTextOut>Sair</DetailItemTextOut>
          </DetailsItem>
        </Content>
        <ModalSelection
          animationType="fade"
          visible={modalMediaVisible}
          transparent
          callback={data => {
            setModalMediaVisible(false);

            if (data) {
              requestAnimationFrame(() => {
                getImage(data);
              });
            }
          }}>
          <ModalContainer>
            <ModalCard>
              <ModalTitle>Selecione uma foto de perfil</ModalTitle>
              <ModalOption
                onPress={() => {
                  setModalMediaVisible(false);

                  requestAnimationFrame(() => {
                    getImage('Tirar foto');
                  });
                }}>
                <ModalButtonText>Tirar foto</ModalButtonText>
              </ModalOption>
              <ModalOption
                onPress={() => {
                  setModalMediaVisible(false);

                  requestAnimationFrame(() => {
                    getImage('Selecionar da galeria');
                  });
                }}>
                <ModalButtonText>Selecionar da galeria</ModalButtonText>
              </ModalOption>
              <ModalButton onPress={() => setModalMediaVisible(false)}>
                <ModalButtonText>CANCELAR</ModalButtonText>
              </ModalButton>
            </ModalCard>
          </ModalContainer>
        </ModalSelection>
      </ScrollViewStyled>
    </Container>
  );
};

export default Profile;
