import React, {useState, useEffect, useRef, useCallback} from 'react';
import {Modalize} from 'react-native-modalize';
import {ProgressBar} from 'react-native-paper';
import firestore from '@react-native-firebase/firestore';
import {useNavigation} from '@react-navigation/native';
import {View} from 'react-native';
import {Dimensions} from 'react-native';
import {format} from 'date-fns';
// import crashlytics from '@react-native-firebase/crashlytics';

import Loading from '../../components/Loading';
import {useQuestionsSimulate} from '../../hooks/QuestionsSimulate';
import {useSimulateUser} from '../../hooks/SimulateUser';

import {
  Container,
  Content,
  ScrollViewStyled,
  ContainerHeader,
  Logo,
  FlatListStyled,
  HeaderName,
  ContentNoBody,
  TextNoBody,
  NextQuestion,
  NextQuestionText,
  IconFeather,
  Row,
  Confirm,
  Count,
  TitleCategory,
  ModalCard,
  ModalTitle,
  ModalButton,
  ModalButtonText,
  RowTwo,
  ModalSelection,
  ModalContainer,
  ModalButtonTextTwo,
  Icon,
  ModalTitleFinished,
  ModalSubTitleFinished,
  TextFinished,
  MotalTitleCount,
} from './styles';
import Item from './Components/Item';
import {colors} from '../../global';
import Button from '../../components/Button';
import {useAuth} from '../../hooks/Auth';

const {width, height} = Dimensions.get('window');

const Profile = ({route}) => {
  const modalizeRef = useRef(null);
  const ref = useRef(null);

  const navigation = useNavigation();
  const [data, setData] = useState();

  const {user} = useAuth();

  const {questionsSimulate, loadingQuestionsSimulate} = useQuestionsSimulate();
  const {simulatesUser, loadingSimulateUser} = useSimulateUser();

  const [loading, setLoading] = useState(false);
  const [questionsData, setQuestionsData] = useState([]);
  const [durationItem, setDurationItem] = useState(0);
  const [isInit, setIsInit] = useState(true);
  const [paused, setPaused] = useState(true);
  const [tenMinutes, setTenMinutes] = useState(false);
  const [fiveMinutes, setFiveMinutes] = useState(false);

  const [modalMediaVisible, setModalMediaVisible] = useState(false);
  const [select, setSelect] = useState(false);
  const [selected, setSelected] = useState('');
  const [indexNext, setIndexNext] = useState(0);
  const [isLast, setIsLast] = useState(false);
  const [finished, setFinished] = useState(false);
  const [questionsSolved, setQuestionsSolved] = useState(0);
  const [userData, setUserData] = useState();
  const [simulateData, setSimulateData] = useState();
  const [correct, setCorrect] = useState(false);
  const [errored, setErrored] = useState(false);
  const [resolvedSimulate, setResolvedSimulate] = useState(false);

  useEffect(() => {
    try {
      firestore()
        .collection('settings')
        .onSnapshot(querySnapshot => {
          let categoriesArr = [];
          if (querySnapshot) {
            querySnapshot.forEach(documentSnapshot => {
              categoriesArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
          }
          setData(categoriesArr[0]);
        });
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, []);

  useEffect(() => {
    try {
      if (route.params.userData) {
        setUserData(route.params.userData);
      }
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, [route]);

  useEffect(() => {
    try {
      if (
        simulatesUser &&
        simulatesUser.length &&
        route &&
        route.params &&
        route.params.item &&
        route.params.item.key
      ) {
        setQuestionsSolved(
          simulatesUser.find(
            sm =>
              sm.status === 'in_progress' &&
              sm.simulate === route.params.item.key,
          ) &&
            simulatesUser.find(
              sm =>
                sm.status === 'in_progress' &&
                sm.simulate === route.params.item.key,
            ).questions_solved
            ? simulatesUser.find(
                sm =>
                  sm.status === 'in_progress' &&
                  sm.simulate === route.params.item.key,
              ).questions_solved
              ? simulatesUser.find(
                  sm =>
                    sm.status === 'in_progress' &&
                    sm.simulate === route.params.item.key,
                ).questions_solved
              : 0
            : 0,
        );
      }
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, [simulatesUser, route]);

  useEffect(() => {
    try {
      if (
        simulatesUser &&
        simulatesUser.length &&
        route.params.item &&
        route.params.item.key &&
        simulatesUser.find(
          sm =>
            sm.status === 'in_progress' &&
            sm.simulate === route.params.item.key,
        )
      ) {
        setIsInit(false);
        setSimulateData(
          simulatesUser.find(
            sm =>
              sm.status === 'in_progress' &&
              sm.simulate === route.params.item.key,
          ),
        );
        setQuestionsSolved(
          simulatesUser.find(
            sm =>
              sm.status === 'in_progress' &&
              sm.simulate === route.params.item.key,
          ).questions_solved
            ? simulatesUser.find(
                sm =>
                  sm.status === 'in_progress' &&
                  sm.simulate === route.params.item.key,
              ).questions_solved
            : 0,
        );
        /***SE EXISTIR LEVAR PARA ULTIMA QUESTÃO RESPONDIDA**/
        if (
          simulatesUser.find(
            sm =>
              sm.status === 'in_progress' &&
              sm.simulate === route.params.item.key,
          ) &&
          simulatesUser.find(
            sm =>
              sm.status === 'in_progress' &&
              sm.simulate === route.params.item.key,
          ).questions_solved
        ) {
          const wait = new Promise(resolve => setTimeout(resolve, 500));
          wait.then(() => {
            ref.current?.scrollToIndex({
              index: simulatesUser.find(
                sm =>
                  sm.status === 'in_progress' &&
                  sm.simulate === route.params.item.key,
              ).questions_solved,
              animated: true,
            });
          });
        }
      }
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [route]);

  useEffect(() => {
    try {
      if (route.params.item && isInit) {
        const durationT = route.params.item.duration.split(':');
        setDurationItem(
          parseInt(durationT[0], 10) * 60 * 60 +
            parseInt(durationT[1], 10) * 60,
        );
      } else if (
        simulatesUser &&
        simulatesUser.length &&
        route.params.item &&
        route.params.item.key &&
        simulatesUser.find(
          sm =>
            sm.status === 'in_progress' &&
            sm.simulate === route.params.item.key,
        )
      ) {
        setDurationItem(
          simulatesUser.find(
            sm =>
              sm.status === 'in_progress' &&
              sm.simulate === route.params.item.key,
          ).time,
        );
      }
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, [route, isInit, simulatesUser]);

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  useEffect(() => {
    try {
      const interval = setInterval(() => {
        if (paused) {
          setDurationItem(durationItem - 1 > 0 ? durationItem - 1 : 0);
          if (durationItem - 1 > 0) {
            if (durationItem < 60 * 10) {
              setTenMinutes(true);
            }
            if (durationItem < 60 * 5) {
              setTenMinutes(false);
              setFiveMinutes(true);
            }
            if (
              simulatesUser.find(
                sm =>
                  sm.status === 'in_progress' &&
                  sm.simulate === route.params.item.key,
              )
            ) {
              firestore()
                .collection('simulates_user')
                .doc(
                  simulatesUser.find(
                    sm =>
                      sm.status === 'in_progress' &&
                      sm.simulate === route.params.item.key,
                  ).key,
                )
                .update({
                  time: durationItem - 1,
                });
            }
          } else {
            if (
              simulatesUser.find(
                sm =>
                  sm.status === 'in_progress' &&
                  sm.simulate === route.params.item.key,
              )
            ) {
              firestore()
                .collection('simulates_user')
                .doc(
                  simulatesUser.find(
                    sm =>
                      sm.status === 'in_progress' &&
                      sm.simulate === route.params.item.key,
                  ).key,
                )
                .update({
                  time: 0,
                  status: 'finished',
                });
              setFinished(true);
              setResolvedSimulate(true);
              onOpen();
            }
          }
        }
      }, 1000);
      return () => clearInterval(interval);
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, [route, durationItem, simulatesUser, paused]);

  useEffect(() => {
    try {
      if (questionsSimulate) {
        setLoading(true);
        let arrData = [];
        questionsSimulate.forEach(qt => {
          if (route.params.item) {
            if (qt.simulate.value === route.params.item.key) {
              if (!arrData.find(ar => ar.key === qt.key)) {
                arrData.push(qt);
              }
            }
          }
        });
        setQuestionsData(arrData);
        setLoading(false);
      }
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, [questionsSimulate, route]);

  const handleSelect = useCallback((indx, val, finish) => {
    try {
      setSelect(true);
      setIndexNext(indx);
      setSelected(val);
      setIsLast(finish);
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, []);

  const handleNext = useCallback(() => {
    try {
      setLoading(true);
      firestore()
        .collection('history')
        .add({
          correct: correct,
          errored: errored,
          user: user.key,
          type: 'simulate',
          date: format(new Date(), 'yyyy-MM-dd HH:mm:ss'),
          selected,
          category: `${route.params.item.category.label}/
        ${
          questionsData && questionsData[0]
            ? questionsData[0].simulate.label
            : ''
        }`,
          simulate: {
            ...simulatesUser.find(
              sm =>
                sm.status === 'in_progress' &&
                sm.simulate === route.params.item.key,
            ),
            finished: isLast,
            time: durationItem - 1,
          },
          question: questionsData[indexNext - 1],
        })
        .catch(function () {
          setLoading(false);
        });
      firestore()
        .collection('simulates_user')
        .doc(
          simulatesUser.find(
            sm =>
              sm.status === 'in_progress' &&
              sm.simulate === route.params.item.key,
          ).key,
        )
        .update({
          questions_solved: simulatesUser.find(
            sm =>
              sm.status === 'in_progress' &&
              sm.simulate === route.params.item.key,
          ).questions_solved
            ? simulatesUser.find(
                sm =>
                  sm.status === 'in_progress' &&
                  sm.simulate === route.params.item.key,
              ).questions_solved + 1
            : 1,
        });
      if (!isLast) {
        ref.current.scrollToIndex({
          animated: true,
          index: indexNext,
          viewPosition: indexNext,
        });
        setSelect(false);
        setIndexNext(0);
        setSelected('');
        setIsLast(false);
        setLoading(false);
        return;
      }
      if (
        simulatesUser.find(
          sm =>
            sm.status === 'in_progress' &&
            sm.simulate === route.params.item.key,
        )
      ) {
        firestore()
          .collection('simulates_user')
          .doc(
            simulatesUser.find(
              sm =>
                sm.status === 'in_progress' &&
                sm.simulate === route.params.item.key,
            ).key,
          )
          .update({
            time: 0,
            status: 'finished',
          });

        setFinished(false);
        setPaused(false);
        setLoading(false);
        setResolvedSimulate(true);
      }
      onOpen();
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, [
    correct,
    errored,
    user.key,
    selected,
    simulatesUser,
    isLast,
    durationItem,
    questionsData,
    route,
    indexNext,
  ]);

  // const handleNextData = useCallback(async () => {
  //   onButtonPress(index + 1);
  // }, []);

  function pad(num) {
    return ('0' + num).slice(-2);
  }
  function hhmmss(secs) {
    var minutes = Math.floor(secs / 60);
    secs = secs % 60;
    var hours = Math.floor(minutes / 60);
    minutes = minutes % 60;
    return `${pad(hours)}h${pad(minutes)}m${pad(secs)}s`;
    // return pad(hours)+":"+pad(minutes)+":"+pad(secs); for old browsers
  }

  // const onClose = useCallback(() => {
  //   modalizeRef.current?.close();
  // }, [modalizeRef]);

  return loading || loadingQuestionsSimulate || loadingSimulateUser ? (
    <Loading />
  ) : (
    <>
      <Container
        style={{
          opacity: modalMediaVisible ? 0.8 : 1,
          backgroundColor: modalMediaVisible ? 'black' : colors.background_main,
        }}>
        <View
          style={{
            position: 'absolute',
            width: modalMediaVisible ? width : 0,
            minHeight: modalMediaVisible ? height : 0,
            backgroundColor: 'black',
            top: 0,
            left: 0,
            zIndex: 99999,
          }}
        />
        <ScrollViewStyled
          contentContainerStyle={{flexGrow: 1}}
          showsVerticalScrollIndicator={false}>
          <ProgressBar
            progress={
              questionsSolved
                ? questionsSolved / questionsData.length >= 100
                  ? 100
                  : questionsSolved / questionsData.length === 0
                  ? 0
                  : questionsSolved / questionsData.length
                : 0
            }
            color={colors.orange}
            style={{
              backgroundColor: 'rgba(241, 241, 241, 0.4)',
              minWidth: '100%',
              marginTop: 12,
            }}
          />

          <ContainerHeader>
            <Logo />
            <HeaderName>{`${questionsSolved + 1}/${
              questionsData.length
            }`}</HeaderName>
          </ContainerHeader>

          <Content>
            <TitleCategory>
              {route.params.item.category.label}/
              {questionsData && questionsData[0]
                ? questionsData[0].simulate.label
                : ''}
            </TitleCategory>

            {questionsData && questionsData.length ? (
              <FlatListStyled
                ref={ref}
                horizontal
                scrollEnabled={false}
                showsHorizontalScrollIndicator={false}
                initialNumToRender={100}
                data={
                  questionsData && questionsData.length ? questionsData : []
                }
                renderItem={({item, index}) => (
                  <Item
                    item={item}
                    index={index}
                    selected={selected}
                    // setIndexData={setIndexData}
                    simulateId={route.params.item.key}
                    onButtonPress={handleSelect}
                    lastIndex={questionsData.length - 1}
                    simulateData={simulateData}
                    setErrored={setErrored}
                    setCorrect={setCorrect}
                  />
                )}
                keyExtractor={item => item.key}
              />
            ) : (
              <ContentNoBody>
                <TextNoBody>Ainda estamos preparando este conteúdo.</TextNoBody>
              </ContentNoBody>
            )}
          </Content>
          <Row>
            <NextQuestion
              onPress={() => {
                if (resolvedSimulate) {
                  navigation.goBack();
                }
              }}>
              {resolvedSimulate ? (
                <NextQuestionText>Voltar</NextQuestionText>
              ) : (
                <>
                  <IconFeather
                    name="clock"
                    size={18}
                    color={
                      tenMinutes
                        ? '#ffc800'
                        : `${fiveMinutes ? '#ff5630' : colors.white}`
                    }
                    style={{marginRight: 8, opacity: 0.4}}
                  />
                  <NextQuestionText
                    style={{opacity: tenMinutes || fiveMinutes ? 1 : 0.4}}
                    tenMinutes={tenMinutes}
                    fiveMinutes={fiveMinutes}>
                    {hhmmss(durationItem)}
                  </NextQuestionText>
                </>
              )}
            </NextQuestion>
            {!resolvedSimulate ? (
              select ? (
                <NextQuestion
                  onPress={handleNext}
                  style={{backgroundColor: colors.orange}}>
                  <NextQuestionText>Confirmar Alternativa</NextQuestionText>
                  <Icon
                    name="check"
                    size={22}
                    color={colors.white}
                    style={{marginLeft: 8}}
                  />
                </NextQuestion>
              ) : (
                <NextQuestion onPress={() => setModalMediaVisible(true)}>
                  <NextQuestionText>Pausar simulado</NextQuestionText>
                  <IconFeather
                    name="x"
                    size={22}
                    color={colors.white}
                    style={{marginLeft: 8}}
                  />
                </NextQuestion>
              )
            ) : null}
          </Row>
        </ScrollViewStyled>
        <Modalize
          ref={modalizeRef}
          withHandle={false}
          scrollViewProps={{
            contentContainerStyle: {
              minHeight: '100%',
              maxHeight: '100%',
            },
          }}
          modalStyle={{
            minHeight: '90%',
            maxHeight: '90%',
            backgroundColor: colors.background_main,
          }}>
          {finished ? (
            <>
              <ModalTitleFinished>Que pena!</ModalTitleFinished>
              <ModalSubTitleFinished>
                Tempo limite atingido.
              </ModalSubTitleFinished>
              <Count />
              <MotalTitleCount>0h00m</MotalTitleCount>
              <TextFinished>
                Você atingiu o tempo limite para responder todas as questões
                desse simulado, acompanhe o seu desempenho.
              </TextFinished>
              <View style={{padding: 24}}>
                <Button
                  onPress={() => {
                    if (data && data.free) {
                      navigation.navigate('DESEMPENHO');
                    } else if (data && !data.free) {
                      userData && userData.is_studenty
                        ? navigation.navigate('DESEMPENHO')
                        : navigation.navigate('Alert');
                    }
                  }}>
                  Ver desempenho
                </Button>
              </View>
            </>
          ) : (
            <>
              <ModalTitleFinished>Parabéns</ModalTitleFinished>
              <ModalSubTitleFinished>
                Você concluiu o simulado.
              </ModalSubTitleFinished>
              <Confirm />
              <TextFinished>
                Agora você pode verificar o desempenho total desse simulado e
                entender quais assuntos você precisa estudar um pouco mais.
              </TextFinished>
              <View style={{padding: 24}}>
                <Button
                  onPress={() => {
                    if (data && data.free) {
                      navigation.navigate('DESEMPENHO');
                    } else if (data && !data.free) {
                      userData && userData.is_studenty
                        ? navigation.navigate('DESEMPENHO')
                        : navigation.navigate('Alert');
                    }
                  }}>
                  Ver desempenho
                </Button>
              </View>
            </>
          )}
        </Modalize>
      </Container>
      <ModalSelection
        animationType="fade"
        visible={modalMediaVisible}
        transparent
        callback={dt => {
          setModalMediaVisible(false);

          if (dt) {
            // requestAnimationFrame(() => {
            //   getImage(data);
            // });
          }
        }}>
        <ModalContainer>
          <ModalCard>
            <ModalTitle>Pausar simulado?</ModalTitle>

            <ModalButtonText>
              Seu simulado será pausado e você poderá concluí-lo futuramente! Ao
              retomar, você irá seguir dessa seção.
            </ModalButtonText>
            <RowTwo
              style={{
                justifyContent: 'flex-end',
                borderTopWidth: 1,
                borderTopColor: 'rgba(241, 241, 241, 0.2)',
                paddingRight: 24,
              }}>
              <ModalButton onPress={() => setModalMediaVisible(false)}>
                <ModalButtonTextTwo>CANCELAR</ModalButtonTextTwo>
              </ModalButton>
              <ModalButton
                onPress={() => {
                  setPaused(true);
                  setModalMediaVisible(false);
                  navigation.goBack();
                }}>
                <ModalButtonTextTwo>Ok</ModalButtonTextTwo>
              </ModalButton>
            </RowTwo>
          </ModalCard>
        </ModalContainer>
      </ModalSelection>
    </>
  );
};

export default Profile;
