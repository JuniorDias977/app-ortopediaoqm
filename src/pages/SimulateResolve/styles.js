import styled, {css} from 'styled-components/native';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FeatherIcon from 'react-native-vector-icons/Feather';
import {Dimensions} from 'react-native';
import {Image} from 'react-native';

import {colors, fonts} from '../../global';

const {width} = Dimensions.get('window');
export const IconFeather = styled(FeatherIcon)``;

export const Logo = styled(Image).attrs({
  source: require('../../assets/log-one.png'),
  resizeMode: 'contain',
})`
  position: absolute;
  width: 40px;
  height: 40px;
  top: 22px;
  left: 20px;
`;
export const Adjust = styled(Image).attrs({
  source: require('../../assets/adjust.png'),
  resizeMode: 'contain',
})`
  width: 24px;
  height: 24px;
`;

export const Confirm = styled(Image).attrs({
  source: require('../../assets/confirm.png'),
  resizeMode: 'contain',
})`
  width: 250px;
  height: 250px;
  align-self: center;
`;

export const Count = styled(Image).attrs({
  source: require('../../assets/count.png'),
  resizeMode: 'contain',
})`
  margin-top: 24px;
  width: 230px;
  height: 230px;
  align-self: center;
`;

export const ButtonAdjust = styled.TouchableOpacity`
  top: 26px;
  right: 20px;
  position: absolute;
`;

export const Container = styled.View`
  flex: 1;
  background-color: ${colors.background_main};
  position: relative;
`;

export const Content = styled.View`
  flex: 1;
  width: ${width}px;
`;

export const ContentNoBody = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  width: ${width}px;
  padding: 24px;
`;

export const ContainerHeader = styled.View`
  width: 100%;
  background-color: ${colors.background_main};
  justify-content: center;
  padding: 24px 20px 0;
  justify-content: space-between;
  flex-direction: row;
  z-index: 3;
  position: relative;
`;

export const ScrollViewStyled = styled.ScrollView`
  flex: 1;
  position: absolute;
  margin: auto;
  height: 100%;
  z-index: 2;
`;

export const Icon = styled(SimpleLineIcon)``;

export const Row = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 24px;
`;

export const RowTwo = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
  margin-top: 24px;
`;

export const IconFont = styled(FontAwesome)``;

export const HeaderName = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyBold};
  text-align: right;
  width: 100%;
`;

export const FlatListStyled = styled.FlatList`
  width: 100%;
`;

export const TextNoBody = styled.Text`
  font-size: ${fonts.bigger};
  color: rgba(241, 241, 241, 0.4);
  font-family: ${fonts.fontFamilyBold};
  text-align: center;
  width: 100%;
`;

export const NextQuestion = styled.TouchableOpacity`
  align-self: flex-end;
  flex-direction: row;
  background-color: #262626;
  padding: 12px;
  border-radius: 18px;
  margin-top: 12px;
  margin-bottom: 12px;
`;

export const NextQuestionText = styled.Text`
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.small};
  ${({tenMinutes}) =>
    tenMinutes &&
    css`
      color: #ffc800;
    `}
  ${({fiveMinutes}) =>
    fiveMinutes &&
    css`
      color: #ff5630;
    `}
`;

export const TitleCategory = styled.Text`
  font-size: ${fonts.small};
  text-align: center;
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  background-color: #0073be;
  padding: 0 18px;
  position: absolute;
  top: ${({explain}) => (explain ? '7%' : '3%')};

  border-radius: 24px;
  z-index: 99999;
  align-self: center;
`;

export const ModalSelection = styled.Modal``;

export const ModalContainer = styled.View`
  flex: 1;
  justify-content: center;
  padding: 30px;
`;

export const ModalCard = styled.View`
  background-color: #333333;
  border-radius: 12px;
`;

export const ModalTitle = styled.Text`
  font-size: ${fonts.bigger};
  color: ${colors.white};
  width: 100%;
  text-align: center;
  padding: 30px 30px 0;
  margin-bottom: 26px;
`;

export const ModalButton = styled.TouchableOpacity`
  align-self: flex-end;
  margin-top: 24px;
  margin-left: 16px;
`;

export const ModalOption = styled.TouchableOpacity`
  margin-top: 24px;
`;

export const ModalButtonText = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  width: 100%;
  text-align: center;
  padding: 0 30px;
  opacity: 0.6;
`;

export const ModalButtonTextTwo = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.orange};
  font-family: ${fonts.fontFamilyRegular};
  width: 100%;
  text-align: center;
  padding: 0 0 24px;
`;
export const ModalTitleFinished = styled.Text`
  font-size: ${fonts.biggerTwo};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  width: 100%;
  text-align: center;
  padding: 36px 0 0;
`;

export const MotalTitleCount = styled.Text`
  font-size: ${fonts.biggerTwo};
  color: ${colors.white};
  opacity: 0.2;
  font-family: ${fonts.fontFamilyRegular};
  width: 100%;
  text-align: center;
`;

export const ModalSubTitleFinished = styled.Text`
  font-size: ${fonts.bigger};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  width: 100%;
  text-align: center;
  padding: 0 24px 0;
`;

export const TextFinished = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  width: 100%;
  text-align: center;
  padding: 24px;
`;
