import React, {useCallback, useState, useEffect} from 'react';
import firestore from '@react-native-firebase/firestore';
// import crashlytics from '@react-native-firebase/crashlytics';

import {useAuth} from '../../../../hooks/Auth';
import {colors} from '../../../../global';

import {
  Container,
  Title,
  Icon,
  TouchableOpacity,
  RowItem,
  TextAnser,
} from './styles';

const Item = ({
  item,
  index,
  onButtonPress,
  lastIndex,
  selected,
  setCorrect,
  setErrored,
}) => {
  const {user} = useAuth();
  const [splited, setSplited] = useState(true);

  useEffect(() => {
    try {
      firestore()
        .collection('users')
        .where('key', '==', user.key)
        .onSnapshot(querySnapshot => {
          let usersArr = [];
          querySnapshot.forEach(documentSnapshot => {
            usersArr.push({
              ...documentSnapshot.data(),
              key: documentSnapshot.id,
            });
          });
        });
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, [user]);

  const handleNext = useCallback(
    async val => {
      try {
        if (index !== lastIndex) {
          onButtonPress(index + 1, val, false);
          setSplited(true);
        } else {
          onButtonPress(index, val, true);
        }
      } catch (error) {
        console.log(error);
        // crashlytics().recordError(error);
      }
    },
    [index, onButtonPress, lastIndex],
  );

  const handleText = useCallback((val, splt) => {
    try {
      if (val.length > 200 && splt) {
        return `${val.slice(0, 200)}...`;
      }
      if (val.length > 200 && !splt) {
        return val;
      }

      return val;
    } catch (error) {
      console.log(error);
      // crashlytics().recordError(error);
    }
  }, []);

  // const handleCorrectOrErrored = useCallback(
  //   (valCorrect, valError) => {
  //     if (item) {
  //     }
  //   },
  //   [user, item, simulateId, simulateData],
  // );

  // useEffect(() => {
  //   if (correct || errored) {
  //     console.log(correct, errored);
  //     handleCorrectOrErrored(correct, errored);
  //   }
  // }, [correct, errored, handleCorrectOrErrored]);

  return (
    <Container>
      <>
        <Title>{handleText(item.description, splited)}</Title>
        {item.description.length > 200 ? (
          <TouchableOpacity onPress={() => setSplited(!splited)}>
            <Icon
              name={!splited ? 'arrow-up' : 'arrow-down'}
              size={22}
              color={colors.white}
            />
          </TouchableOpacity>
        ) : null}
      </>
      <RowItem
        selected={selected === 'a'}
        onPress={() => {
          handleNext('a');
          if (item.is_correct_a) {
            setCorrect(true);
            setErrored(false);
          } else {
            setErrored(true);
            setCorrect(false);
          }
          // setClicked('a');
          // setHidden(true);
          // setDisabled(true);
        }}>
        {/* {!errored && !correct ? (
          <TextItem selected={selected === 'a'}>A</TextItem>
        ) : null} */}
        <TextAnser selected={selected === 'a'}>{item.alternative_a}</TextAnser>
      </RowItem>
      <RowItem
        selected={selected === 'b'}
        onPress={() => {
          handleNext('b');
          if (item.is_correct_b) {
            setCorrect(true);
            setErrored(false);
          } else {
            setErrored(true);
            setCorrect(false);
          }
          // setClicked('b');
          // setHidden(true);
          // setDisabled(true);
        }}>
        {/* {!errored && !correct ? (
          <TextItem selected={selected === 'b'}>B</TextItem>
        ) : null} */}
        <TextAnser selected={selected === 'b'}>{item.alternative_b}</TextAnser>
      </RowItem>
      <RowItem
        selected={selected === 'c'}
        onPress={() => {
          handleNext('c');
          if (item.is_correct_c) {
            setCorrect(true);
            setErrored(false);
          } else {
            setErrored(true);
            setCorrect(false);
          }
          // setClicked('c');
          // setHidden(true);
          // setDisabled(true);
        }}>
        {/* {!errored && !correct ? (
          <TextItem selected={selected === 'c'}>C</TextItem>
        ) : null} */}
        <TextAnser selected={selected === 'c'}>{item.alternative_c}</TextAnser>
      </RowItem>
      <RowItem
        selected={selected === 'd'}
        onPress={() => {
          handleNext('d');
          if (item.is_correct_d) {
            setCorrect(true);
            setErrored(false);
          } else {
            setErrored(true);
            setCorrect(false);
          }
          // setClicked('d');
          // setHidden(true);
          // setDisabled(true);
        }}>
        {/* {!errored && !correct ? (
          <TextItem selected={selected === 'd'}>D</TextItem>
        ) : null} */}
        <TextAnser selected={selected === 'd'}>{item.alternative_d}</TextAnser>
      </RowItem>
    </Container>
  );
};

export default Item;
