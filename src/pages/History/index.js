import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {KeyboardAvoidingView, Platform, ScrollView} from 'react-native';

import {Container, Content, ContainerHeader, Logo, HeaderName} from './styles';

import Questions from './Components/Questions';
import Simulates from './Components/Simulates';
import {colors} from '../../global';

const History = () => {
  const Tab = createMaterialTopTabNavigator();

  return (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <ContainerHeader>
        <Logo />
        <HeaderName>Desempenho</HeaderName>
      </ContainerHeader>
      <Container>
        <ScrollView
          contentContainerStyle={{flexGrow: 1}}
          style={{width: '100%'}}>
          <Content>
            <Tab.Navigator
              tabBarOptions={{
                activeTintColor: colors.orange,
                inactiveTintColor: colors.color_botton_inactive,
                activeBorderColor: colors.orange,
                style: {
                  backgroundColor: colors.background_main,
                  height: 60,
                  elevation: 0, // for Android
                  borderBottomWidth: 1,
                  borderBottomColor: colors.color_botton_inactive,
                  shadowOffset: {
                    width: 0,
                  },
                  paddingBottom: 8,
                },
                indicatorStyle: {
                  backgroundColor: colors.orange,
                },
              }}>
              <Tab.Screen name="Questões" component={Questions} />
              <Tab.Screen name="Simulados" component={Simulates} />
            </Tab.Navigator>
          </Content>
        </ScrollView>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default History;
