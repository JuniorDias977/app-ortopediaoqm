import styled from 'styled-components/native';
import {Dimensions} from 'react-native';

import {colors, fonts} from '../../../../../../global';

const {width} = Dimensions.get('window');

export const Container = styled.View`
  border-width: 1px;
  border-color: #333333;
  border-radius: 12px;
  margin-right: ${({marginBottom}) => (marginBottom ? 0 : 12)}px;
  margin-bottom: ${({marginBottom}) => (marginBottom ? 12 : 0)}px;
  width: ${width - 48}px;
`;

export const Column = styled.View`
  flex-direction: column;
  flex-basis: ${({basis}) => (basis ? basis : 30)}%;
`;

export const Row = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const ContainerHeader = styled.View`
  background-color: #333;
  border-top-left-radius: 12px;
  border-top-right-radius: 12px;
  padding: 16px 20px;
`;

export const Content = styled.View`
  border-bottom-left-radius: 12px;
  border-bottom-right-radius: 12px;
  padding: 16px 20px;
`;

export const TitleLabel = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  width: 100%;
`;

export const TitleLabelTwo = styled.Text`
  font-size: ${fonts.small};
  color: ${({color}) => (color ? color : colors.white)};
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  width: 100%;
`;

export const Label = styled.Text`
  font-size: 9px;
  color: rgba(241, 241, 241, 0.6);
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  width: 100%;
`;
