import React from 'react';

import {
  Container,
  TitleLabel,
  ContainerHeader,
  Column,
  Label,
  Content,
  Row,
  TitleLabelTwo,
} from './styles';

const Item = ({item, marginBottom = false}) => {
  return (
    <Container marginBottom={marginBottom}>
      <ContainerHeader>
        <TitleLabel>{item.category_name}</TitleLabel>
      </ContainerHeader>
      <Content>
        <Row>
          <Column basis={30}>
            <TitleLabel>{item.correct + item.error + item.skiped}</TitleLabel>
            <Label>Questões</Label>
          </Column>
          <Column basis={20}>
            <TitleLabelTwo color="#A9C21B">
              {(
                (100 * item.correct) /
                (item.correct + item.error + item.skiped)
              ).toFixed(0)}
              %
            </TitleLabelTwo>
            <Label>{item.correct} Acertos</Label>
          </Column>
          <Column basis={20}>
            <TitleLabelTwo color="#FFC800">
              {(
                (100 * item.skiped) /
                (item.correct + item.error + item.skiped)
              ).toFixed(0)}
              %
            </TitleLabelTwo>
            <Label>{item.skiped} Pulo</Label>
          </Column>
          <Column basis={20}>
            <TitleLabelTwo color="#FF5630">
              {(
                (100 * item.error) /
                (item.correct + item.error + item.skiped)
              ).toFixed(0)}
              %
            </TitleLabelTwo>
            <Label>{item.error} Erros</Label>
          </Column>
        </Row>
      </Content>
    </Container>
  );
};

export default Item;
