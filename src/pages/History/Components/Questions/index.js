import React, {useEffect, useState, useRef, useCallback} from 'react';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {Dimensions} from 'react-native';
import {isWithinInterval, subDays, parseISO, format} from 'date-fns';
import {Modalize} from 'react-native-modalize';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

import {
  Container,
  Content,
  FlatListStyledHorizontal,
  TitlePercent,
  WrapperHistory,
  ContentHistory,
  LabelPercent,
  TitleLabel,
  WrapperDate,
  TitleDate,
  LabelDate,
  PickerDate,
  TextPickerDate,
  ButtonSearchDate,
  ButtonClean,
  TitleButtonSearchDate,
} from './styles';

import Options from './Components/Options';
import Categories from './Components/Categories';
import {useHistoryUser} from '../../../../hooks/History';
import Loading from '../../../../components/Loading';

const {height, width} = Dimensions.get('window');

const History = () => {
  const modalizeRef = useRef(null);
  const [loading, setLoading] = useState(false);
  const {historyUser, loadingHistory} = useHistoryUser();
  const [selectedOption, setSelectedOption] = useState('Tudo');
  const [categoriesCalc, setCategoriesCalc] = useState([]);
  const [subCategoryCalc, setSubCategoriesCalc] = useState([]);
  const [startDate, setStartDate] = useState(new Date());
  const [isDatePickerVisibleStart, setIsDatePickerVisibleStart] =
    useState(false);
  const [isDatePickerVisibleEnd, setIsDatePickerVisibleEnd] = useState(false);
  const [endDate, setEndDate] = useState(new Date());
  const [calc, setCalc] = useState({
    corrects: {percent: 0, total: 0},
    erroreds: {percent: 0, total: 0},
    skipeds: {percent: 0, total: 0},
  });
  const options = ['Tudo', '7 dias', '15 dias', '30 dias', 'Outro'];

  const onOpen = () => {
    console.log('aqui');
    modalizeRef.current?.open();
  };

  const onClose = useCallback(() => {
    modalizeRef.current?.close();
  }, [modalizeRef]);

  useEffect(() => {
    setLoading(true);
    if (historyUser && !loadingHistory) {
      const questions = historyUser.filter(hist => {
        switch (selectedOption) {
          case 'Tudo':
            return hist.type === 'question';
          case '7 dias':
            return (
              hist.type === 'question' &&
              isWithinInterval(new Date(parseISO(hist.date)), {
                start: subDays(new Date(), 7),
                end: new Date(),
              })
            );
          case '15 dias':
            return (
              hist.type === 'question' &&
              isWithinInterval(new Date(parseISO(hist.date)), {
                start: subDays(new Date(), 15),
                end: new Date(),
              })
            );
          case '30 dias':
            return (
              hist.type === 'question' &&
              isWithinInterval(new Date(parseISO(hist.date)), {
                start: subDays(new Date(), 30),
                end: new Date(),
              })
            );
          case 'Outro':
            return (
              hist.type === 'question' &&
              isWithinInterval(new Date(parseISO(hist.date)), {
                start: endDate,
                end: startDate,
              })
            );
          default:
            return hist.type === 'question';
        }
      });
      // console.log(questions);
      if (questions) {
        let corrects = 0;
        let erroreds = 0;
        let skipeds = 0;

        let categories = [];
        let subCategories = [];
        questions.forEach(itm => {
          if (itm.sub_category) {
            const categoryNow = subCategories.find(
              cat => cat.category === itm.sub_category.value,
            );
            const categoryItem = {
              category: itm.sub_category.value,
              category_name: itm.sub_category.label,
            };
            if (!categoryNow) {
              subCategories.push({
                ...categoryItem,
                error: itm.errored ? 1 : 0,
                skiped: itm.skiped ? 1 : 0,
                correct: itm.correct ? 1 : 0,
              });
            } else {
              const oldCat = subCategories.filter(
                cat => cat.category !== itm.sub_category.value,
              );
              subCategories = oldCat || [];
              subCategories.push({
                ...categoryItem,
                error: itm.errored
                  ? (categoryNow.error += 1)
                  : categoryNow.error,
                skiped: itm.skiped
                  ? (categoryNow.skiped += 1)
                  : categoryNow.skiped,
                correct: itm.correct
                  ? (categoryNow.correct += 1)
                  : categoryNow.correct,
              });
            }
          }
          if (itm.category) {
            if (itm.errored) {
              erroreds += 1;
            }
            if (itm.skiped) {
              skipeds += 1;
            }
            if (itm.correct) {
              corrects += 1;
            }
            const categoryItem = {
              category: itm.category.value,
              category_name: itm.category.label,
            };
            if (itm.category) {
              const categoryNow = categories.find(
                cat => cat.category === itm.category.value,
              );
              if (!categoryNow) {
                categories.push({
                  ...categoryItem,
                  error: itm.errored ? 1 : 0,
                  skiped: itm.skiped ? 1 : 0,
                  correct: itm.correct ? 1 : 0,
                });
              } else {
                const oldCat = categories.filter(
                  cat => cat.category !== itm.category.value,
                );
                categories = oldCat || [];
                categories.push({
                  ...categoryItem,
                  error: itm.errored
                    ? (categoryNow.error += 1)
                    : categoryNow.error,
                  skiped: itm.skiped
                    ? (categoryNow.skiped += 1)
                    : categoryNow.skiped,
                  correct: itm.correct
                    ? (categoryNow.correct += 1)
                    : categoryNow.correct,
                });
              }
            }
          }
        });
        let calcs = {
          corrects: {percent: 0, total: 0},
          erroreds: {percent: 0, total: 0},
          skipeds: {percent: 0, total: 0},
        };
        if (questions && questions.length) {
          const correctsTotal = (100 * corrects) / questions.length;
          const erroredsTotal = (100 * erroreds) / questions.length;
          const skipedsTotal = (100 * skipeds) / questions.length;
          calcs = {
            corrects: {percent: correctsTotal.toFixed(0), total: corrects},
            erroreds: {percent: erroredsTotal.toFixed(0), total: erroreds},
            skipeds: {percent: skipedsTotal.toFixed(0), total: skipeds},
          };
        }
        setCalc(calcs);
        setCategoriesCalc(categories);
        setSubCategoriesCalc(subCategories);
      }
      setLoading(false);
    }
  }, [historyUser, loadingHistory, selectedOption, startDate, endDate]);

  return loading || loadingHistory ? (
    <Loading />
  ) : (
    <Container>
      <Content>
        <FlatListStyledHorizontal
          showsHorizontalScrollIndicator={false}
          data={options}
          renderItem={({item}) => (
            <Options
              key={item}
              item={item}
              setSelectedOption={setSelectedOption}
              selectedOption={selectedOption}
              onOpen={onOpen}
              onClose={onClose}
            />
          )}
          keyExtractor={item => item.id}
          horizontal
        />

        <WrapperHistory>
          <ContentHistory>
            <AnimatedCircularProgress
              size={height / 5 - 60}
              width={7}
              fill={100}
              tintColor="#A9C21B"
              // onAnimationComplete={() => console.log('onAnimationComplete')}
              backgroundColor="#333333"
              arcSweepAngle={300}
              rotation={210}>
              {fill => (
                <TitlePercent color="#A9C21B">
                  {calc.corrects.percent || 0}%
                </TitlePercent>
              )}
            </AnimatedCircularProgress>
            <TitlePercent color="#A9C21B">
              {calc.corrects.total || 0}
            </TitlePercent>
            <LabelPercent>Acertos</LabelPercent>
          </ContentHistory>
          <ContentHistory>
            <AnimatedCircularProgress
              size={height / 5 - 60}
              width={7}
              fill={100}
              tintColor="#FFC800"
              // onAnimationComplete={() => console.log('onAnimationComplete')}
              backgroundColor="#333333"
              arcSweepAngle={300}
              rotation={210}>
              {fill => (
                <TitlePercent color="#FFC800">
                  {calc.skipeds.percent || 0}%
                </TitlePercent>
              )}
            </AnimatedCircularProgress>
            <TitlePercent color="#FFC800">
              {calc.skipeds.total || 0}
            </TitlePercent>
            <LabelPercent>Pulos</LabelPercent>
          </ContentHistory>
          <ContentHistory>
            <AnimatedCircularProgress
              size={height / 5 - 60}
              width={7}
              fill={100}
              tintColor="#FF5630"
              // onAnimationComplete={() => console.log('onAnimationComplete')}
              backgroundColor="#333333"
              arcSweepAngle={300}
              rotation={210}>
              {fill => (
                <TitlePercent color="#FF5630">
                  {calc.erroreds.percent || 0}%
                </TitlePercent>
              )}
            </AnimatedCircularProgress>
            <TitlePercent color="#FF5630">
              {calc.erroreds.total || 0}
            </TitlePercent>
            <LabelPercent>Erros</LabelPercent>
          </ContentHistory>
        </WrapperHistory>
        {categoriesCalc && categoriesCalc.length ? (
          <>
            <TitleLabel>Por temas</TitleLabel>
            <FlatListStyledHorizontal
              height={130}
              width={width}
              showsHorizontalScrollIndicator={false}
              data={categoriesCalc}
              renderItem={({item}) => (
                <Categories key={item.category} item={item} />
              )}
              keyExtractor={item => item.id}
              horizontal
            />
          </>
        ) : null}
        {subCategoryCalc && subCategoryCalc.length ? (
          <>
            <TitleLabel>Por Assunto</TitleLabel>
            <FlatListStyledHorizontal
              height={130}
              width={width}
              showsHorizontalScrollIndicator={false}
              data={subCategoryCalc}
              renderItem={({item}) => (
                <Categories
                  key={item.category}
                  item={item}
                  marginBottom={true}
                />
              )}
              keyExtractor={item => item.id}
            />
          </>
        ) : null}
        <Modalize
          ref={modalizeRef}
          modalHeight={500}
          modalStyle={{
            maxHeight: 500,
            minHeight: 500,
            backgroundColor: '#1C1C1C',
            zIndex: 999999999,
          }}>
          <WrapperDate>
            <TitleDate>Outro período</TitleDate>
            <LabelDate>De</LabelDate>
            <PickerDate onPress={() => setIsDatePickerVisibleStart(true)}>
              <TextPickerDate>
                {startDate ? format(startDate, 'dd/MM/yyyy') : 'DD/MM/YYYY'}
              </TextPickerDate>
            </PickerDate>
            <LabelDate>Até</LabelDate>
            <PickerDate onPress={() => setIsDatePickerVisibleEnd(true)}>
              <TextPickerDate>
                {endDate ? format(endDate, 'dd/MM/yyyy') : 'DD/MM/YYYY'}
              </TextPickerDate>
            </PickerDate>
            <ButtonSearchDate
              onPress={() => {
                onClose();
              }}>
              <TitleButtonSearchDate>Aplicar</TitleButtonSearchDate>
            </ButtonSearchDate>
            <ButtonClean
              onPress={() => {
                setStartDate(new Date());
                setEndDate(new Date());
                onClose();
              }}>
              <TitleDate>Limpar</TitleDate>
            </ButtonClean>
          </WrapperDate>
          <DateTimePickerModal
            isVisible={isDatePickerVisibleStart}
            mode="date"
            locale="pt_BR"
            onConfirm={date => {
              setStartDate(date);
              setIsDatePickerVisibleStart(false);
            }}
            onCancel={() => setIsDatePickerVisibleStart(false)}
            maximumDate={new Date()}
          />
          <DateTimePickerModal
            isVisible={isDatePickerVisibleEnd}
            mode="date"
            locale="pt_BR"
            onConfirm={date => {
              setEndDate(date);
              setIsDatePickerVisibleEnd(false);
            }}
            onCancel={() => setIsDatePickerVisibleEnd(false)}
            maximumDate={new Date()}
          />
        </Modalize>
      </Content>
    </Container>
  );
};

export default History;
