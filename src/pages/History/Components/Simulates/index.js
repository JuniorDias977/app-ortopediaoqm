import React, {useEffect, useState, useRef, useCallback} from 'react';
import {Dimensions} from 'react-native';
import {isWithinInterval, subDays, parseISO, format, subMonths} from 'date-fns';
import {Modalize} from 'react-native-modalize';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {VictoryChart, VictoryLine, VictoryTheme} from 'victory-native';
import _ from 'lodash';

import {
  Container,
  Content,
  FlatListStyledHorizontal,
  WrapperHistory,
  TitleLabel,
  WrapperDate,
  TitleDate,
  LabelDate,
  PickerDate,
  TextPickerDate,
  ButtonSearchDate,
  ButtonClean,
  TitleButtonSearchDate,
} from './styles';

import Options from './Components/Options';
import Categories from './Components/Categories';
import {useHistoryUser} from '../../../../hooks/History';
import Loading from '../../../../components/Loading';
import Theme from './Components/Theme';

const {width} = Dimensions.get('window');

const History = () => {
  const modalizeRef = useRef(null);
  const [loading, setLoading] = useState(false);
  const {historyUser, loadingHistory} = useHistoryUser();
  const [selectedOption, setSelectedOption] = useState('Tudo');
  // const [categoriesCalc, setCategoriesCalc] = useState([]);
  const [simulatesFinded, setSimulatesFindedCalc] = useState([]);
  const [startDate, setStartDate] = useState(new Date());
  const [isDatePickerVisibleStart, setIsDatePickerVisibleStart] =
    useState(false);
  const [isDatePickerVisibleEnd, setIsDatePickerVisibleEnd] = useState(false);
  const [endDate, setEndDate] = useState(new Date());
  const options = ['Tudo', '7 dias', '15 dias', '6 meses', '1 ano'];
  const [graphCorr, setGraphCorr] = useState([{x: '', y: 0}]);
  const [graphErr, setGraphErr] = useState([{x: '', y: 0}]);

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  const onClose = useCallback(() => {
    modalizeRef.current?.close();
  }, [modalizeRef]);

  function groupBy(objectArray, property) {
    return objectArray.reduce(function (acc, obj) {
      var key = obj[property];
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(obj);
      return acc;
    }, {});
  }

  useEffect(() => {
    setLoading(true);
    if (historyUser && !loadingHistory) {
      let simulates = historyUser.filter(hist => {
        if (hist.simulate && hist.date) {
          switch (selectedOption) {
            case 'Tudo':
              return hist.type === 'simulate';
            case '7 dias':
              return (
                hist.type === 'simulate' &&
                isWithinInterval(new Date(parseISO(hist.date)), {
                  start: subDays(new Date(), 7),
                  end: new Date(),
                })
              );
            case '15 dias':
              return (
                hist.type === 'simulate' &&
                isWithinInterval(new Date(parseISO(hist.date)), {
                  start: subDays(new Date(), 30),
                  end: new Date(),
                })
              );
            case '6 meses':
              return (
                hist.type === 'simulate' &&
                isWithinInterval(new Date(parseISO(hist.date)), {
                  start: subMonths(endDate, 6),
                  end: new Date(),
                })
              );
            case '1 ano':
              return (
                hist.type === 'simulate' &&
                isWithinInterval(new Date(parseISO(hist.date)), {
                  start: subMonths(endDate, 12),
                  end: new Date(),
                })
              );
            default:
              return hist.type === 'simulate';
          }
        }
      });

      simulates = simulates.sort(function (a, b) {
        if (
          new Date(parseISO(a.date)).getTime() >
          new Date(parseISO(b.date)).getTime()
        ) {
          return 1;
        }
        if (
          new Date(parseISO(a.date)).getTime() <
          new Date(parseISO(b.date)).getTime()
        ) {
          return -1;
        }
        // a must be equal to b
        return 0;
      });
      let datesSimulates = [];
      if (simulates) {
        // let categories = [];
        let simulatesFindedNew = [];
        let alredy = [];
        simulates.forEach(itm => {
          if (
            itm.date &&
            simulates.find(
              itemD =>
                itemD.simulate.key === itm.simulate.key &&
                itemD.simulate.finished,
            )
          ) {
            if (itm.simulate && itm.date) {
              simulates.forEach(sim => {
                if (
                  sim.simulate.finished &&
                  sim.date &&
                  !alredy.find(k => k === sim.simulate.key)
                ) {
                  alredy.push(sim.simulate.key);
                  datesSimulates.push(parseISO(sim.date));
                }
              });
              const categoryNow = simulatesFindedNew.find(
                cat =>
                  cat.category === itm.simulate.key &&
                  itm.simulate.date === cat.date,
              );
              const categoryItem = {
                category: itm.simulate.key,
                categoryData: itm.category,
                sub_category: itm.sub_category,
                date: itm.date,
                time: itm.simulate.time,
                simulateId: itm.simulate.simulate,
                simulateUserId: itm.simulate.key,
                question: itm.question,
                selected: itm.selected,
                error: itm.errored,
                correct: itm.correct,
              };
              if (!categoryNow) {
                simulatesFindedNew.push({
                  ...categoryItem,
                  error: itm.errored ? 1 : 0,
                  skiped: itm.skiped ? 1 : 0,
                  correct: itm.correct ? 1 : 0,
                });
              } else {
                const oldCat = simulatesFindedNew.filter(
                  cat => cat.category !== itm.simulate.key,
                );
                simulatesFindedNew = oldCat || [];
                simulatesFindedNew.push({
                  ...categoryItem,
                  error: itm.errored
                    ? (categoryNow.error += 1)
                    : categoryNow.error,
                  skiped: itm.skiped
                    ? (categoryNow.skiped += 1)
                    : categoryNow.skiped,
                  correct: itm.correct
                    ? (categoryNow.correct += 1)
                    : categoryNow.correct,
                });
              }
            }
          }
        });
        const oldSim = simulatesFindedNew;
        simulatesFindedNew = groupBy(simulatesFindedNew, 'simulateUserId');
        let newArr = [];
        Object.keys(simulatesFindedNew).forEach(dt => {
          newArr.push(simulatesFindedNew[dt]);
        });
        simulatesFindedNew = newArr;
        // setCategoriesCalc(categories);
        let dataGraphCorrect = [];
        let dataGraphErrored = [];
        const monthNames = [
          'jan',
          'fev',
          'mar',
          'abr',
          'mai',
          'jun',
          'jul',
          'ago',
          'set',
          'out',
          'nov',
          'dez',
        ];
        datesSimulates.forEach((element, index) => {
          const cor = oldSim.reduce((acc, val) => acc + val.correct, 0);
          const er = oldSim.reduce((acc, val) => acc + val.error, 0);
          if (selectedOption !== '7 dias' && selectedOption !== '15 dias') {
            const objCorrect = {
              x: monthNames[element.getMonth()],
              y: cor,
            };
            const objErrored = {
              x: monthNames[element.getMonth()],
              y: er,
            };
            dataGraphCorrect.push(objCorrect);
            dataGraphErrored.push(objErrored);
          } else {
            const objCorrect = {
              x: `${format(element, 'dd')}`,
              y: cor,
            };
            const objErrored = {
              x: `${format(element, 'dd')}`,
              y: er,
            };
            dataGraphCorrect.push(objCorrect);
            dataGraphErrored.push(objErrored);
          }
        });
        setGraphCorr(dataGraphCorrect);
        setGraphErr(dataGraphErrored);
        console.log(dataGraphCorrect, dataGraphErrored);
        setSimulatesFindedCalc(simulatesFindedNew);
      }
      setLoading(false);
    }
  }, [historyUser, loadingHistory, selectedOption, startDate, endDate]);

  return loading || loadingHistory ? (
    <Loading />
  ) : (
    <Container>
      <Content>
        <FlatListStyledHorizontal
          showsHorizontalScrollIndicator={false}
          data={options}
          renderItem={({item}) => (
            <Options
              key={item}
              item={item}
              setSelectedOption={setSelectedOption}
              selectedOption={selectedOption}
              onOpen={onOpen}
              onClose={onClose}
            />
          )}
          keyExtractor={item => item.id}
          horizontal
        />

        <WrapperHistory>
          <VictoryChart
            width={width - 24}
            height={280}
            theme={Theme}
            style={{
              color: 'white',
            }}>
            <VictoryLine
              style={{
                data: {stroke: '#A9C21B'},
                labels: {
                  fontSize: 15,
                  fill: '#fff',
                  color: '#fff',
                },
              }}
              interpolation="natural"
              data={graphCorr}
            />
            <VictoryLine
              interpolation="natural"
              style={{
                data: {stroke: '#FF5630'},
              }}
              data={graphErr}
            />
          </VictoryChart>
        </WrapperHistory>
        {/* <TitleLabel>Por temas</TitleLabel> */}
        {/* <FlatListStyledHorizontal
          height={130}
          width={width}
          showsHorizontalScrollIndicator={false}
          data={categoriesCalc}
          renderItem={({item}) => (
            <Categories key={item.category} item={item} />
          )}
          keyExtractor={item => item.id}
          horizontal
        /> */}
        {simulatesFinded && simulatesFinded.length ? (
          <>
            <TitleLabel>Simulados finalizados</TitleLabel>
            <FlatListStyledHorizontal
              width={width}
              showsHorizontalScrollIndicator={false}
              data={simulatesFinded}
              renderItem={({item, index}) => (
                <Categories item={item} index={index} marginBottom={true} />
              )}
              keyExtractor={item => item.id}
            />
          </>
        ) : null}
        <Modalize
          ref={modalizeRef}
          modalHeight={500}
          modalStyle={{
            maxHeight: 500,
            minHeight: 500,
            backgroundColor: '#1C1C1C',
            zIndex: 999999999,
          }}>
          <WrapperDate>
            <TitleDate>Outro período</TitleDate>
            <LabelDate>De</LabelDate>
            <PickerDate onPress={() => setIsDatePickerVisibleStart(true)}>
              <TextPickerDate>
                {startDate ? format(startDate, 'dd/MM/yyyy') : 'DD/MM/YYYY'}
              </TextPickerDate>
            </PickerDate>
            <LabelDate>Até</LabelDate>
            <PickerDate onPress={() => setIsDatePickerVisibleEnd(true)}>
              <TextPickerDate>
                {endDate ? format(endDate, 'dd/MM/yyyy') : 'DD/MM/YYYY'}
              </TextPickerDate>
            </PickerDate>
            <ButtonSearchDate
              onPress={() => {
                onClose();
              }}>
              <TitleButtonSearchDate>Aplicar</TitleButtonSearchDate>
            </ButtonSearchDate>
            <ButtonClean
              onPress={() => {
                setStartDate(new Date());
                setEndDate(new Date());
                onClose();
              }}>
              <TitleDate>Limpar</TitleDate>
            </ButtonClean>
          </WrapperDate>
          <DateTimePickerModal
            isVisible={isDatePickerVisibleStart}
            mode="date"
            locale="pt_BR"
            onConfirm={date => {
              setStartDate(date);
              setIsDatePickerVisibleStart(false);
            }}
            onCancel={() => setIsDatePickerVisibleStart(false)}
            maximumDate={new Date()}
          />
          <DateTimePickerModal
            isVisible={isDatePickerVisibleEnd}
            mode="date"
            locale="pt_BR"
            onConfirm={date => {
              setEndDate(date);
              setIsDatePickerVisibleEnd(false);
            }}
            onCancel={() => setIsDatePickerVisibleEnd(false)}
            maximumDate={new Date()}
          />
        </Modalize>
      </Content>
    </Container>
  );
};

export default History;
