import styled from 'styled-components/native';

import {colors, fonts} from '../../../../global';

export const Container = styled.View`
  flex: 1;
  background-color: ${colors.background_main};
  position: relative;
  padding: 24px 0 0;
  z-index: 999;
`;

export const Content = styled.View`
  z-index: 9999;
  padding: 0 24px;
`;

export const FlatListStyledHorizontal = styled.FlatList`
  flex: 1;
  margin: 0 0 0 0;
  width: ${({width}) => (width ? `${width}px` : '100%')};
`;

export const TitlePercent = styled.Text`
  font-size: ${fonts.regular};
  color: ${({color}) => (color ? color : colors.color_percent_primary)};
  font-family: ${fonts.fontFamilyBold};
  text-align: center;
  width: 100%;
`;

export const LabelPercent = styled.Text`
  font-size: 10px;
  color: rgba(241, 241, 241, 0.6);
  font-family: ${fonts.fontFamilyBold};
  text-align: center;
  width: 100%;
`;

export const WrapperHistory = styled.View`
  width: 100%;
`;

export const ContentHistory = styled.View`
  border-width: 1px;
  border-color: #333333;
  padding: 10px;
  flex-basis: 32%;
  justify-content: center;
  align-items: center;
  border-radius: 8px;
`;

export const TitleLabel = styled.Text`
  font-size: ${fonts.regular};
  color: $rgba(241, 241, 241, 0.6);
  font-family: ${fonts.fontFamilyLight};
  text-align: left;
  width: 100%;
  margin-top: 24px;
  margin-bottom: 12px;
`;

export const WrapperDate = styled.View`
  width: 100%;
  background-color: ${colors.background_main};
  z-index: 9999999;
  padding: 0 24px;
`;

export const TitleDate = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  width: 100%;
  margin-top: 12px;
  margin-bottom: 12px;
  text-align: center;
`;

export const LabelDate = styled.Text`
  font-size: ${fonts.small};
  color: $rgba(241, 241, 241, 0.6);
  font-family: ${fonts.fontFamilyLight};
  text-align: left;
  width: 100%;
  margin-top: 12px;
  margin-bottom: 8px;
`;

export const PickerDate = styled.TouchableOpacity`
  width: 100%;
  background-color: #262626;
  padding: 14px 16px;
  border-radius: 8px;
`;

export const TextPickerDate = styled.Text`
  font-size: ${fonts.regular};
  color: $rgba(241, 241, 241, 0.6);
  font-family: ${fonts.fontFamilyLight};
  text-align: left;
  width: 100%;
`;

export const ButtonSearchDate = styled.TouchableOpacity`
  width: 100%;
  padding: 14px 16px;
  background-color: ${colors.orange};
  border-radius: 24px;
  margin-top: 24px;
`;

export const ButtonClean = styled.TouchableOpacity``;

export const TitleButtonSearchDate = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  text-align: center;
  width: 100%;
`;
