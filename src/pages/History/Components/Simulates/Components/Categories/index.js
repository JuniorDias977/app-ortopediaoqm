import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {format, parseISO} from 'date-fns';

import {useSimulate} from '../../../../../../hooks/Simulate';
import {
  Container,
  TitleLabel,
  ContainerHeader,
  Column,
  Label,
  Content,
  Row,
  TitleLabelTwo,
  LabelDate,
  LabelAproveReprove,
} from './styles';

export const APPROVAL_RANGE = 50;

const Item = ({item, marginBottom = false, index}) => {
  const navigation = useNavigation();
  const {simulates} = useSimulate();
  const [time, setTime] = useState('00h00m');
  const [simulate, setSimulate] = useState();
  const [correct, setCorrect] = useState(0);
  const [errored, setErrored] = useState(0);

  const averageScore = ((100 * correct) / (correct + errored)).toFixed(0);

  function pad(num) {
    return ('0' + num).slice(-2);
  }

  function hhmmss(secs) {
    var minutes = Math.floor(secs / 60);
    secs = secs % 60;
    var hours = Math.floor(minutes / 60);
    minutes = minutes % 60;
    return `${pad(hours)}h${pad(minutes)}m`;
    // return pad(hours)+":"+pad(minutes)+":"+pad(secs); for old browsers
  }

  useEffect(() => {
    if (item && item.length) {
      let cr = 0;
      let er = 0;
      item.forEach(itm => {
        if (itm.correct) {
          cr += 1;
        } else {
          er += 1;
        }
      });
      setCorrect(cr);
      setErrored(er);
    }
  }, [item]);

  useEffect(() => {
    if (simulates && simulates.find(sim => sim.key === item[0].simulateId)) {
      setSimulate(simulates.find(sim => sim.key === item[0].simulateId));
    }
  }, [simulates, item]);

  useEffect(() => {
    if (simulate) {
      const durationTime = simulate.duration.split(':');
      setTime(
        hhmmss(
          durationTime[0] * 60 * 60 +
            durationTime[1] * 60 -
            (item[index] && item[index].time ? item[index].time : 0),
        ),
      );
    } else {
      setTime('03h00');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [simulate, item]);

  return (
    <Container
      marginBottom={marginBottom}
      onPress={() => navigation.navigate('HistoryItem', item)}>
      <ContainerHeader>
        <TitleLabel>
          {simulate && simulate.name ? simulate.name : 'Simulado especial'}
        </TitleLabel>
      </ContainerHeader>
      <Content>
        <Row>
          <Column basis={70}>
            <Row>
              <Column basis={40}>
                <LabelDate>
                  {item[item.length - 1].date &&
                  !item[item.length - 1].date.nanoseconds
                    ? format(parseISO(item[item.length - 1].date), 'dd/MM/yyyy')
                    : null}
                </LabelDate>
                <TitleLabel>{item.length}</TitleLabel>
                <Label>{item.length > 1 ? 'Questões' : 'Questão'}</Label>
              </Column>
              <Column basis={20}>
                <TitleLabelTwo color="#A9C21B">
                  {((100 * correct) / (correct + errored)).toFixed(0)}%
                </TitleLabelTwo>
                <Label>
                  {correct} {correct > 1 ? 'Acertos' : 'Acerto'}
                </Label>
              </Column>
              <Column basis={20}>
                <TitleLabelTwo color="#FF5630">
                  {((100 * errored) / (correct + errored)).toFixed(0)}%
                </TitleLabelTwo>
                <Label>
                  {errored} {errored > 1 ? 'Erros' : 'Erro'}
                </Label>
              </Column>
            </Row>
          </Column>

          <Column basis={30} style={{paddingRight: 12}}>
            <Row>
              <Column basis={94}>
                <LabelDate>{time}</LabelDate>
                <LabelAproveReprove averageScore={averageScore}>
                  {averageScore <= APPROVAL_RANGE ? 'Aprovado' : 'Reprovado'}
                </LabelAproveReprove>
              </Column>
            </Row>
          </Column>
        </Row>
      </Content>
    </Container>
  );
};

export default Item;
