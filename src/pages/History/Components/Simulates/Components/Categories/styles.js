import styled from 'styled-components/native';
import {Dimensions} from 'react-native';

import {colors, fonts} from '../../../../../../global';
import {APPROVAL_RANGE} from './index';

const {width} = Dimensions.get('window');

export const Container = styled.TouchableOpacity`
  border-width: 1px;
  border-color: #333333;
  border-radius: 12px;
  margin-right: ${({marginBottom}) => (marginBottom ? 0 : 12)}px;
  margin-bottom: ${({marginBottom}) => (marginBottom ? 12 : 0)}px;
  width: ${width - 48}px;
`;

export const Column = styled.View`
  flex-direction: column;
  flex-basis: ${({basis}) => (basis ? basis : 30)}%;
  margin-right: 12px;
`;

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ContainerHeader = styled.View`
  background-color: #333;
  border-top-left-radius: 12px;
  border-top-right-radius: 12px;
  padding: 16px 20px;
`;

export const Content = styled.View`
  border-bottom-left-radius: 12px;
  border-bottom-right-radius: 12px;
  padding: 16px 20px;
`;

export const TitleLabel = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  width: 100%;
`;

export const TitleLabelTwo = styled.Text`
  font-size: ${fonts.small};
  color: ${({color}) => (color ? color : colors.white)};
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  width: 100%;
`;

export const Label = styled.Text`
  font-size: 9px;
  color: rgba(241, 241, 241, 0.6);
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  width: 100%;
`;

export const LabelDate = styled.Text`
  font-size: 12px;
  color: rgba(241, 241, 241, 0.6);
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  width: 100%;
`;

export const LabelAproveReprove = styled.Text`
  font-size: 10px;
  color: #262626;
  font-family: ${fonts.fontFamilyRegular};
  text-align: center;
  background-color: ${({averageScore}) =>
    averageScore <= APPROVAL_RANGE ? '#a9c21b' : colors.error};
  padding: 2px;
  border-radius: 8px;
`;
