import React from 'react';

import {Container, Title} from './styles';

const Item = ({item, setSelectedOption, selectedOption, onOpen, onClose}) => {
  return (
    <Container
      onPress={() => {
        setSelectedOption(item);
        if (item === 'Outro') {
          onOpen();
        } else {
          onClose();
        }
      }}
      checked={selectedOption && selectedOption === item}>
      <Title checked={selectedOption && selectedOption === item}>{item}</Title>
    </Container>
  );
};

export default Item;
