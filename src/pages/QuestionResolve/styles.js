import styled from 'styled-components/native';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {Dimensions} from 'react-native';
import {Image} from 'react-native';

import {colors, fonts} from '../../global';

const {width} = Dimensions.get('window');

export const ButtonLogo = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
`;

export const Logo = styled(Image).attrs({
  source: require('../../assets/log-one.png'),
  resizeMode: 'contain',
})`
  width: 40px;
  height: 40px;
`;
export const Adjust = styled(Image).attrs({
  source: require('../../assets/adjust.png'),
  resizeMode: 'contain',
})`
  width: 24px;
  height: 24px;
`;

export const ButtonAdjust = styled.TouchableOpacity``;

export const Container = styled.View`
  flex: 1;
  background-color: ${colors.background_main};
  position: relative;
`;

export const Content = styled.View`
  flex: 1;
  width: ${width}px;
`;

export const ContentNoBody = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  width: ${width}px;
  padding: 24px;
`;

export const ContainerHeader = styled.View`
  width: 100%;
  background-color: ${colors.background_main};
  justify-content: center;
  padding: 24px 20px 0;
  justify-content: space-between;
  flex-direction: row;
  z-index: 3;
  position: relative;
  align-items: center;
`;

export const ScrollViewStyled = styled.ScrollView`
  flex: 1;
  position: absolute;
  margin: auto;
  height: 100%;
  z-index: 2;
`;

export const Icon = styled(SimpleLineIcon)``;

export const IconFont = styled(FontAwesome)``;

export const HeaderName = styled.Text`
  font-size: ${fonts.bigger};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyBold};
  text-align: center;
`;

export const FlatListStyled = styled.FlatList`
  width: 100%;
`;

export const TextNoBody = styled.Text`
  font-size: ${fonts.bigger};
  color: rgba(241, 241, 241, 0.4);
  font-family: ${fonts.fontFamilyBold};
  text-align: center;
  width: 100%;
`;
