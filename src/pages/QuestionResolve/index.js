import React, {useState, useEffect, useRef, useCallback} from 'react';
import {Modalize} from 'react-native-modalize';
import {useNavigation} from '@react-navigation/native';

import Loading from '../../components/Loading';
import {useQuestions} from '../../hooks/Questions';

import Item from './Components/Item';
import ContentModal from './Components/ContentModal';
import ContentModalCategory from './Components/CategoryFilter';
import Congratulations from './Components/Congratulations';

import reactotron from 'reactotron-react-native';

import {colors} from '../../global';

import {
  Container,
  Content,
  ScrollViewStyled,
  ContainerHeader,
  Logo,
  FlatListStyled,
  HeaderName,
  Adjust,
  ButtonAdjust,
  ContentNoBody,
  TextNoBody,
  ButtonLogo,
} from './styles';

const Profile = ({route}) => {
  const {questions} = useQuestions();
  const navigation = useNavigation();

  const [loading, setLoading] = useState(false);
  const [loadingModal, setLoadingModal] = useState(false);

  const [questionsData, setQuestionsData] = useState([]);
  const [newQuestionData, setNewQuestion] = useState([]);

  const [hidden, setHidden] = useState(false);
  const ref = useRef(null);
  const modalizeRef = useRef(null);
  const modalizeCategoryRef = useRef(null);
  const [full, setFull] = useState(false);

  const [subCategory, setSubCategory] = useState();

  useEffect(() => {
    if (questions) {
      setLoading(true);
      let arrData = [];
      questions.forEach(qt => {
        if (qt.sub_group) {
          qt.sub_group.forEach(sb => {
            if (route.params.item) {
              route.params.item.map(itm => {
                if (itm.key === sb.value) {
                  if (!arrData.find(ar => ar.key === qt.key)) {
                    arrData.push(qt);
                  }
                }
              });
            }
          });
        }
      });

      arrData.sort(() => Math.random() - 0.5);

      setQuestionsData(arrData);
      setLoading(false);
    }
  }, [questions, route.params.item]);

  useEffect(() => {
    if (route.params.item.length > 0) {
      const sub_category = route.params.item[0].sub_category.label;

      setSubCategory(sub_category);
    }
  }, [route]);

  useEffect(() => {
    if (route.params.newQuestion?.length > 0) {
      const {answered_question_user} = route.params.newQuestion[0];

      let results = questionsData.filter(arrayQuestion => {
        return !answered_question_user.some(questionUser => {
          return arrayQuestion.key === questionUser.key;
        });
      });

      results.sort(() => Math.random() - 0.5);

      setNewQuestion(results);
    }
  }, [route, questionsData]);

  const handleNext = useCallback(
    indx => {
      ref.current.scrollToIndex({
        animated: true,
        index: indx,
        viewPosition: indx,
      });
    },
    [ref],
  );

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  const onClose = useCallback(() => {
    modalizeRef.current?.close();
  }, [modalizeRef]);

  const onOpenCategory = () => {
    modalizeCategoryRef.current?.open();
  };

  const onCloseCategory = useCallback(() => {
    modalizeCategoryRef.current?.close();
    setFull(false);
  }, [modalizeCategoryRef]);

  const fullModalCategory = useCallback(() => {
    setFull(true);
  }, []);

  return loading || loadingModal ? (
    <Loading />
  ) : (
    <Container>
      <ScrollViewStyled
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={false}>
        {hidden ? null : (
          <ContainerHeader>
            <ButtonLogo
              onPress={() => {
                try {
                  navigation.navigate('Category', {item: {goback: true}});
                } catch (er) {
                  console.log(er);
                }
              }}>
              <Logo />
            </ButtonLogo>

            <HeaderName>Ortopedia OQM</HeaderName>

            <ButtonAdjust onPress={onOpen}>
              <Adjust />
            </ButtonAdjust>
          </ContainerHeader>
        )}

        <Content>
          {questionsData && questionsData.length ? (
            <FlatListStyled
              ref={ref}
              horizontal
              scrollEnabled={false}
              showsHorizontalScrollIndicator={false}
              data={
                route.params.newQuestion?.length > 0
                  ? newQuestionData
                  : questionsData || []
              }
              ListEmptyComponent={() => (
                <Congratulations sub_category={subCategory} />
              )}
              renderItem={({item, index}) => (
                <Item
                  item={item}
                  index={index}
                  onButtonPress={handleNext}
                  lastIndex={
                    route.params.newQuestion
                      ? newQuestionData.length - 1
                      : questionsData.length - 1
                  }
                  setHidden={setHidden}
                />
              )}
              keyExtractor={item => item.key}
            />
          ) : (
            <ContentNoBody>
              <TextNoBody>Ainda estamos preparando este conteúdo.</TextNoBody>
            </ContentNoBody>
          )}
        </Content>

        <Modalize
          ref={modalizeRef}
          withHandle={false}
          modalStyle={{
            minHeight: '90%',
            backgroundColor: colors.background_main,
          }}>
          <ContentModal
            handleCloseModal={onClose}
            handleOpenCategory={onOpenCategory}
          />
        </Modalize>

        <Modalize
          ref={modalizeCategoryRef}
          withHandle={false}
          scrollViewProps={{contentContainerStyle: {minHeight: '70%'}}}
          modalStyle={{
            minHeight: full ? '100%' : '90%',
            backgroundColor: colors.background_main,
          }}>
          <ContentModalCategory
            handleCloseModal={onCloseCategory}
            fullModalCategory={fullModalCategory}
          />
        </Modalize>
      </ScrollViewStyled>
    </Container>
  );
};

export default Profile;
