import styled from 'styled-components/native';
import {Picker} from '@react-native-community/picker';
import FeatherIcon from 'react-native-vector-icons/SimpleLineIcons';

import {colors, fonts} from '../../../../global';

export const Container = styled.View`
  flex: 1;
  background-color: ${colors.background_main};
  position: relative;
  padding: 24px 24px 0;
  height: 100%;
`;

export const Content = styled.View`
  flex: 1;
  padding-bottom: 12px;
  height: 100%;
`;

export const ContentCount = styled.View`
  flex: 1;
  padding-bottom: 12px;
  height: 100%;
  justify-content: center;
  align-items: center;
`;

export const TextError = styled.Text`
  font-size: ${fonts.small};
  color: ${colors.error};
  font-family: ${fonts.fontFamilyRegular};
`;

export const ForgotPassword = styled.TouchableOpacity`
  margin-top: 12px;
`;

export const ForgotPasswordText = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white_correct};
  font-family: ${fonts.fontFamilyRegular};
  text-align: center;
  margin-top: 5px;
`;

export const CreateAccount = styled.TouchableOpacity`
  margin-top: 50px;
  margin-bottom: 50px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const CreateAccountText = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.grey};
  font-family: ${fonts.fontFamilyRegular};
  margin-left: 16px;
`;

export const RowTwo = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: 24px;
`;

export const RowThree = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  border-bottom-width: 1px;
  border-bottom-color: #333333;
  margin-left: 12px;
`;

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ButtonBack = styled.TouchableOpacity`
  z-index: 9999999999999;
`;

export const HeaderTitle = styled.Text`
  font-size: ${fonts.regular};
  color: rgba(241, 241, 241, 0.4);
  font-family: ${fonts.fontFamilyRegular};
  margin: 24px 0 0 5px;
  z-index: 9999999;
`;

export const HeaderTitleTwo = styled.Text`
  font-size: ${fonts.regular};
  color: rgba(241, 241, 241, 0.4);
  font-family: ${fonts.fontFamilyRegular};
  margin: 5px 0 0 5px;
`;
export const HeaderLabel = styled.Text`
  font-size: ${fonts.small};
  color: ${colors.color_botton_inactive};
  font-family: ${fonts.fontFamilyRegular};
  margin-top: 5px;
  margin-left: 16px;
`;

export const TitlePage = styled.Text`
  font-size: ${fonts.bigger};
  color: rgba(241, 241, 241, 0.4);
  font-family: ${fonts.fontFamilyRegular};
  margin: 36px 0 12px 0;
  margin-left: 16px;
`;

export const TextTerms = styled.Text`
  font-size: ${fonts.regular};
  color: rgba(241, 241, 241, 0.4);
  font-family: ${fonts.fontFamilyRegular};
  margin-top: auto;
  text-align: center;
  margin-left: 5px;
`;
export const ButtonSignIn = styled.TouchableOpacity`
  margin-bottom: 12px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: 12px;
  margin-top: auto;
  border-radius: 24px;
  background-color: ${({background}) =>
    background ? background : colors.orange};
  width: 100%;
`;

export const TextButtonSignIn = styled.Text`
  font-size: ${fonts.regular};
  color: ${({color}) => (color ? color : colors.color_text)};
  font-family: ${fonts.fontFamilyRegular};
`;

export const PickerContainer = styled.View`
  width: 100%;
  border-bottom-width: 2px;
  border-bottom-color: ${({borderBottom}) =>
    borderBottom ? borderBottom : colors.color_text};
  padding: 5px 16px 0 16px;
  margin-bottom: 8px;
  flex-direction: row;
  align-items: center;
`;

export const PickerStyled = styled(Picker)`
  border-width: 2px;
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.regular};
  padding: 0 4px;
  color: ${colors.color_input_text};
  margin: 0;
  width: 89%;
  text-align: center;
  ::placeholder {
    width: 100%;
    text-align: center;
    color: ${({color}) => (color ? color : colors.color_text)};
  }
`;

export const IconStyled = styled(FeatherIcon)`
  margin: 5px 0 0 5px;
`;

export const FlatListStyled = styled.FlatList`
  min-height: ${({itemData}) => (itemData ? 43 : 34)}%;
`;

export const FlatListStyledHorizontal = styled.FlatList`
  height: 45px;
  margin: 24px 0 0 0;
  width: 100%;
`;

export const Logo = styled.Image.attrs({
  source: require('../../../../assets/log-one.png'),
  resizeMode: 'contain',
})`
  width: 100px;
  height: 100px;
  margin-top: 12px;
`;

export const Vector = styled.Image.attrs({
  source: require('../../../../assets/vector.png'),
  resizeMode: 'cover',
})`
  margin-top: 10%;
  width: 212.19px;
  height: 232.66px;
  position: relative;
`;

export const LogoIcon = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: 40px;
  height: 40px;
`;

export const Title = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyBold};
  text-align: left;
  margin-left: 16px;
  margin-top: 5px;
`;

export const ExportAll = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyBold};
`;

export const Count = styled.Text`
  font-size: 75px;
  color: rgba(241, 241, 241, 0.6);
  font-family: ${fonts.fontFamilyBold};
  position: absolute;
  top: 29%;
`;
