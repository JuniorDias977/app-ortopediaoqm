import React from 'react';

import {Container, Title, SubTitle} from './styles';

const Item = ({item}) => {
  return (
    <Container>
      <Title>{item.name}</Title>
      <SubTitle>12 questões</SubTitle>
    </Container>
  );
};

export default Item;
