import styled from 'styled-components';

import {colors, fonts} from '../../../../../../global';

export const Container = styled.View`
  margin-top: 12px;
  align-items: center;
  justify-content: flex-start;
  padding-left: 13px;
`;

export const Title = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyBold};
  text-align: left;
  width: 100%;
`;

export const SubTitle = styled.Text`
  width: 100%;
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyLight};
  text-align: left;
`;
