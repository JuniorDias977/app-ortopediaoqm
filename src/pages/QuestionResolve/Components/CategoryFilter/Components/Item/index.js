import React from 'react';
import {Checkbox} from 'react-native-paper';
import {colors} from '../../../../../../global';

import {Container, Title} from './styles';

const Item = ({
  item,
  selectSubGroup,
  setSelectSubGroup,
  setChecked,
  checked,
}) => {
  return (
    <Container>
      <Title>{item.name}</Title>
      <Checkbox
        color={colors.orange}
        uncheckedColor={colors.white}
        status={
          checked || selectSubGroup.find(sb => sb.key === item.key)
            ? 'checked'
            : 'unchecked'
        }
        onPress={() => {
          if (
            selectSubGroup &&
            selectSubGroup.find(sb => sb.key === item.key)
          ) {
            setSelectSubGroup(selectSubGroup.filter(sb => sb.key !== item.key));
            setChecked(false);
          } else {
            setChecked(false);
            setSelectSubGroup(stt => [...stt, item]);
          }
        }}
      />
    </Container>
  );
};

export default Item;
