import styled from 'styled-components';

import {colors, fonts} from '../../../../../../global';

export const Container = styled.View`
  margin-top: 12px;
  align-items: center;
  justify-content: flex-start;
  flex-direction: row;
  justify-content: space-between;
  padding-left: 13px;
`;

export const Title = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyBold};
  text-align: left;
`;
