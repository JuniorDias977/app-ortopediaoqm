import React, {useState, useEffect, useCallback, useRef} from 'react';
import {
  Alert,
  KeyboardAvoidingView,
  Platform,
  Animated,
  Easing,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {Checkbox} from 'react-native-paper';
import moment from 'moment';

import Loading from '../../../../components/Loading';
import InputFilter from '../../../../components/InputFilter';
import Button from '../../../../components/Button';
import {useSubCategories} from '../../../../hooks/SubCategories';
import {useSubGroups} from '../../../../hooks/SubGroups';

import {
  Container,
  Content,
  Row,
  FlatListStyled,
  ButtonBack,
  FlatListStyledHorizontal,
  RowThree,
  ExportAll,
  IconStyled,
  HeaderTitleTwo,
  ContentCount,
  Count,
  Vector,
} from './styles';
import Item from './Components/Item';
import ItemByText from './Components/ItemByText';
import SubCategory from './Components/SubCategory';

import {colors} from '../../../../global';

const Category = ({handleCloseModal, fullModalCategory}) => {
  const navigation = useNavigation();
  const {subCategories} = useSubCategories();
  const {subGroups} = useSubGroups();
  const [loading, setLoading] = useState(false);
  const [category, setCategory] = useState(false);
  const [selectSubCategory, setSelectSubCategory] = useState();
  const [subsCategories, setSubsCategories] = useState([]);
  const [subsGroups, setSubsGroups] = useState([]);
  const [checked, setChecked] = useState(true);
  const [selectSubGroup, setSelectSubGroup] = useState([]);
  const [itemData, setItemData] = useState(true);
  const [text, setText] = useState('');
  const [isFocusedData, setIsFocusedData] = useState(false);
  const [hasFocusedData, setHasFocusedData] = useState(false);
  const [subsGroupsFiltered, setSubsGroupsFiltered] = useState([]);
  const [loadingSubs, setLoadingSubs] = useState(false);
  const [count, setCount] = useState('5');
  const [isCount, setIsCount] = useState(false);
  const [nextPage, setNextPage] = useState(false);
  const [timer, setTimer] = useState();
  const [time, setTime] = useState(0);
  const spinAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 5000);
    Animated.timing(spinAnim, {
      toValue: 1,
      duration: 7000,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start();
  }, [spinAnim]);

  const spin = spinAnim.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg'],
  });

  useEffect(() => {
    async function getCategory() {
      const categoryStorage = await AsyncStorage.getItem('@categoryOrtopedia:');
      if (categoryStorage) {
        setCategory(JSON.parse(categoryStorage));
      }
    }
    getCategory();
  }, [itemData]);

  useEffect(() => {
    setLoading(true);
    if (subCategories && subCategories.length) {
      const newSubs =
        subCategories.filter(sub => {
          return sub.category.value === category.key && sub.active;
        }) || [];
      setSubsCategories(newSubs);
      setSelectSubCategory(newSubs[0]);
    }
    setLoading(false);
  }, [category, subCategories]);

  useEffect(() => {
    if (selectSubCategory && subGroups && subGroups.length) {
      const newSubs =
        subGroups.filter(sub => {
          return sub.sub_category.value === selectSubCategory.key && sub.active;
        }) || [];
      setSubsGroups(newSubs);
    } else {
      setSubsGroups([]);
    }
  }, [selectSubCategory, subGroups]);

  useEffect(() => {
    if (subsGroups && subsGroups.length) {
      let newSubs = [];
      setLoadingSubs(true);
      setTimeout(() => {
        subsGroups.forEach(element => {
          if (text) {
            if (element.name.toUpperCase().includes(text.toUpperCase())) {
              newSubs.push(element);
            }
          }
        });
        setSubsGroupsFiltered(newSubs);
        setLoadingSubs(false);
      }, 3000);
    }
  }, [subsGroups, text]);

  function secondsToHours(numberOfSeconds) {
    const duration = moment.duration(numberOfSeconds, 'seconds');
    const ss = duration.seconds();

    if (numberOfSeconds < 6) {
      if (ss === 1) {
        return '4';
      }
      if (ss === 2) {
        return '3';
      }
      if (ss === 3) {
        return '2';
      }
      if (ss === 4) {
        return '1';
      }
      if (ss === 5) {
        return '0';
      }
    }
    setIsCount(false);
    setNextPage(true);
    return true;
  }

  function diffMinutes(dt2, dt1) {
    const a = moment(dt1);
    const b = moment(dt2);
    return a.diff(b, 'seconds');
  }

  const handlePage = useCallback(() => {
    if (checked) {
      handleCloseModal();
      setIsCount(false);
      navigation.navigate('QuestionResolve', {item: subsGroups});
      return;
    }
    if (!selectSubGroup) {
      Alert.alert('ATENÇÃO', 'Selecione um sub grupo.');
      return;
    }
    handleCloseModal();
    setIsCount(false);
    navigation.navigate('QuestionResolve', {item: selectSubGroup});
  }, [selectSubGroup, subsGroups, navigation, checked, handleCloseModal]);

  useEffect(() => {
    if (isCount) {
      const interval = setInterval(() => {
        if (timer) {
          setTime(
            secondsToHours(
              parseInt(diffMinutes(new Date(timer), new Date()), 10),
            ),
          );
        }
      }, 1000);
      return () => clearInterval(interval);
    } else {
      if (nextPage) handlePage();
    }
  }, [isCount, count, timer, handlePage, nextPage]);

  const handleQuestions = useCallback(() => {
    setTimer(new Date());
    fullModalCategory();
    setIsCount(true);
  }, [fullModalCategory]);

  return loading ? (
    <Loading />
  ) : (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <Content>
          {isCount ? (
            <ContentCount>
              <Vector />
              <Count>0{time ? time : '5'}</Count>
              <Animated.Image
                source={require('../../../../assets/log-one.png')}
                style={{
                  marginTop: 12,
                  width: 100,
                  height: 100,
                  transform: [{rotate: spin}],
                }}
              />
            </ContentCount>
          ) : (
            <>
              <Row>
                {text || hasFocusedData ? (
                  <ButtonBack
                    onPress={() => {
                      setText('');
                      setIsFocusedData(true);
                      setHasFocusedData(false);
                    }}>
                    <Icon
                      name="arrow-left"
                      color={colors.white_correct}
                      size={22}
                    />
                  </ButtonBack>
                ) : null}
                {itemData ? (
                  <InputFilter
                    value={text}
                    placeholder="Selecione ou pesquise por assunto"
                    icon="magnifier"
                    onChangeText={e => setText(e)}
                    setText={setText}
                    handleInputBlurData={isFocusedData}
                    setIsFocusedData={setIsFocusedData}
                    setHasFocusedData={setHasFocusedData}
                  />
                ) : null}
              </Row>
              {subsCategories &&
              subsCategories.length &&
              (!text || !hasFocusedData) ? (
                <FlatListStyledHorizontal
                  data={
                    subsCategories && subsCategories.length
                      ? subsCategories
                      : []
                  }
                  renderItem={({item}) => (
                    <SubCategory
                      key={item.key}
                      item={item}
                      setSelectSubCategory={setSelectSubCategory}
                      selectSubCategory={selectSubCategory}
                    />
                  )}
                  keyExtractor={item => item.id}
                  horizontal
                  showsVerticalScrollIndicator={false}
                />
              ) : null}
              {!text || (!hasFocusedData && !subsGroupsFiltered.length) ? (
                <>
                  <RowThree>
                    <ExportAll>Todos</ExportAll>
                    <Checkbox
                      color={colors.orange}
                      uncheckedColor={colors.white}
                      status={checked ? 'checked' : 'unchecked'}
                      onPress={() => {
                        setChecked(!checked);
                      }}
                    />
                  </RowThree>
                  <FlatListStyled
                    itemData={itemData}
                    data={subsGroups && subsGroups.length ? subsGroups : []}
                    renderItem={({item}) => (
                      <Item
                        item={item}
                        key={item.key}
                        setSelectSubGroup={setSelectSubGroup}
                        selectSubGroup={selectSubGroup}
                        setChecked={setChecked}
                        checked={checked}
                      />
                    )}
                    keyExtractor={item => item.id}
                    showsVerticalScrollIndicator={false}
                  />
                </>
              ) : null}
              {loadingSubs && text ? (
                <Row>
                  <IconStyled
                    name="refresh"
                    size={24}
                    color="rgba(241, 241, 241, 0.4)"
                  />
                  <HeaderTitleTwo>Procurando por "{text}”...</HeaderTitleTwo>
                </Row>
              ) : null}
              {subsGroupsFiltered && subsGroupsFiltered.length && text ? (
                <FlatListStyled
                  itemData={itemData}
                  data={
                    subsGroupsFiltered && subsGroupsFiltered.length
                      ? subsGroupsFiltered
                      : []
                  }
                  renderItem={({item}) => (
                    <ItemByText
                      item={item}
                      key={item.key}
                      setSelectSubGroup={setSelectSubGroup}
                      selectSubGroup={selectSubGroup}
                      setChecked={setChecked}
                      checked={checked}
                    />
                  )}
                  keyExtractor={item => item.id}
                  showsVerticalScrollIndicator={false}
                />
              ) : null}

              <Button
                onPress={handleQuestions}
                active={
                  (selectSubGroup &&
                    selectSubGroup.length &&
                    !checked &&
                    !text) ||
                  (checked && subsGroups && subsGroups.length && !text)
                }>
                Reiniciar
              </Button>
            </>
          )}
        </Content>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default Category;
