import styled from 'styled-components/native';
import SvgUri from 'react-native-svg-uri';
import {Picker} from '@react-native-community/picker';
import FeatherIcon from 'react-native-vector-icons/SimpleLineIcons';

import {colors, fonts} from '../../../../global';

export const Container = styled.View`
  flex: 1;
  background-color: ${colors.background_main};
  position: relative;
  padding: 24px 24px 500px;
`;

export const Content = styled.View`
  flex: 1;
`;

export const TextError = styled.Text`
  font-size: ${fonts.small};
  color: ${colors.error};
  font-family: ${fonts.fontFamilyRegular};
`;

export const Logo = styled(SvgUri).attrs({
  source: require('../../../../assets/log.svg'),
  resizeMode: 'contain',
})`
  height: 200px;
`;

export const ForgotPassword = styled.TouchableOpacity`
  margin-top: 12px;
`;

export const ForgotPasswordText = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white_correct};
  font-family: ${fonts.fontFamilyRegular};
  text-align: center;
  margin-top: 5px;
`;

export const CreateAccount = styled.TouchableOpacity`
  margin-top: 50px;
  margin-bottom: 50px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const CreateAccountText = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.grey};
  font-family: ${fonts.fontFamilyRegular};
  margin-left: 16px;
`;

export const RowTwo = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ButtonBack = styled.TouchableOpacity``;

export const HeaderTitle = styled.Text`
  font-size: ${fonts.bigger};
  color: rgba(241, 241, 241, 0.4);
  font-family: ${fonts.fontFamilyRegular};
  margin: 24px 0 0 0;
`;

export const HeaderLabel = styled.Text`
  font-size: ${fonts.small};
  color: ${colors.color_botton_inactive};
  font-family: ${fonts.fontFamilyRegular};
  margin-top: 5px;
  margin-left: 16px;
`;

export const TitlePage = styled.Text`
  font-size: ${fonts.bigger};
  color: rgba(241, 241, 241, 0.4);
  font-family: ${fonts.fontFamilyRegular};
  margin: 36px 0 12px 0;
  margin-left: 16px;
`;

export const TextTerms = styled.Text`
  font-size: ${fonts.regular};
  color: rgba(241, 241, 241, 0.4);
  font-family: ${fonts.fontFamilyRegular};
  margin-top: auto;
  text-align: center;
  margin-left: 5px;
`;
export const ButtonSignIn = styled.TouchableOpacity`
  margin-bottom: 12px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: 12px;
  margin-top: auto;
  border-radius: 24px;
  background-color: ${({background}) =>
    background ? background : colors.orange};
  width: 100%;
`;

export const TextButtonSignIn = styled.Text`
  font-size: ${fonts.regular};
  color: ${({color}) => (color ? color : colors.color_text)};
  font-family: ${fonts.fontFamilyRegular};
`;

export const PickerContainer = styled.View`
  width: 100%;
  border-bottom-width: 2px;
  border-bottom-color: ${({borderBottom}) =>
    borderBottom ? borderBottom : colors.color_text};
  padding: 5px 16px 0 16px;
  margin-bottom: 8px;
  flex-direction: row;
  align-items: center;
`;

export const PickerStyled = styled(Picker)`
  border-width: 2px;
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.regular};
  padding: 0 4px;
  color: ${colors.color_input_text};
  margin: 0;
  width: 89%;
  text-align: center;
  ::placeholder {
    width: 100%;
    text-align: center;
    color: ${({color}) => (color ? color : colors.color_text)};
  }
`;

export const IconStyled = styled(FeatherIcon)``;

export const FlatListStyled = styled.FlatList`
  flex-basis: 0;
  flex: 1;
`;
