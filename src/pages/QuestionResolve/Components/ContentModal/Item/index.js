import React, {useState, useEffect, useCallback} from 'react';
import storage from '@react-native-firebase/storage';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';

import {colors} from '../../../../../global';

import {
  Container,
  Title,
  LogoIcon,
  Icon,
  Logo,
  TouchableOpacity,
  IconF,
} from './styles';

const Item = ({
  item,
  checked = false,
  handleCloseModal,
  handleOpenCategory,
}) => {
  const [urlImage, setUrlImage] = useState();
  const navigation = useNavigation();
  useEffect(() => {
    async function getImage() {
      if (item && item.icon) {
        storage()
          .ref(item.icon)
          .getDownloadURL()
          .then(url => {
            setUrlImage(url);
          });
      }
    }
    getImage();
  }, [item]);

  const handleCategory = useCallback(() => {
    handleCloseModal();
    handleOpenCategory();
    // navigation.navigate('CategoryFilter', {item: true});
    // await AsyncStorage.setItem('@categoryOrtopedia:', JSON.stringify(item));
  }, [handleCloseModal, handleOpenCategory]);

  return (
    <Container checked={checked} onPress={handleCategory}>
      {item.icon && urlImage ? <LogoIcon source={{uri: urlImage}} /> : <Logo />}
      <Title>{item.name}</Title>
      <TouchableOpacity checked={checked}>
        {checked ? (
          <IconF name={'check'} size={22} color={colors.orange} />
        ) : (
          <Icon name={'arrow-right'} size={22} color={colors.white} />
        )}
      </TouchableOpacity>
    </Container>
  );
};

export default Item;
