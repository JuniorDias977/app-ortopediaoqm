import styled from 'styled-components';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import FeatherIcon from 'react-native-vector-icons/Feather';

import {colors, fonts} from '../../../../../global';

export const TouchableOpacity = styled.View`
  position: absolute;
  right: 18px;
  top: 50%;
  width: 30px;
  height: 30px;
  background-color: ${({checked}) => (checked ? '#483e2f' : 'transparent')};
  padding: 5px;
  border-radius: 15px;
  justify-content: center;
  align-items: center;
`;

export const Icon = styled(SimpleLineIcon)``;

export const IconF = styled(FeatherIcon)``;

export const Container = styled.TouchableOpacity`
  border-bottom-width: 1px;
  border-bottom-color: #333333;
  margin-top: 12px;
  align-items: center;
  justify-content: flex-start;
  padding: 10px 12px 12px;
  background-color: ${({checked}) => (checked ? '#333333' : 'transparent')};
  border-radius: 12px;
  flex-direction: row;
`;

export const Title = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyBold};
  text-align: left;
  margin-top: 3%;
  width: 70%;
  margin-left: 10%;
`;

export const Logo = styled.Image.attrs({
  source: require('../../../../../assets/log-one.png'),
  resizeMode: 'contain',
})`
  width: 56px;
  height: 56px;
`;

export const LogoIcon = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: 56px;
  height: 56px;
`;
