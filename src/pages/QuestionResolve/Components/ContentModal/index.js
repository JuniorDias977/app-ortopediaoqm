import React, {useEffect, useState, useCallback} from 'react';
import {KeyboardAvoidingView, Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import Loading from '../../../../components/Loading';
import {useCategories} from '../../../../hooks/Categories';

import {Container, Content, Row, HeaderTitle, FlatListStyled} from './styles';
import Item from './Item';

const ContentModal = ({handleCloseModal, handleOpenCategory}) => {
  const {categories, loadingCategories} = useCategories();
  const [categoriesData, setCategoriesData] = useState([]);
  const [categorySelected, setCategorySelected] = useState([]);

  useEffect(() => {
    if (categorySelected) {
      setCategoriesData(
        categories.filter(
          cat => cat.active && cat.key !== categorySelected.key,
        ),
      );
    }
  }, [categories, categorySelected]);

  const selectCategory = useCallback(async () => {
    const resp = await AsyncStorage.getItem('@categoryOrtopedia:');
    setCategorySelected(JSON.parse(resp));
  }, []);

  useEffect(() => {
    selectCategory();
  }, [selectCategory]);

  const handleCloseModalData = useCallback(() => {
    handleCloseModal();
  }, [handleCloseModal]);

  return loadingCategories ? (
    <Loading />
  ) : (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <Content>
          <Row>
            <HeaderTitle>Você está estudando:</HeaderTitle>
          </Row>
          <Item
            item={categorySelected}
            checked={true}
            handleCloseModal={handleCloseModalData}
            handleOpenCategory={handleOpenCategory}
          />
          <Row>
            <HeaderTitle>Deseja estudar outro assunto?</HeaderTitle>
          </Row>
          <FlatListStyled
            showsVerticalScrollIndicator={false}
            data={categoriesData && categoriesData.length ? categoriesData : []}
            renderItem={({item}) => (
              <Item
                item={item}
                handleCloseModal={handleCloseModalData}
                handleOpenCategory={handleOpenCategory}
              />
            )}
            keyExtractor={item => item.id}
          />
        </Content>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default ContentModal;
