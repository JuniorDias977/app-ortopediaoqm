import React, {useCallback, useState, useEffect} from 'react';
import {Alert, Linking, Platform} from 'react-native';

import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import {useNavigation} from '@react-navigation/native';

import reactotron from 'reactotron-react-native';
import WebView from 'react-native-webview';

import {format, sub} from 'date-fns';

import {useAuth} from '../../../../hooks/Auth';
import {colors} from '../../../../global';

import {
  Container,
  Title,
  Icon,
  TouchableOpacity,
  TitleCategory,
  RowItem,
  TextItem,
  TextAnser,
  NextQuestion,
  NextQuestionText,
  IconFeather,
  ViewButtonBottom,
  ButtonItem,
  ButtonItemText,
  PontRight,
  ErrorText,
  ContainerVideo,
  ButtonRreplay,
  TextReplay,
  ErrorTextTwo,
  HeaderLabel,
  Image,
} from './styles';

const Item = ({item, index, onButtonPress, lastIndex, setHidden}) => {
  const {user} = useAuth();

  const [splited, setSplited] = useState(true);

  const [errored, setErrored] = useState(false);
  const [correct, setCorrect] = useState(false);

  const [disabled, setDisabled] = useState(false);
  const [clicked, setClicked] = useState();

  const [explain, setExplain] = useState(false);
  const [replay, setReplay] = useState(false);

  const [userData, setUserData] = useState();

  const [questionAnswered, setQuestionAnswered] = useState([]);
  const [urlData, setUrlData] = useState();
  const [loading, setLoading] = useState(false);

  const navigation = useNavigation();

  useEffect(() => {
    firestore()
      .collection('users')
      .where('key', '==', user.key)
      .onSnapshot(querySnapshot => {
        let usersArr = [];

        querySnapshot.forEach(documentSnapshot => {
          usersArr.push({
            ...documentSnapshot.data(),
            key: documentSnapshot.id,
          });
        });
        setUserData(usersArr[0]);
      });
  }, [user]);

  useEffect(() => {
    firestore()
      .collection('question_answered')
      .where('user_id', '==', user.key)
      .onSnapshot(querySnapshot => {
        let filterDocQuestion = [];

        const {category, sub_category, sub_group} = item;

        querySnapshot.forEach(documentSnapshot => {
          const {answered_question_user, key} = documentSnapshot.data();
          filterDocQuestion.push({
            answered_question_user,
            key,
          });
        });

        let filterResult = [];

        filterDocQuestion.forEach(arrayQuestion => {
          const key = arrayQuestion.key;

          let answered_question_user =
            arrayQuestion.answered_question_user.filter(itemArray => {
              return (
                itemArray.category.value === category.value &&
                itemArray.sub_category.value === sub_category.value
              );
            });

          if (answered_question_user.length > 0) {
            filterResult.push({
              answered_question_user,
              key,
            });
          }
        });

        setQuestionAnswered(filterResult);
      });
  }, [item, user.key]);

  useEffect(() => {
    async function getImage() {
      if (item?.image) {
        storage()
          .ref(item.image)
          .getDownloadURL()
          .then(url => {
            setUrlData(url);
            setLoading(false);
          })
          .catch(() => {
            setUrlData(item.image);
            setLoading(false);
          });
      }
    }
    getImage();
  }, [item]);

  const handleNext = useCallback(async () => {
    if (!errored && !correct) {
      firestore()
        .collection('history')
        .add({
          date: format(new Date(), 'yyyy-MM-dd')
            .concat(' ')
            .concat(format(new Date(), 'HH:mm:ss')),
          correct: false,
          errored: false,
          skiped: true,
          user: user.key,
          type: 'question',
          category: item.category,
          sub_category: item.sub_category,
          sub_group: item.sub_group,
        })
        .catch(function () {});
    }
    onButtonPress(index + 1);
    setCorrect(false);
    setErrored(false);
    setHidden(false);
    setDisabled(false);
    setClicked(null);
    setSplited(true);
    setExplain(false);
    setReplay(false);
  }, [index, onButtonPress, setHidden, item, user, correct, errored]);

  const handleText = useCallback((val, splt) => {
    if (val.length > 200 && splt) {
      return `${val.slice(0, 200)}...`;
    }
    if (val.length > 200 && !splt) {
      return val;
    }

    return val;
  }, []);

  const handleVideo = useCallback((video, bkg) => {
    return (
      <WebView
        allowsFullscreenVideo
        style={{backgroundColor: bkg}}
        containerStyle={{
          flex: 1,
          borderRadius: 15,
          width: '100%',
          height: '100%',
          padding: 0,
          margin: 0,
        }}
        scrollEnabled={false}
        automaticallyAdjustContentInsets
        originWhitelist={['*']}
        mediaPlaybackRequiresUserAction={false}
        useWebKit
        scalesPageToFit={false}
        allowsInlineMediaPlayback
        source={{
          html: `
              <html>
                <style>
                  .embed-container { 
                    position: relative; 
                    height: 0; 
                    overflow: hidden; 
                    max-width: 100%;  
                    background: #000;
                    height: 100%;
                    width: 100%;
                    margin: 0;
                    padding: 0;
                  }
                  .embed-container iframe, .embed-container object, .embed-container embed {
                    position: absolute; 
                    top: 0; 
                    left: 0; 
                    width: 100%; 
                    height: 100%; 
                  }
                  .player.hide-controlls-mode {
                    pointer-events: none!important;
                  }
                </style>
                <script src="https://player.vimeo.com/api/player.js"></script>
                <body>
                  <div class='embed-container'>
                    <iframe src="https://player.vimeo.com/video/${video}?background=0&autoplay=0" 
                      frameBorder="0"
                      allow="fullscreen"
                      webkitAllowFullScreen
                      mozallowfullscreen
                      allowFullScreen></iframe>
                  </div>
                </body>
              </html>
            `,
        }}
      />
    );
  }, []);

  const handleCorrectOrErrored = useCallback(
    (valCorrect, valError) => {
      if (item) {
        firestore()
          .collection('history')
          .add({
            date: format(new Date(), 'yyyy-MM-dd')
              .concat(' ')
              .concat(format(new Date(), 'HH:mm:ss')),
            correct: valCorrect,
            errored: valError,
            user: user.key,
            type: 'question',
            category: item.category,
            sub_category: item.sub_category,
            sub_group: item.sub_group,
          })
          .catch(function () {});
      }
    },
    [user, item],
  );

  useEffect(() => {
    if (correct || errored) {
      handleCorrectOrErrored(correct, errored);
    }
  }, [correct, errored, handleCorrectOrErrored]);

  const handleQuestionAnswered = useCallback(
    async selectedAlternative => {
      if (questionAnswered.length > 0) {
        let {answered_question_user, key} = questionAnswered[0];

        const docExists = answered_question_user.filter(arrayItem => {
          return arrayItem.key === item.key;
        });

        if (docExists.length === 0) {
          answered_question_user.push({
            category: item.category,
            selected_alternative: selectedAlternative,
            sub_group: item.sub_group,
            key: item.key,
            sub_category: item.sub_category,
          });

          const questionObject = {
            key,
            user_id: user.key,
            answered_question_user,
          };

          if (item) {
            firestore()
              .collection('question_answered')
              .doc(key)
              .update(questionObject)
              .then(docRef => {
                reactotron.log('RESULT DOC');
              });
          }
        }
      } else {
        const questionObject = {
          user_id: user.key,
          answered_question_user: [
            {
              key: item.key,
              category: item.category,
              sub_category: item.sub_category,
              sub_group: item.sub_group,
              selected_alternative: selectedAlternative,
            },
          ],
        };

        if (item) {
          firestore()
            .collection('question_answered')
            .add(questionObject)
            .then(docRef => {
              docRef.set({
                ...questionObject,
                key: docRef.id,
              });
            });
        }
      }
    },
    [item, user, questionAnswered],
  );

  return (
    <Container errored={errored} correct={correct}>
      {!correct && !errored ? (
        <TitleCategory errored={errored} explain={explain}>
          {item.category.label}/{item.sub_category.label}
        </TitleCategory>
      ) : null}

      {explain && (!correct || !errored) ? (
        <TitleCategory errored={errored} explain={explain}>
          {item.category.label}/{item.sub_category.label}
        </TitleCategory>
      ) : null}

      {explain && (correct || errored) ? (
        item.lesson_video ? (
          <ContainerVideo>
            {handleVideo(item.lesson_video, 'black')}
          </ContainerVideo>
        ) : null
      ) : null}

      {item.is_video ? (
        !errored && !correct ? (
          <ContainerVideo>
            {handleVideo(
              replay
                ? item.video
                : `${
                    (!correct || !errored) && !explain
                      ? item.video
                      : `${(correct || errored) && explain ? '' : null}`
                  }`,
              'transparent',
            )}
          </ContainerVideo>
        ) : (errored || correct) && !explain ? (
          <>
            <ErrorText>{correct ? 'Acertou :)' : 'Errou :('}</ErrorText>
            {!replay ? (
              <ButtonRreplay
                onPress={() => {
                  setReplay(true);
                  setExplain(false);
                }}>
                <Icon name="reload" size={23} />
                <TextReplay>Assistir novamente</TextReplay>
              </ButtonRreplay>
            ) : (
              <ContainerVideo>
                {handleVideo(item.video, 'black')}
              </ContainerVideo>
            )}
          </>
        ) : null
      ) : (
        <>
          <Title>
            {handleText(
              !correct && !errored
                ? item.description
                : `${explain ? item.description_answer : ''}`,
              splited,
            )}
          </Title>

          {!correct && !errored && item.description.length > 200 ? (
            <TouchableOpacity onPress={() => setSplited(!splited)}>
              <Icon
                name={!splited ? 'arrow-up' : 'arrow-down'}
                size={22}
                color={colors.white}
              />
            </TouchableOpacity>
          ) : null}

          {explain &&
          (correct || errored) &&
          item.description_answer.length > 200 ? (
            <TouchableOpacity onPress={() => setSplited(!splited)}>
              <Icon
                name={!splited ? 'arrow-up' : 'arrow-down'}
                size={22}
                color={colors.white}
              />
            </TouchableOpacity>
          ) : null}
          {(correct || errored) && !explain ? (
            <ErrorTextTwo>{correct ? 'Acertou :)' : 'Errou :('}</ErrorTextTwo>
          ) : null}
        </>
      )}

      {urlData && <Image source={{uri: urlData}} />}

      <RowItem
        errored={errored}
        correct={correct}
        disabled={disabled}
        clicked={clicked === 'a'}
        isCorrect={item.is_correct_a}
        onPress={() => {
          if (item.is_correct_a) {
            setCorrect(true);
          } else {
            setErrored(true);
          }
          setClicked('a');
          setHidden(true);
          setDisabled(true);
          handleQuestionAnswered('a');
        }}>
        {errored && clicked === 'a' && !item.is_correct_a ? (
          <IconFeather name="x" size={22} color={colors.red} />
        ) : null}
        {!errored && !correct ? (
          <TextItem
            isCorrect={item.is_correct_a}
            errored={errored}
            correct={correct}>
            A
          </TextItem>
        ) : null}
        {(errored || correct) && !item.is_correct_a && clicked !== 'a' ? (
          <TextItem
            isCorrect={item.is_correct_a}
            errored={errored}
            correct={correct}>
            A
          </TextItem>
        ) : null}
        {(errored || correct) && item.is_correct_a ? (
          <IconFeather name="check" size={22} color="#A9C21B" />
        ) : null}
        <TextAnser
          isCorrect={item.is_correct_a}
          errored={errored}
          correct={correct}>
          {item.alternative_a}
        </TextAnser>
      </RowItem>

      <RowItem
        errored={errored}
        correct={correct}
        disabled={disabled}
        isCorrect={item.is_correct_b}
        clicked={clicked === 'b'}
        onPress={() => {
          if (item.is_correct_b) {
            setCorrect(true);
          } else {
            setErrored(true);
          }
          setClicked('b');
          setHidden(true);
          setDisabled(true);
          handleQuestionAnswered('b');
        }}>
        {errored && clicked === 'b' && !item.is_correct_b ? (
          <IconFeather name="x" size={22} color={colors.red} />
        ) : null}
        {!errored && !correct ? (
          <TextItem
            isCorrect={item.is_correct_b}
            errored={errored}
            correct={correct}>
            B
          </TextItem>
        ) : null}
        {(errored || correct) && !item.is_correct_b && clicked !== 'b' ? (
          <TextItem
            isCorrect={item.is_correct_b}
            errored={errored}
            correct={correct}>
            B
          </TextItem>
        ) : null}
        {(errored || correct) && item.is_correct_b ? (
          <IconFeather name="check" size={22} color="#A9C21B" />
        ) : null}
        <TextAnser
          isCorrect={item.is_correct_b}
          errored={errored}
          correct={correct}>
          {item.alternative_b}
        </TextAnser>
      </RowItem>

      <RowItem
        disabled={disabled}
        errored={errored}
        correct={correct}
        clicked={clicked === 'c'}
        isCorrect={item.is_correct_c}
        onPress={() => {
          if (item.is_correct_c) {
            setCorrect(true);
          } else {
            setErrored(true);
          }
          setClicked('c');
          setHidden(true);
          setDisabled(true);
          handleQuestionAnswered('c');
        }}>
        {errored && clicked === 'c' && !item.is_correct_c ? (
          <IconFeather name="x" size={22} color={colors.red} />
        ) : null}
        {!errored && !correct ? (
          <TextItem
            isCorrect={item.is_correct_c}
            errored={errored}
            correct={correct}>
            C
          </TextItem>
        ) : null}
        {(errored || correct) && !item.is_correct_c && clicked !== 'c' ? (
          <TextItem
            isCorrect={item.is_correct_c}
            errored={errored}
            correct={correct}>
            C
          </TextItem>
        ) : null}
        {(errored || correct) && item.is_correct_c ? (
          <IconFeather name="check" size={22} color="#A9C21B" />
        ) : null}
        <TextAnser
          isCorrect={item.is_correct_c}
          errored={errored}
          correct={correct}>
          {item.alternative_c}
        </TextAnser>
      </RowItem>

      <RowItem
        clicked={clicked === 'd'}
        isCorrect={item.is_correct_d}
        disabled={disabled}
        errored={errored}
        correct={correct}
        onPress={() => {
          if (item.is_correct_d) {
            setCorrect(true);
          } else {
            setErrored(true);
          }
          setClicked('d');
          setHidden(true);
          setDisabled(true);
          handleQuestionAnswered('d');
        }}>
        {errored && clicked === 'c' && !item.is_correct_d ? (
          <IconFeather name="x" size={22} color={colors.red} />
        ) : null}
        {!errored && !correct ? (
          <TextItem
            isCorrect={item.is_correct_d}
            errored={errored}
            correct={correct}>
            D
          </TextItem>
        ) : null}
        {(errored || correct) && !item.is_correct_d && clicked !== 'c' ? (
          <TextItem
            isCorrect={item.is_correct_d}
            errored={errored}
            correct={correct}>
            D
          </TextItem>
        ) : null}
        {(errored || correct) && item.is_correct_d ? (
          <IconFeather name="check" size={22} color="#A9C21B" />
        ) : null}
        <TextAnser
          isCorrect={item.is_correct_d}
          errored={errored}
          correct={correct}>
          {item.alternative_d}
        </TextAnser>
      </RowItem>

      <HeaderLabel style={{marginTop: 'auto'}}>
        Questão retirada da Sociedade Brasileira de Ortopedia e Traumatologia,
        utilizando suas referências oficiais.
      </HeaderLabel>

      <TouchableOpacity
        onPress={() => Linking.openURL('https://sbot.org.br/a-sbot/teot/')}>
        <HeaderLabel> Clique aqui e veja mais detalhes</HeaderLabel>
      </TouchableOpacity>

      {errored || correct ? (
        <ViewButtonBottom>
          <ButtonItem
            onPress={() => {
              if (Platform.OS === 'android') {
                if (!explain && userData && !userData.is_studenty) {
                  Alert.alert(
                    'ATENÇÃO',
                    'Apenas alunos podem ver as explicações. Torne-se um aluno OQM e tenha acesso a todo o nosso material.',
                  );
                  return;
                }
              }
              setExplain(!explain);
              setReplay(false);
            }}>
            <Icon
              name={
                (correct || errored) && explain
                  ? 'control-pause'
                  : 'control-play'
              }
              size={38}
            />
            <ButtonItemText>Ver explicação</ButtonItemText>
          </ButtonItem>

          {index === lastIndex ? (
            <ButtonItem onPress={() => navigation.goBack()}>
              <Icon name="control-pause" size={33} />
              <ButtonItemText>Voltar</ButtonItemText>
            </ButtonItem>
          ) : (
            <ButtonItem onPress={handleNext}>
              <PontRight />
              <ButtonItemText>Próxima questão</ButtonItemText>
            </ButtonItem>
          )}
        </ViewButtonBottom>
      ) : index === lastIndex && !errored && !correct ? null : (
        <NextQuestion onPress={handleNext}>
          <NextQuestionText>Pular questão</NextQuestionText>
          <Icon
            name="arrow-right"
            size={18}
            color={colors.white}
            style={{marginTop: 2, marginLeft: 8}}
          />
        </NextQuestion>
      )}
    </Container>
  );
};

export default Item;
