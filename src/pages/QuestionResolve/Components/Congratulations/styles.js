import styled from 'styled-components/native';
import {Dimensions} from 'react-native';
import { RFPercentage, RFValue } from 'react-native-responsive-fontsize';

import {colors, fonts} from '../../../../global';
import { Image } from 'react-native';

const {width} = Dimensions.get('window');

export const Container = styled.View`
  flex: 1;
  width: ${width}px;
  /* background-color: red; */
  /* justify-content: center; */
  align-items: center;
  padding: 24px;
  margin-top: 16px;
`;

export const ContentText = styled.View`
  margin-top: 44px;
  width: 75%;
`;

export const Title = styled.Text`
  font-family: ${fonts.fontFamilyStrongBold};
  font-size: ${RFValue(24)}px;
  line-height: ${RFValue(34)}px;
  color: ${colors.secondary};
  text-align: center;
  letter-spacing: -0.01px;
`;

export const SubTitle = styled.Text`
  font-family: ${fonts.fontFamilyStrongBold};
  font-size: ${RFValue(18)}px;
  line-height: ${RFValue(34)}px;
  color: ${colors.secondary};
  text-align: center;
  letter-spacing: -0.01px;
`;

export const Confirm = styled(Image).attrs({
  source: require('../../../../assets/congratulations.png'),
  resizeMode: 'contain',
})`
  width: ${RFValue(303)}px;
  height: ${RFValue(225)}px;;
  align-self: center;
  margin-top: 44px;
  margin-bottom: 32px;
`;



