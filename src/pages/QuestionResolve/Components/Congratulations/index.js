import React, { useEffect, useState} from 'react';
import { Text } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import { TitleCategory } from '../Item/styles';
import {useNavigation} from '@react-navigation/native';
import reactotron from 'reactotron-react-native';

import {
  Container,
  Title,
  ContentText,
  SubTitle,
  Confirm,
} from './styles';
import Button from '../../../../components/Button';

const Congratulations = ({ sub_category }) => {
  const [category, setCategory] = useState({});

  const navigation = useNavigation();

  useEffect(() => {
    async function getCategory() {
      const categoryStorage = await AsyncStorage.getItem('@categoryOrtopedia:');
      if (categoryStorage) {
        setCategory(JSON.parse(categoryStorage));
      }
    }
    getCategory();
  }, []);

  return (
    <Container>
        <TitleCategory>
          {category.name}/{sub_category}
        </TitleCategory>

      <ContentText>
        <Title>Parabéns!</Title>
        <SubTitle>Você concluiu todas as questões dessa categorias.</SubTitle>
      </ContentText>

      <Confirm />

      <Button onPress={() => navigation.navigate('Category')}>Voltar</Button>
    </Container>
  );
};

export default Congratulations;
