import styled from 'styled-components/native';
import SvgUri from 'react-native-svg-uri';
import {Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

import {colors, fonts} from '../../global';

const ImageStyle = styled.Image``;

export const Background = styled(ImageStyle).attrs({
  source: require('../../assets/bg.png'),
})`
  width: ${width}px;
  height: ${height}px;
  position: absolute;
  top: 0;
  left: 0;
`;
export const BackgroundBlack = styled.View`
  width: ${width}px;
  height: ${height}px;
  position: absolute;
  top: 0;
  left: 0;
  background-color: ${colors.black};
  opacity: 0.7;
`;

export const Container = styled.View`
  flex: 1;
  background-color: ${colors.secondary};
  position: relative;
`;

export const Content = styled.View`
  flex: 1;
  padding: 0 30px;
`;

export const TextError = styled.Text`
  font-size: ${fonts.small};
  color: ${colors.error};
  font-family: ${fonts.fontFamilyRegular};
`;

export const Logo = styled(ImageStyle).attrs({
  source: require('../../assets/log.png'),
  resizeMode: 'contain',
})`
  height: 52px;
  width: 100%;
  align-self: center;
  margin-top: 30%;
`;

export const ForgotPassword = styled.TouchableOpacity`
  margin-top: 12px;
`;

export const TextAlert = styled.Text`
  font-size: ${fonts.bigger};
  color: ${colors.white_correct};
  font-family: ${fonts.fontFamilyRegular};
  width: 100%;
  margin-top: 80%;
  text-align: center;
`;

export const ButtonSignIn = styled.TouchableOpacity`
  margin-top: 24px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: 12px;
  border-radius: 24px;
  background-color: ${colors.orange};
  width: 100%;
`;

export const TextButtonSignIn = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.color_text};
  font-family: ${fonts.fontFamilyRegular};
`;

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-top: 24px;
`;

export const TextSignIn = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white_correct};
  font-family: ${fonts.fontFamilyRegular};
`;

export const TextSignInUnderline = styled.Text`
  font-size: ${fonts.regular};
  text-decoration: underline;
  color: ${colors.white_correct};
  font-family: ${fonts.fontFamilyRegular};
`;
