import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

import {
  Container,
  Content,
  Logo,
  TextAlert,
  Background,
  BackgroundBlack,
  ButtonSignIn,
  Row,
  TextButtonSignIn,
  TextSignIn,
  TextSignInUnderline,
} from './styles';

const SignIn = () => {
  const navigation = useNavigation();

  return (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <Background />
        <BackgroundBlack />
        <ScrollView style={{flex: 1}} contentContainerStyle={{flexGrow: 1}}>
          <Content>
            <Logo />
            <TextAlert>Estude onde quiser e na hora que você puder.</TextAlert>
            <ButtonSignIn onPress={() => navigation.navigate('SignIn')}>
              <TextButtonSignIn>Entrar</TextButtonSignIn>
            </ButtonSignIn>
            <Row>
              <TextSignIn>Não tem uma conta? </TextSignIn>
              <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
                <TextSignInUnderline>Cadastre-se</TextSignInUnderline>
              </TouchableOpacity>
            </Row>
          </Content>
        </ScrollView>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default SignIn;
