import React, {useCallback, useState, useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';

import {colors} from '../../global';

import {
  Container,
  Title,
  Icon,
  TouchableOpacity,
  RowItem,
  TextAnser,
  TextItem,
  TitleQuestion,
  IconFeather,
  Row,
  TitleCategory,
  TouchableOpacityTwo,
  ButtonDetails,
  ButtonDetailsText,
} from './styles';
import {ScrollView} from 'react-native';

const Item = props => {
  const navigation = useNavigation();
  const [splited, setSplited] = useState(true);
  const [item, setItem] = useState();
  const [selected, setSelected] = useState();
  const [correct, setCorrect] = useState(true);
  const [errored, setErrored] = useState(true);
  const [splitedAnswer, setSplitedAnswer] = useState(true);
  const [showAnswer, setShowAnswer] = useState(true);

  const {question} = props.route.params;

  useEffect(() => {
    if (question && question.question) {
      setItem(question.question);
    }
    if (question) {
      setSelected(question.selected);
      switch (question.selected) {
        case 'a':
          if (question && question.question && question.question.is_correct_a) {
            setCorrect(true);
            setErrored(false);
          }
          break;
        case 'b':
          if (question && question.question && question.question.is_correct_b) {
            setCorrect(true);
            setErrored(false);
          }
          break;
        case 'c':
          if (question && question.question && question.question.is_correct_c) {
            setCorrect(true);
            setErrored(false);
          }
          break;
        case 'd':
          if (question && question.question && question.question.is_correct_d) {
            setCorrect(true);
            setErrored(false);
          }
          break;
      }
    }
  }, [question]);

  const handleText = useCallback((val, splt) => {
    if (val.length > 200 && splt) {
      return `${val.slice(0, 200)}...`;
    }
    if (val.length > 200 && !splt) {
      return val;
    }

    return val;
  }, []);

  return item ? (
    <ScrollView style={{flex: 1}}>
      <Container>
        <>
          <Row>
            <TitleQuestion>Questão {question.questionId}</TitleQuestion>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <IconFeather name="x" size={30} color={colors.white} />
            </TouchableOpacity>
          </Row>
          <TitleCategory>
            {item.categoryData ? item.categoryData.label : 'Sem categoria'}
            {item.sub_category ? `/${item.sub_category.label}` : ''}
          </TitleCategory>
          <Title>{handleText(item.description, splited)}</Title>
          {item.description.length > 200 ? (
            <TouchableOpacityTwo onPress={() => setSplited(!splited)}>
              <Icon
                name={!splited ? 'arrow-up' : 'arrow-down'}
                size={22}
                color={colors.white}
              />
            </TouchableOpacityTwo>
          ) : null}
        </>
        <RowItem
          clicked={selected === 'a'}
          isCorrect={item.is_correct_a}
          disabled={true}
          errored={errored}
          correct={correct}>
          {errored && selected === 'a' && !item.is_correct_a ? (
            <IconFeather name="x" size={22} color={colors.red} />
          ) : null}
          {!errored && !correct ? (
            <TextItem
              isCorrect={item.is_correct_a}
              errored={errored}
              correct={correct}>
              A
            </TextItem>
          ) : null}
          {(errored || correct) && !item.is_correct_a && selected !== 'a' ? (
            <TextItem
              isCorrect={item.is_correct_a}
              errored={errored}
              correct={correct}>
              A
            </TextItem>
          ) : null}
          {(errored || correct) && item.is_correct_a ? (
            <IconFeather name="check" size={22} color="#A9C21B" />
          ) : null}
          <TextAnser
            isCorrect={item.is_correct_a}
            errored={errored}
            correct={correct}>
            {item.alternative_a}
          </TextAnser>
        </RowItem>

        <RowItem
          clicked={selected === 'b'}
          isCorrect={item.is_correct_b}
          disabled={true}
          errored={errored}
          correct={correct}>
          {errored && selected === 'b' && !item.is_correct_b ? (
            <IconFeather name="x" size={22} color={colors.red} />
          ) : null}
          {!errored && !correct ? (
            <TextItem
              isCorrect={item.is_correct_b}
              errored={errored}
              correct={correct}>
              B
            </TextItem>
          ) : null}
          {(errored || correct) && !item.is_correct_b && selected !== 'b' ? (
            <TextItem
              isCorrect={item.is_correct_b}
              errored={errored}
              correct={correct}>
              B
            </TextItem>
          ) : null}
          {(errored || correct) && item.is_correct_b ? (
            <IconFeather name="check" size={22} color="#A9C21B" />
          ) : null}
          <TextAnser
            isCorrect={item.is_correct_b}
            errored={errored}
            correct={correct}>
            {item.alternative_b}
          </TextAnser>
        </RowItem>

        <RowItem
          clicked={selected === 'c'}
          isCorrect={item.is_correct_c}
          disabled={true}
          errored={errored}
          correct={correct}>
          {errored && selected === 'c' && !item.is_correct_c ? (
            <IconFeather name="x" size={22} color={colors.red} />
          ) : null}
          {!errored && !correct ? (
            <TextItem
              isCorrect={item.is_correct_c}
              errored={errored}
              correct={correct}>
              C
            </TextItem>
          ) : null}
          {(errored || correct) && !item.is_correct_c && selected !== 'c' ? (
            <TextItem
              isCorrect={item.is_correct_c}
              errored={errored}
              correct={correct}>
              C
            </TextItem>
          ) : null}
          {(errored || correct) && item.is_correct_c ? (
            <IconFeather name="check" size={22} color="#A9C21B" />
          ) : null}
          <TextAnser
            isCorrect={item.is_correct_c}
            errored={errored}
            correct={correct}>
            {item.alternative_c}
          </TextAnser>
        </RowItem>
        <RowItem
          clicked={selected === 'd'}
          isCorrect={item.is_correct_d}
          disabled={true}
          errored={errored}
          correct={correct}>
          {errored && selected === 'd' && !item.is_correct_d ? (
            <IconFeather name="x" size={22} color={colors.red} />
          ) : null}
          {!errored && !correct ? (
            <TextItem
              isCorrect={item.is_correct_d}
              errored={errored}
              correct={correct}>
              D
            </TextItem>
          ) : null}
          {(errored || correct) && !item.is_correct_d && selected !== 'd' ? (
            <TextItem
              isCorrect={item.is_correct_d}
              errored={errored}
              correct={correct}>
              D
            </TextItem>
          ) : null}
          {(errored || correct) && item.is_correct_d ? (
            <IconFeather name="check" size={22} color="#A9C21B" />
          ) : null}
          <TextAnser
            isCorrect={item.is_correct_d}
            errored={errored}
            correct={correct}>
            {item.alternative_d}
          </TextAnser>
        </RowItem>
        {showAnswer ? (
          <Title>
            {handleText(
              item && item.description_answer ? item.description_answer : '',
              splitedAnswer,
            )}
          </Title>
        ) : null}
        {showAnswer &&
        item &&
        item.description_answer &&
        item.description_answer.length > 200 ? (
          <TouchableOpacityTwo onPress={() => setSplitedAnswer(!splitedAnswer)}>
            <Icon
              name={!splited ? 'arrow-up' : 'arrow-down'}
              size={22}
              color={colors.white}
            />
          </TouchableOpacityTwo>
        ) : null}
        {item && item.description_answer ? (
          <ButtonDetails onPress={() => setShowAnswer(!showAnswer)}>
            <ButtonDetailsText>
              {showAnswer ? 'Ocultar explicação' : 'Ver explicação'}
            </ButtonDetailsText>
          </ButtonDetails>
        ) : null}
      </Container>
    </ScrollView>
  ) : (
    <Container />
  );
};

export default Item;
