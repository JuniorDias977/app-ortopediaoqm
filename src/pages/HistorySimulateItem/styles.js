import styled, {css} from 'styled-components';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import FeatherIcon from 'react-native-vector-icons/Feather';
import {Dimensions} from 'react-native';

const {width} = Dimensions.get('window');

import {colors, fonts} from '../../global';

export const TouchableOpacityTwo = styled.TouchableOpacity`
  margin-bottom: 24px;
`;
export const TouchableOpacity = styled.TouchableOpacity``;

export const ButtonDetails = styled.TouchableOpacity`
  background-color: ${colors.orange};
  width: 100%;
  border-radius: 24px;
  height: 70px;
  align-items: center;
  justify-content: center;
  margin-top: 24px;
`;

export const Icon = styled(SimpleLineIcon)``;
export const IconFeather = styled(FeatherIcon)``;

export const Container = styled.View`
  align-items: center;
  justify-content: flex-start;
  padding: 0 24px 80px;
  width: ${width}px;
  ${({errored}) =>
    errored
      ? css`
          background-color: #ff483b;
          padding-top: 32px;
        `
      : null}
  ${({correct}) =>
    correct
      ? css`
          background-color: #a3c338;
          padding-top: 32px;
        `
      : null}
`;

export const Title = styled.Text`
  font-size: ${fonts.regular};
  text-align: center;
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  margin-top: 5%;
  margin-bottom: 16px;
`;

export const TitleQuestion = styled.Text`
  font-size: ${fonts.regular};
  text-align: left;
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
`;

export const TitleCategory = styled.Text`
  font-size: ${fonts.small};
  text-align: center;
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  background-color: #0073be;
  padding: 0 18px;
  top: ${({explain}) => (explain ? '7%' : '3%')};
  border-radius: 24px;
  z-index: 99999;
  margin-bottom: 24px;
`;

export const Logo = styled.Image.attrs({
  source: require('../../assets/log-one.png'),
  resizeMode: 'contain',
})`
  position: absolute;
  width: 56px;
  height: 56px;
  top: 24px;
  left: 0;
`;

export const PontRight = styled.Image.attrs({
  source: require('../../assets/hand-point-right.png'),
  resizeMode: 'contain',
})`
  width: 40px;
  height: 40px;
`;

export const LogoIcon = styled.Image.attrs({
  resizeMode: 'contain',
})`
  position: absolute;
  width: 56px;
  height: 56px;
  top: 24px;
  left: 0;
`;

export const RowItem = styled.TouchableOpacity`
  width: 100%;
  flex-direction: row;
  align-items: center;
  margin-bottom: 12px;
  background-color: #262626;
  padding: 24px;
  border-radius: 12px;

  ${({errored, isCorrect}) =>
    errored && !isCorrect
      ? css`
          opacity: 0.4;
        `
      : null}
  ${({errored, isCorrect, clicked}) =>
    errored && !isCorrect && !clicked
      ? css`
          background-color: rgba(38, 38, 38, 0.1);
        `
      : null}
  ${({errored, isCorrect}) =>
    errored && isCorrect
      ? css`
          background-color: ${colors.white};
        `
      : null}

${({correct, isCorrect}) =>
    correct && !isCorrect
      ? css`
          opacity: 0.4;
        `
      : null}
${({correct, isCorrect, clicked}) =>
    correct && !isCorrect && !clicked
      ? css`
          background-color: rgba(38, 38, 38, 0.1);
        `
      : null}
${({correct, isCorrect}) =>
    correct && isCorrect
      ? css`
          background-color: ${colors.white};
        `
      : null}
`;

export const TextItem = styled.Text`
  color: rgba(241, 241, 241, 0.2);
  font-size: ${fonts.bigger};
  font-family: ${fonts.fontFamilyRegular};

  ${({selected}) =>
    selected
      ? css`
          color: #262626;
          opacity: 0.2;
        `
      : null}
`;

export const TextAnser = styled.Text`
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.regular};
  flex-wrap: wrap;
  margin-left: 12px;
  padding-left: 12px;
  max-width: 90%;
  border-left-width: 1px;
  border-left-color: rgba(241, 241, 241, 0.2);

  ${({errored, isCorrect}) =>
    errored && isCorrect
      ? css`
          color: ${colors.background_main};
          border-left-color: rgba(38, 38, 38, 0.2);
        `
      : null}

  ${({correct, isCorrect}) =>
    correct && isCorrect
      ? css`
          border-left-color: rgba(38, 38, 38, 0.2);
          color: ${colors.background_main};
        `
      : null}
`;

export const NextQuestion = styled.TouchableOpacity`
  align-self: flex-end;
  flex-direction: row;
  background-color: #262626;
  padding: 12px 24px;
  border-radius: 18px;
  margin-top: 12px;
  margin-bottom: 12px;
`;

export const NextQuestionText = styled.Text`
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.small};
`;

export const ViewButtonBottom = styled.View`
  width: 100%;
  justify-content: space-between;
  flex-direction: row;
  margin-bottom: 12px;
`;

export const ButtonItem = styled.TouchableOpacity`
  flex-basis: 48%;
  background-color: ${colors.white};
  width: 48%;
  border-radius: 12px;
  padding: 12px;
  justify-content: center;
  align-items: center;
  min-height: 80px;
`;

export const ButtonItemText = styled.Text`
  color: ${colors.background_main};
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.regular};
  margin-top: 18px;
  text-align: center;
`;

export const ErrorText = styled.Text`
  color: ${colors.white};
  font-family: ${fonts.fontFamilyStrongBold};
  margin-bottom: 24px;

  font-size: 44px;
  line-height: 66px;
  text-align: center;
  margin: 64px 0 0;
`;

export const ErrorTextTwo = styled.Text`
  color: ${colors.white};
  font-family: ${fonts.fontFamilyStrongBold};
  margin-bottom: 64px;

  font-size: 44px;
  line-height: 66px;
  text-align: center;
`;

export const ContainerVideo = styled.View`
  min-height: 33%;
  max-height: 33%;
  border-radius: 24px;
  width: ${width}px;
  margin-top: 12px;
  z-index: 99998;
`;

export const ButtonRreplay = styled.TouchableOpacity`
  background-color: ${colors.white};
  padding: 5px 12px;
  flex-direction: row;
  border-radius: 12px;
  margin-bottom: 64px;
`;

export const TextReplay = styled.Text`
  color: ${colors.background_main};
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.regular};
  margin-left: 12px;
`;

export const Row = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
  margin-top: 5%;
  justify-content: space-between;
`;

export const ButtonDetailsText = styled.Text`
  color: ${colors.background_main};
  font-family: ${fonts.fontFamilyRegular};
  font-size: ${fonts.bigger};
  text-align: center;
`;
