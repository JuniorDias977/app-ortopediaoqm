import React, {useState, useEffect, useCallback} from 'react';
import {KeyboardAvoidingView, Platform, View, ScrollView} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import IconF from 'react-native-vector-icons/Feather';
import storage from '@react-native-firebase/storage';
import firestore from '@react-native-firebase/firestore';
import {format} from 'date-fns';

import Loading from '../../components/Loading';
import Button from '../../components/Button';
import {useSimulateUser} from '../../hooks/SimulateUser';
import {useAuth} from '../../hooks/Auth';

import {
  Container,
  TitlePage,
  Content,
  Row,
  ButtonBack,
  LogoIcon,
  LogoPreparate,
  Icon,
  TouchableOpacity,
  Title,
  LogoAlert,
  Text,
  TextWhite,
  ContentDescription,
  ViewAlert,
  AlertText,
  RowTwo,
  TextWhiteTwo,
  RowThree,
} from './styles';

import {colors} from '../../global';

const Category = props => {
  const navigation = useNavigation();
  const {user} = useAuth();
  const [loading, setLoading] = useState(true);
  const [urlImage, setUrlImage] = useState();
  const [itemData, setItemData] = useState({});
  const [splited, setSplited] = useState(true);
  const [isUserSimulate, setIsuserSimulate] = useState(false);
  const {simulatesUser, loadingSimulateUser, getSimulateUser} =
    useSimulateUser();
  const [isReady, setIsReady] = useState(false);
  const [userData, setUserData] = useState();
  const [timer, setTimer] = useState(0);

  useEffect(() => {
    setLoading(true);
    firestore()
      .collection('users')
      .where('key', '==', user.key)
      .onSnapshot(querySnapshot => {
        let usersArr = [];
        querySnapshot.forEach(documentSnapshot => {
          usersArr.push({
            ...documentSnapshot.data(),
            key: documentSnapshot.id,
          });
        });
        setUserData(usersArr[0]);
        setLoading(false);
      });
  }, [user]);

  useEffect(() => {
    if (props && props.route.params) {
      setItemData(props.route.params.item);
    }
  }, [props]);

  useEffect(() => {
    setLoading(true);
    if (simulatesUser && simulatesUser.length) {
      if (
        simulatesUser.find(
          sim => sim.simulate === itemData.key && sim.status === 'in_progress',
        )
      ) {
        setIsuserSimulate(true);
        setTimer(
          simulatesUser.find(
            sim =>
              sim.simulate === itemData.key && sim.status === 'in_progress',
          ).time,
        );
      }
    }
    setLoading(false);
  }, [itemData, simulatesUser]);

  useEffect(() => {
    async function getImage() {
      if (itemData && itemData.icon) {
        setLoading(true);
        storage()
          .ref(itemData.icon)
          .getDownloadURL()
          .then(url => {
            setUrlImage(url);
            setLoading(false);
          })
          .catch(() => setLoading(false));
      }
    }
    getImage();
  }, [itemData]);

  const handleText = useCallback((val, splt) => {
    if (val.length > 200 && splt) {
      return `${val.slice(0, 200)}...`;
    }
    if (val.length > 200 && !splt) {
      return val;
    }

    return val;
  }, []);

  const handleInitSimulate = useCallback(() => {
    // if (userData && !userData.is_studenty) {
    //   navigation.navigate('Alert');
    //   return;
    // }
    setLoading(true);
    const splitedDuration = itemData.duration.split(':');
    const val = splitedDuration[0] * 60 * 60 + splitedDuration[1] * 60;
    firestore()
      .collection('simulates_user')
      .add({
        date: format(new Date(), 'yyyy-MM-dd')
          .concat(' ')
          .concat(format(new Date(), 'HH:mm:ss')),
        active: true,
        user: user.key,
        simulate: itemData.key,
        status: 'in_progress',
        time: val,
      })
      .then(async () => {
        setLoading(false);
        navigation.navigate('SimulateResolve', {item: itemData, userData});
      })
      .catch(function () {
        setLoading(false);
      });
  }, [navigation, user, itemData, userData]);

  function pad(num) {
    return ('0' + num).slice(-2);
  }
  function hhmmss(secs) {
    var minutes = Math.floor(secs / 60);
    secs = secs % 60;
    var hours = Math.floor(minutes / 60);
    minutes = minutes % 60;
    return `${pad(hours)}:${pad(minutes)}`;
    // return pad(hours)+":"+pad(minutes)+":"+pad(secs); for old browsers
  }

  return loading || loadingSimulateUser ? (
    <Loading />
  ) : (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <Content>
          <ScrollView showsVerticalScrollIndicator={false}>
            <RowTwo>
              <ButtonBack onPress={() => navigation.goBack()}>
                <IconF
                  name="arrow-left"
                  color={colors.white_correct}
                  size={30}
                />
              </ButtonBack>
              {isReady ? <TitlePage>Está preparado?</TitlePage> : null}
            </RowTwo>
            {itemData.icon && urlImage && !isReady ? (
              <LogoIcon source={{uri: urlImage}} />
            ) : (
              <LogoPreparate />
            )}
            {isReady ? (
              <TextWhite
                style={{
                  marginTop: 16,
                  marginLeft: 24,
                  marginRight: 24,
                  opacity: 0.8,
                  fontSize: 14,
                  marginBottom: 12,
                }}>
                Mantenha-se concentrado e simule um dia de prova, reserve-se em
                um local protegido de interrupções e tente reponder todas as
                questões dentro do tempo de prova estabelecido.
              </TextWhite>
            ) : (
              <Title>{itemData.name}</Title>
            )}
            {isReady ? (
              <ViewAlert style={{marginLeft: 24, marginRight: 24}}>
                <LogoAlert />
                <AlertText>
                  Você não precisa realizar o simulado de uma única vez! Seu
                  progresso será mantido, em caso de pausas durante a sua
                  realização. Bom simulado!
                </AlertText>
              </ViewAlert>
            ) : null}
            <Row>
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <Text>Questões</Text>
                <TextWhiteTwo>{itemData.questions}</TextWhiteTwo>
              </View>
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <Text>
                  {isUserSimulate ? 'Tempo restante' : 'Tempo de prova'}
                </Text>
                <RowThree>
                  <IconF
                    name="clock"
                    color={colors.white_correct}
                    size={25}
                    style={{marginRight: 5}}
                  />
                  <TextWhiteTwo style={{marginTop: 5}}>
                    {timer !== 0 ? hhmmss(timer) : itemData.duration}
                  </TextWhiteTwo>
                </RowThree>
              </View>
            </Row>
            {isReady ? (
              <></>
            ) : (
              <ContentDescription>
                <TextWhite
                  style={{
                    opacity: 0.8,
                    fontSize: 14,
                    marginBottom: isUserSimulate ? 0 : 12,
                  }}>
                  {itemData && itemData.description
                    ? handleText(itemData.description, splited)
                    : ''}
                </TextWhite>
                {itemData &&
                itemData.description &&
                itemData.description.length > 200 ? (
                  <TouchableOpacity onPress={() => setSplited(!splited)}>
                    <Icon
                      name={!splited ? 'arrow-up' : 'arrow-down'}
                      size={22}
                      style={{alignSelf: 'center'}}
                      color={colors.white}
                    />
                  </TouchableOpacity>
                ) : null}
                <ViewAlert>
                  <LogoAlert />
                  <AlertText>
                    Você não precisa realizar o simulado de uma única vez! Seu
                    progresso será mantido, em caso de pausas durante a sua
                    realização. Bom simulado!
                  </AlertText>
                </ViewAlert>
                {isUserSimulate ? (
                  <TextWhite
                    style={{opacity: 0.8, marginBottom: 6, fontSize: 14}}>
                    Finalize o simulado para visualizar o seu desempenho.
                  </TextWhite>
                ) : null}
                <View style={{marginTop: 'auto'}}>
                  <Button
                    onPress={() => {
                      // if (userData && !userData.is_studenty) {
                      //   navigation.navigate('Alert');
                      //   return;
                      // }
                      if (isUserSimulate) {
                        navigation.navigate('SimulateResolve', {
                          item: itemData,
                          userData,
                        });
                        return;
                      }
                      setIsReady(true);
                    }}
                    style={{marginTop: 'auto'}}>
                    {isUserSimulate ? 'Retomar simulado' : 'Iniciar Simulado'}
                  </Button>
                </View>
              </ContentDescription>
            )}
            {isReady ? (
              <View
                style={{
                  marginLeft: 24,
                  marginRight: 24,
                  marginTop: 24,
                  marginBottom: 24,
                }}>
                <Button
                  onPress={handleInitSimulate}
                  style={{marginTop: 'auto'}}>
                  Agora sim, vamos começar...
                </Button>
              </View>
            ) : null}
          </ScrollView>
        </Content>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default Category;
