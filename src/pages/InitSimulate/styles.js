import styled from 'styled-components/native';
import {Dimensions} from 'react-native';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';

import {colors, fonts} from '../../global';

const {width, height} = Dimensions.get('window');

export const Container = styled.View`
  flex: 1;
  background-color: ${colors.background_main};
  position: relative;
`;

export const Content = styled.View`
  flex: 1;
`;

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 70%;
  align-self: center;
`;

export const RowThree = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ButtonBack = styled.TouchableOpacity`
  margin: 24px 0 0 24px;
`;

export const LogoAlert = styled.Image.attrs({
  source: require('../../assets/alert.png'),
  resizeMode: 'contain',
})`
  width: 24px;
  height: 24px;
`;

export const Logo = styled.Image.attrs({
  source: require('../../assets/log-one.png'),
  resizeMode: 'cover',
})`
  width: 124px;
  height: 124px;
  align-self: center;
  margin-top: 32px;
  border-width: 6px;
  border-color: #333333;
  border-radius: 62px;
`;

export const LogoPreparate = styled.Image.attrs({
  source: require('../../assets/img-preparado.png'),
  resizeMode: 'cover',
})`
  width: 228px;
  height: 228px;
  align-self: center;
`;

export const LogoIcon = styled.Image.attrs({
  resizeMode: 'cover',
})`
  width: 110px;
  align-self: center;
  height: 110px;
  border-radius: 55px;
  border-width: 6px;
  border-color: #333333;
`;

export const Title = styled.Text`
  font-size: 18px;
  color: ${colors.white};
  font-family: ${fonts.fontFamilyBold};
  text-align: left;
  align-self: center;
  margin-top: 16px;
`;

export const TitlePage = styled.Text`
  font-size: 24px;
  color: ${colors.white};
  font-family: ${fonts.fontFamilyBold};
  text-align: left;
  align-self: center;
  margin-top: 24px;
  margin-left: 48px;
`;

export const Text = styled.Text`
  font-size: ${fonts.small};
  color: ${colors.orange};
  font-family: ${fonts.fontFamilyRegular};
  text-align: left;
  align-self: center;
`;

export const TextWhite = styled.Text`
  font-size: ${fonts.regular};
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  align-self: center;
  text-align: center;
`;

export const TextWhiteTwo = styled.Text`
  font-size: 24px;
  color: ${colors.white};
  font-family: ${fonts.fontFamilyRegular};
  align-self: center;
  text-align: center;
  opacity: 0.8;
`;

export const ContentDescription = styled.View`
  flex: 1;
  background-color: #262626;
  width: ${width}px;
  border-top-left-radius: 24px;
  border-top-right-radius: 24px;
  margin-top: 12px;
  padding: 12px 24px;
  min-height: ${height / 2 + 20}px;
`;

export const Icon = styled(SimpleLineIcon)``;

export const TouchableOpacity = styled.TouchableOpacity`
  margin-bottom: 8px;
`;
export const ViewAlert = styled.View`
  padding: 12px 24px;
  flex-direction: row;
  align-items: center;
  border-width: 1px;
  border-color: rgba(241, 241, 241, 0.6);
  border-radius: 12px;
  margin-bottom: 6px;
`;

export const AlertText = styled.Text`
  font-size: ${fonts.small};
  color: rgba(241, 241, 241, 0.6);
  font-family: ${fonts.fontFamilyLight};
  text-align: center;
  margin: 0 12px;
`;
export const RowTwo = styled.View`
  flex-direction: row;
  align-items: center;
`;
