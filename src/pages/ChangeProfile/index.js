import React, {useCallback, useState} from 'react';
import {Formik} from 'formik';
import {useNavigation} from '@react-navigation/native';
import {Alert, KeyboardAvoidingView, Platform, ScrollView} from 'react-native';
import IconF from 'react-native-vector-icons/Feather';
import firestore from '@react-native-firebase/firestore';

import Loading from '../../components/Loading';
import {useAuth} from '../../hooks/Auth';
import Input from '../../components/Input';
import {colors} from '../../global';

import {
  Container,
  Content,
  Row,
  ButtonBack,
  HeaderTitle,
  HeaderLabel,
} from './styles';

const SignIn = () => {
  const {language, user, updateUser} = useAuth();
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState(false);

  const handleEdit = useCallback(async () => {
    try {
      if (!name) {
        Alert.alert('ERRO', 'Insira um nome');
        return;
      }
      setLoading(true);
      firestore().collection('users').doc(user.key).update({
        name,
      });
      updateUser(user);
      navigation.goBack();
    } catch (error) {
      setLoading(false);
    }
  }, [name, user, updateUser, navigation]);

  return loading ? (
    <Loading />
  ) : (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled>
      <Container>
        <ScrollView
          style={{flex: 1}}
          contentContainerStyle={{flexGrow: 1}}
          showsVerticalScrollIndicator={false}>
          <Content>
            <Row>
              <ButtonBack onPress={() => navigation.goBack()}>
                <IconF name="x" color={colors.white_correct} size={30} />
              </ButtonBack>
              <HeaderTitle>Alterar meu nome</HeaderTitle>
              <ButtonBack
                onPress={() => handleEdit()}
                style={{marginLeft: 'auto'}}>
                <IconF name="check" color={colors.orange} size={30} />
              </ButtonBack>
            </Row>
            <Formik initialValues={{name: user.name}} onSubmit={handleEdit}>
              {({handleBlur, values, errors, setFieldValue}) => (
                <>
                  <HeaderLabel>Meu nome</HeaderLabel>
                  <Input
                    name="name"
                    placeholder={
                      language ? 'Type your e-mail' : 'Digite seu nome'
                    }
                    autoCorrect={false}
                    autoCapitalize="none"
                    onChangeText={e => {
                      setFieldValue('name', e);
                      setName(e);
                    }}
                    onBlur={handleBlur('name')}
                    value={values.name}
                    error={errors.name}
                  />
                </>
              )}
            </Formik>
          </Content>
        </ScrollView>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default SignIn;
