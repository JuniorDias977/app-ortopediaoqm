import React from 'react';
import AppRoutes from './app.routes';
import AuthRoutes from './auth.routes';

import {useAuth} from '../hooks/Auth';
import Loading from '../components/Loading';

const Routes = () => {
  const {loading, user} = useAuth();
  if (loading) {
    return <Loading />;
  }

  return user ? <AppRoutes /> : <AuthRoutes />;
};

export default Routes;
