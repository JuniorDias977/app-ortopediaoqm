import React, {useEffect, useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Feather';
import IconF from 'react-native-vector-icons/SimpleLineIcons';
import {Platform, Linking} from 'react-native';
import firestore from '@react-native-firebase/firestore';

import {colors} from '../global';
import {useAuth} from '../hooks/Auth';

import Profile from '../pages/Profile';
import ChangeProfile from '../pages/ChangeProfile';
import ChangeProfilePhone from '../pages/ChangeProfilePhone';
import ChangeProfileResidence from '../pages/ChangeProfileResidence';
import Questions from '../pages/Questions';
import ChangeProfilePass from '../pages/ChangeProfilePass';
import Category from '../pages/Category';
import QuestionResolve from '../pages/QuestionResolve';
import CategoryFilter from '../pages/CategoryFilter';
import Simulates from '../pages/Simulates';
import InitSimulate from '../pages/InitSimulate';
import InitSimulateMain from '../pages/InitSimulateMain';
import Alert from '../pages/Alert';
import SimulateResolve from '../pages/SimulateResolve';
import SimulateResolveMain from '../pages/SimulateResolveMain';
import History from '../pages/History';
import HistoryItem from '../pages/HistoryItem';
import HistorySimulateItem from '../pages/HistorySimulateItem';

const App = createStackNavigator();
const Tab = createBottomTabNavigator();

const StackRoutes = () => {
  return (
    <App.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: colors.background_main,
          paddingTop: Platform.OS === 'ios' ? 24 : 0,
          paddingBottom: Platform.OS === 'ios' ? 24 : 0,
        },
        headerShown: false,
      }}
      // initialRouteName="WebView"
    >
      <App.Screen name="Profile" component={Profile} />
    </App.Navigator>
  );
};

const StackRoutesQuestions = () => {
  return (
    <App.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: colors.background_main,
          paddingTop: Platform.OS === 'ios' ? 24 : 0,
          paddingBottom: Platform.OS === 'ios' ? 24 : 0,
        },
        headerShown: false,
      }}
      // initialRouteName="WebView"
    >
      <App.Screen name="Questions" component={Questions} />
      <App.Screen name="Category" component={Category} />
    </App.Navigator>
  );
};

const BottomTabNavigator = () => {
  const [userData, setUserData] = useState();
  const {user} = useAuth();
  const [data, setData] = useState();

  useEffect(() => {
    firestore()
      .collection('users')
      .where('key', '==', user.key)
      .onSnapshot(querySnapshot => {
        let usersArr = [];
        querySnapshot.forEach(documentSnapshot => {
          usersArr.push({
            ...documentSnapshot.data(),
            key: documentSnapshot.id,
          });
        });
        setUserData(usersArr[0]);
      });
  }, [user]);

  useEffect(() => {
    firestore()
      .collection('settings')
      .onSnapshot(querySnapshot => {
        let categoriesArr = [];
        if (querySnapshot) {
          querySnapshot.forEach(documentSnapshot => {
            categoriesArr.push({
              ...documentSnapshot.data(),
              key: documentSnapshot.id,
            });
          });
        }
        setData(categoriesArr[0]);
      });
  }, []);

  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'QUESTÕES') {
            // iconName = focused ? 'check' : 'check';IconF
            return <IconF name="check" size={size} color={color} />;
          } else if (route.name === 'PERFIL') {
            iconName = focused ? 'user' : 'user';
          } else if (route.name === 'SIMULADOS') {
            iconName = focused ? 'clock' : 'clock';
          } else if (route.name === 'AULAS') {
            iconName = focused ? 'play' : 'play';
          } else if (route.name === 'DESEMPENHO') {
            iconName = focused ? 'pie-chart' : 'pie-chart';
          }

          // You can return any component that you like here!
          return <Icon name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: colors.white_correct,
        inactiveTintColor: colors.color_botton_inactive,
        style: {
          backgroundColor: colors.background_main,
          height: 60,
          elevation: 0, // for Android
          borderTopWidth: 0,
          shadowOffset: {
            width: 0,
          },
          paddingBottom: 8,
        },
      }}>
      <Tab.Screen name="QUESTÕES" component={StackRoutesQuestions} />

      <Tab.Screen
        name="SIMULADOS"
        options={{tabBarLabel: 'SIMULADO'}}
        component={Simulates}
      />

      {userData && userData.is_studenty ? (
        <Tab.Screen
          name="AULAS"
          options={{tabBarLabel: 'AULAS'}}
          listeners={{
            tabPress: e => {
              e.preventDefault();
              Linking.openURL(
                'https://cursos.ortopediaoqm.com.br/curso-extensivo-teot',
              );
            },
          }}
          component={Simulates}
        />
      ) : null}

      {data && data.free ? (
        <Tab.Screen
          name="DESEMPENHO"
          options={{tabBarLabel: 'DESEMPENHO'}}
          component={History}
        />
      ) : userData && userData.is_studenty ? (
        <Tab.Screen
          name="DESEMPENHO"
          options={{tabBarLabel: 'DESEMPENHO'}}
          component={History}
        />
      ) : null}
      <Tab.Screen name="PERFIL" component={StackRoutes} />
    </Tab.Navigator>
  );
};

const AppRoutes = () => (
  <App.Navigator
    screenOptions={{
      cardStyle: {
        backgroundColor: colors.background_main,
        paddingTop: Platform.OS === 'ios' ? 24 : 0,
        paddingBottom: Platform.OS === 'ios' ? 24 : 0,
      },
      headerShown: false,
    }}>
    <App.Screen name="Dashboard" component={BottomTabNavigator} />
    <App.Screen name="ChangeProfile" component={ChangeProfile} />
    <App.Screen name="ChangeProfilePhone" component={ChangeProfilePhone} />
    <App.Screen
      name="ChangeProfileResidence"
      component={ChangeProfileResidence}
    />
    <App.Screen name="ChangeProfilePass" component={ChangeProfilePass} />
    <App.Screen name="QuestionResolve" component={QuestionResolve} />
    <App.Screen name="CategoryFilter" component={CategoryFilter} />
    <App.Screen name="InitSimulate" component={InitSimulate} />
    <App.Screen name="InitSimulateMain" component={InitSimulateMain} />
    <App.Screen name="Alert" component={Alert} />
    <App.Screen name="SimulateResolve" component={SimulateResolve} />
    <App.Screen name="SimulateResolveMain" component={SimulateResolveMain} />
    <App.Screen name="HistoryItem" component={HistoryItem} />
    <App.Screen name="HistorySimulateItem" component={HistorySimulateItem} />
  </App.Navigator>
);

export default AppRoutes;
