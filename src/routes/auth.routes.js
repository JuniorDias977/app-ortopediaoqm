import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Platform} from 'react-native';

import {colors} from '../global';
import LogIn from '../pages/LogIn';
import SignIn from '../pages/SignIn';
import SignUp from '../pages/SignUp';
import Recover from '../pages/Recover';
import SuccessUser from '../pages/SuccessUser';
import Terms from '../pages/Terms';
import Policy from '../pages/Policy';
import SuccessUserConfirm from '../pages/SuccessUserConfirm';

const Auth = createStackNavigator();

const AuthRoutes = () => {
  return (
    <Auth.Navigator
      screenOptions={{
        headerShown: false,
        cardStyle: {
          backgroundColor: colors.background_main,
          paddingTop: Platform.OS === 'ios' ? 24 : 0,
          paddingBottom: Platform.OS === 'ios' ? 24 : 0,
        },
      }}>
      <Auth.Screen name="LogIn" component={LogIn} />
      <Auth.Screen name="SignIn" component={SignIn} />
      <Auth.Screen name="SignUp" component={SignUp} />
      <Auth.Screen name="Recover" component={Recover} />
      <Auth.Screen name="SuccessUser" component={SuccessUser} />
      <Auth.Screen name="Terms" component={Terms} />
      <Auth.Screen name="Policy" component={Policy} />
      <Auth.Screen name="SuccessUserConfirm" component={SuccessUserConfirm} />
    </Auth.Navigator>
  );
};

export default AuthRoutes;
